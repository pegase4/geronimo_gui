import os
import shutil
import time
from datetime import datetime


# parameters
app_path = r'F:\test app geronimo'
app_folder_name = datetime.now().strftime('%Y%m%d_geronimo_gui')
overwrite_app = True  # overwrite the application bundle if already exists in the specified path

t0 = time.time()

# app folder creation
print('creating folder...')
if not os.path.exists(app_path):
    raise RuntimeError('app path does not exist')
app_folder = os.path.join(app_path, app_folder_name)
if overwrite_app and os.path.isdir(app_folder):
    print('emptying folder...')
    shutil.rmtree(app_folder)
os.mkdir(app_folder)

# code copy
print('copying code...')
# main folder
shutil.copy(r'.\launch_app.bat', os.path.join(app_folder, 'launch_app.bat'))
# data folder
os.mkdir(os.path.join(app_folder, 'data'))
# signals folder
os.mkdir(os.path.join(app_folder, 'signals'))
shutil.copy(r'.\signals\sin_40kHz_5c_hann_50V.csv',
            os.path.join(app_folder, r'signals\sin_40kHz_5c_hann_50V.csv'))
# code folders
shutil.copytree(r'.\src', os.path.join(app_folder, 'src'), ignore=lambda *_: ['__pycache__', 'config.json'])
shutil.copytree(r'.\geronimo_utilities', os.path.join(app_folder, 'geronimo_utilities'),
                ignore=lambda *_: ['__pycache__', 'config.json'])

# venv copy
print('copying virtual env...')
shutil.copytree(r'.\venv', os.path.join(app_folder, 'venv'))

# python copy
print('copying python folder...')
with open(os.path.join(app_folder, r'venv\pyvenv.cfg'), 'r') as file:
    pyvenv_str = file.read()
pyvenv_list = pyvenv_str.split('\n')
python_folder = [s for s in pyvenv_list if s.startswith('home = ')][0][7:]
shutil.copytree(python_folder, os.path.join(app_folder, 'python'))

# pyvenv.cfg modification
pyvenv_str = pyvenv_str.replace(python_folder, r'.\python')
with open(os.path.join(app_folder, r'venv\pyvenv.cfg'), 'w') as file:
    pyvenv_str = file.write(pyvenv_str)

print('OK')
print(f'total time: {time.time() - t0:.0f}s')
