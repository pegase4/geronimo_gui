import copy
import logging
import numpy as np
import time
import requests
import json
import struct
import netifaces as ni
import ipaddress
import h5py
import io
import threading

from .config_files import GeroParam


GERO_URL_ADDRESSES = [
    'http://192.168.X.X:42420',
]


class GeronimoGeneralInterface:
    """
    Back to front communication with geronimo(s)
    """

    def __init__(self, gero_addresses: list[str | None], gero_nb_ch: list[int]):
        """
        General communication with geronimo(s) object
        :param gero_addresses: list of geronimo(s) address(es)
        :param gero_nb_ch: list of number of channels of each geronimo
        """
        if not gero_addresses or not gero_nb_ch:
            raise ValueError('Empty Geronimos list')
        if len(gero_addresses) != len(gero_nb_ch):
            raise ValueError(f'Geronimos lists of different lengths ({len(gero_addresses)}, {len(gero_nb_ch)})')
        # geronimos number of channels
        self.gero_nb_ch = gero_nb_ch
        # geronimos
        self.geronimos = []
        for addr in gero_addresses:
            if addr is not None and addr.lower() == 'fake':
                self.geronimos.append(FakeSingleGeronimo(addr))
            else:
                self.geronimos.append(SingleGeronimo(addr))

    def check_connection(self) -> (list[bool], list[dict]):
        """
        Checks the connection status of all geronimos
        :return: list of booleans, connection status of all geronimos, and list of dicts, info (MAC, satellites) of all
        geronimos
        """
        connexion = []
        info = []
        for gero in self.geronimos:
            co, i = gero.check_connection()
            connexion.append(co)
            info.append(i)
        return connexion, info

    def set_acquisition(self, config: dict, emission_ch: str, emission_sig: (np.ndarray[float], float), shot_id: int):
        """
        Sends the acquisition parameters to the geronimo(s)
        :param config: dict with keys 'averages' (number of shot averaged, int >= 1), 'samples' (number of samples, int
        >= 0), 'prf' (pulse repetition frequency, float > 0), 'fs' (sampling frequency, int in [2E6, 1E6, 0.5E6]) and
        'reception_ch' (reception channels, list of str in ['OFF', '0dB', '20dB', '40dB', '60dB'] by order of geronimo
        then reception channel (first geronimo's channel 1, first geronimo's channel 2, etc.))
        :param emission_ch: 'E1' for channel 1, etc. for 1 geronimo, 'G1:E1' for channel 1 of first geronimo, etc. for
         multiple synchronized geronimos, and 'E*' for no emission
        :param emission_sig: emission signal in V & sampling frequency, in a tuple containing 1D numpy array of floats
        (signal) and one float (sampling frequency)
        :param shot_id: acquisition shot id
        :return: 0 if no error encountered, error string otherwise
        """
        # preparing geronimos
        for k_gero in range(len(self.geronimos) - 1, -1, -1):
            if len(emission_ch.split(':')) == 2:  # multiple geronimos
                if emission_ch.split(':')[0] == f'G{k_gero + 1}':  # corresponding geronimo
                    emission_ch_gero = emission_ch.split(':')[-1]
                else:
                    emission_ch_gero = 'E*'
            elif len(emission_ch.split(':')) == 1:  # single geronimo
                emission_ch_gero = emission_ch
            reception_ch_gero = config['reception_ch'][sum(self.gero_nb_ch[:k_gero]):sum(self.gero_nb_ch[:k_gero+1])]
            config_gero = copy.deepcopy(config)
            if k_gero > 0 and config['trigger'] == 'internal':  # slave geronimo
                config_gero['trigger'] = 'external'
                config_gero['prf'] = 1.05 * config_gero['prf']  # slightly smaller repetition period for slave geronimo
            self.geronimos[k_gero].set_acquisition(config_gero, emission_ch_gero, reception_ch_gero, emission_sig,
                                                   shot_id)

    def get_acquisition_data(self, config: dict, shot_id: int) -> (np.ndarray[float], float):
        """
        Gets the acquisition data from the geronimo(s)
        :param config: same as for set_acquisition
        :param reception_ch: same as for set_acquisition
        :param shot_id: same as for set_acquisition
        :return: (signal, timestamp) with reception signal in V (2D n*m numpy array of floats, with n number of active
        reception channels and m number of samples), and timestamp (float)
        """
        u, ts = None, None
        for k_gero in range(len(self.geronimos)):
            gero = self.geronimos[k_gero]
            # timeout
            timeout = 60
            if u is None:  # master geronimo
                timeout += config['averages'] / config['prf']
                match config['trigger']:
                    case 'internal':
                        pass
                    case 'external':
                        timeout = None  # for external trigger long after geronimo setting
                    case 'date':  # for time lag between pc and geronimo
                        timeout += max(config['trigger_timestamp'] - time.time(), 0) + 5 * 60
            # data request
            reception_ch_gero = config['reception_ch'][sum(self.gero_nb_ch[:k_gero]):sum(self.gero_nb_ch[:k_gero + 1])]
            u_k, ts_k = gero.get_acquisition_data(config, reception_ch_gero, shot_id, timeout)
            if u is None:
                u, ts = u_k, ts_k
            else:
                u = np.concatenate((u, u_k), axis=0)
        return u, ts


class SingleGeronimo:
    """
    Single Geronimo class
    """
    TRIG_DICT = {
        'internal': 'TRIG_SOFT',
        'external': 'TRIG_ON_EXT',
        'date': 'TRIG_ON_PPS'
    }

    def __init__(self, gero_address: str | None):
        self.gero_address = gero_address

    @staticmethod
    def format_emission_signal(u: np.ndarray[float]) -> (np.ndarray[float], float):
        """
        Get normalized signal and amplitude from raw signal
        :param u: raw signal
        :return: normalized signal, amplitude
        """
        ampl = np.max(np.abs(u))
        v = u / ampl
        return v, ampl

    @staticmethod
    def convert_amplitude(ampl: float) -> int:
        """
        Convert amplitude in V to int (for internal geronimo parameter)
        :param ampl: amplitude, in V
        :return: amplitude int
        """
        ampls = [0, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        ampls_int = [0, 218, 338, 450, 563, 686, 768, 896, 1014, 1115]
        if not (ampls[0] <= ampl <= ampls[-1]):
            raise RuntimeError('Emission amplitude out of range')
        for ka in range(len(ampls)):
            if ampls[ka] > ampl:
                break
        ampl_int = ampls_int[ka-1] + (ampls_int[ka] - ampls_int[ka-1]) * \
            (ampl - ampls[ka-1]) / (ampls[ka] - ampls[ka-1])
        ampl_int *= 2  #conversion to Vpp
        ampl_int = int(np.round(ampl_int))
        return ampl_int

    @staticmethod
    def get_gero_address(progress_func, prev_addrs: list[str | None], n_ip_max: int)\
            -> list[str]:
        """
        search for available Geronimo address
        :param progress_func: progress function for display
        :param n_ip_max: maximum number of ip to find
        :return addresses: list of ip addresses
        """
        if n_ip_max == 0:
            return []
        addresses = []
        ni_interfaces = ni.interfaces()
        progress_func(1/2)
        for addr in ni_interfaces:
            if "VirtualBox" in addr:
                continue
            try:
                network_ip = ipaddress.IPv4Address(
                    int(ipaddress.IPv4Address(ni.ifaddresses(addr)[2][0]["addr"])) // 256 * 256)
            except KeyError:
                continue
            if not str(network_ip).startswith("192.168"):  # local private address
                continue

            # looping over all potential addresses
            id_addrs = [str(id_addr) for id_addr in ipaddress.IPv4Network(f"{network_ip}/24")]
            try:
                id_addrs.remove('192.168.202.1')
                id_addrs = ['192.168.202.1'] + id_addrs
            except ValueError:
                pass

            def ping_addr(addr_bool_shared: list[bool], k_addr: int, address: str, timeout: float):
                try:
                    req = requests.get(rf"http://{address}:42420/api/v1/hf8ch/on", timeout=timeout)
                    addr_bool_shared[k_addr] = req.status_code == 200
                    return
                except (requests.exceptions.ConnectTimeout, OSError):
                    return
            timeout = 2
            addr_bool = [False for _ in range(len(id_addrs))]
            threads = [None for _ in range(len(id_addrs))]
            for k in range(len(id_addrs)):
                threads[k] = threading.Thread(target=ping_addr, args=(addr_bool, k, id_addrs[k], timeout))
                threads[k].start()
            for k_time in np.arange(0, timeout, 0.5):
                progress_func(1/2 + k_time / timeout / 2)
                time.sleep(0.5)
            for k in range(len(id_addrs)):
                threads[k].join(timeout=timeout + 1)
            progress_func(1)
            for k in range(len(id_addrs)):
                if addr_bool[k]:
                    addresses.append(f'http://{id_addrs[k]}:42420')
                    if len(addresses) >= n_ip_max:
                        return addresses
        for prev_addr in prev_addrs:
            pass  # TODO
        return addresses

    def check_connection(self) -> (bool, dict):
        """
        Checks the geronimo connection
        :return: connection flag, geronimo info dict (MAC, satellites)
        """
        if self.gero_address is None:
            return False, {}
        else:
            try:
                r = requests.request('GET', self.gero_address + '/api/v1/hf8ch/on', timeout=2,
                                     headers={'Content-Type': 'application/json'})
                logging.debug(str(r.elapsed.total_seconds()))
                gero_info = {}  # TODO
                return r.status_code == 200, gero_info
            except:
                return False, {}

    def set_acquisition(self, config: dict, emission_ch: str, reception_ch: list[str],
                        emission_sig: (np.ndarray[float], float), shot_id: int):
        # address check
        if self.gero_address is None:
            raise RuntimeError('Missing Geronimo address')
        # implementation check
        if config['samples'] <= 0:
            raise NotImplementedError('Number of samples must be positive')
        if sum([c != 'OFF' for c in reception_ch]) == 0:
            raise NotImplementedError('No reception channel selected')
        # prf check (min is 32 samples at 500kHz (added in API))
        if config['averages'] > 1 and 1 / config['prf'] < config['samples'] / config['fs'] + 100e-6:
            raise RuntimeError('Pulse repetition period (1/PRF) cannot be less than shot duration')
        # request preparation
        emission_sig, emission_fs = emission_sig
        normed_sig, ampl = self.format_emission_signal(emission_sig)
        emission_ch_int = -1 if emission_ch == 'E*' else int(emission_ch[1:])
        signal_str = ';'.join([str(el) for el in normed_sig]) if emission_ch_int > 0 else ''
        ampl_int = self.convert_amplitude(ampl)
        command_dict = {
            'mean': config['averages'] - 1,
            'samples': config['samples'],
            'prf': config['prf'],
            'sampling_frequency': config['fs'],
            'active_channel': emission_ch_int,
            'gains': reception_ch,
            'signal': signal_str,
            'amplitude': ampl_int,
            'sampling_frequency_emission': emission_fs,
            'delay': config['delay'],
            'acquisition_mode': SingleGeronimo.TRIG_DICT[config['trigger']],
            'trigger_timestamp': config['trigger_timestamp'],
            'timestamp': time.time(),
            'uuid': shot_id,
        }
        logging.info('config request: ' + self.gero_address + ' '
                     + str([(k, command_dict[k]) for k in command_dict.keys() if k != 'signal']))
        r = requests.request('POST', self.gero_address + '/api/v1/hf8ch/configuration',
                             headers={'Content-Type': 'application/json'},
                             timeout=30,
                             data=json.dumps(command_dict))
        logging.info('config request received: ' + self.gero_address)
        if r.status_code != 200:
            error_str = f'Server connection error (code {r.status_code})'
            try:
                error_str += ': ' + str(r.content)
            except:
                pass
            raise RuntimeError(error_str)

    def get_acquisition_data(self, config: dict, reception_ch: list[str], shot_id: int, timeout: float)\
            -> (np.ndarray[float], float):
        # timestamp
        timestamp = time.time()
        # address check
        if self.gero_address is None:
            raise RuntimeError('Missing Geronimo address')
        # request preparation
        timeout_int = -1 if timeout is None else int(timeout)
        command_dict = {
            'uuid': shot_id,
            'timeout': timeout_int,
        }
        logging.info('data request: ' + self.gero_address + ' '
                     + str([(k, command_dict[k]) for k in command_dict.keys() if k != 'signal']))
        r = requests.request('POST', self.gero_address + '/api/v1/hf8ch/datas',
                             headers={'Content-Type': 'application/json'},
                             timeout=timeout + 5 if timeout is not None else None,
                             data=json.dumps(command_dict))
        logging.info('data request received: ' + self.gero_address)
        if r.status_code != 200:
            error_str = f'Server connection error (code {r.status_code})'
            try:
                error_str += ': ' + str(r.content)
            except:
                pass
            raise RuntimeError(error_str)
        try:  # previous api support
            shape_arr = struct.unpack('>ll', r.content[:8])
            u = struct.unpack('>' + np.prod(shape_arr) * 'd', r.content[8:])
            u = np.array(u)
            u = np.reshape(u, shape_arr).T
        except:
            with h5py.File(io.BytesIO(r.content), 'r') as data_file:
                u = data_file['signals'][()].T
        if u.size == 0 and not (all([s == 'OFF' for s in reception_ch])):
            raise RuntimeError(f'Data transfer error (empty signal)')
        if np.any(np.isnan(u)):
            raise RuntimeError(f'Data transfer error (NaN(s) in signal)')
        if u.size > 0 and np.all(u == 0):
            raise RuntimeError(f'Data transfer error (null signal)')
        return u, timestamp


class FakeSingleGeronimo(SingleGeronimo):
    """
    Geronimo mockup for testing.
    """

    TIME_DELAY_1CH = 0.1e-3  # time delay between two channels (non dispersive model)
    SIGNAL_FACTOR = 1e-3  # signal factor between input and output

    def __init__(self, gero_address: str | None):
        super().__init__(gero_address)
        self.u, self.ts = None, None

    def check_connection(self) -> (bool, dict):
        return True, {'MAC': 'F4-KE-GE-R0-N1-M0', 'sat': 100}

    @staticmethod
    def fake_data(config: dict, emission_ch: str, reception_ch: list[str], emission_sig: (np.ndarray[float], float))\
            -> (np.ndarray[float], float):
        ts = time.time()
        if np.random.random() < 0.:  # for bug handling testing
            time.sleep(2)
            raise RuntimeError('Fake acquisition bug')
        emission_sig, emission_fs = emission_sig
        emission_sig = emission_sig[::int(emission_fs / config['fs'])]  # rough downsampling
        k_delay_1ch = int(FakeSingleGeronimo.TIME_DELAY_1CH * config['fs'])
        n_rec = np.sum([el != 'OFF' for el in reception_ch])
        sig = np.zeros((n_rec, max(config['samples'], len(reception_ch) * k_delay_1ch + emission_sig.size)))
        if emission_ch != 'E*':  # 'E*' means no active emission channel
            k_emission_ch = int(emission_ch[1:]) - 1  # emission channel index
            k_rec_sig = 0
            for k_rec in range(len(reception_ch)):
                if reception_ch[k_rec] != 'OFF':
                    k_delay = abs(k_rec - k_emission_ch) * k_delay_1ch
                    reception_sig = np.copy(emission_sig)
                    rec_dB = float(reception_ch[k_rec][:-2])
                    reception_sig *= np.exp(np.log(10) * rec_dB / 20)
                    if k_rec != k_emission_ch:
                        reception_sig *= FakeSingleGeronimo.SIGNAL_FACTOR
                    reception_sig[reception_sig > 5] = 5
                    reception_sig[reception_sig < -5] = -5
                    sig[k_rec_sig][k_delay:k_delay+emission_sig.size] = reception_sig
                    k_rec_sig += 1
        sig += 0.001 * np.random.randn(*sig.shape)
        time.sleep(1)
        return sig[:, :config['samples']], ts

    def set_acquisition(self, config: dict, emission_ch: str, reception_ch: list[str],
                        emission_sig: (np.ndarray[float], float), shot_id: int):
        # acquisition time simulation
        if config['trigger'] == 'internal':
            time.sleep(config['averages'] / config['prf'])
        elif config['trigger'] == 'date':
            time.sleep(max(config['trigger_timestamp'] - time.time(), 0))
        # acquisition data
        self.u, self.ts = FakeSingleGeronimo.fake_data(config, emission_ch, reception_ch, emission_sig)

    def get_acquisition_data(self, config: dict, reception_ch: list[str], shot_id: int, timeout: float)\
            -> (np.ndarray[float], float):
        return self.u, self.ts
