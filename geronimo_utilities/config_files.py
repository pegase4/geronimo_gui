import json
import copy
import re
import os
import time


class GeroParam:
    """
    Class for geronimo hardware parameters
    """

    EMISSION_FS = [10E6, 5E6, 2.5E6, 2E6, 1E6, 0.625e6]  # emission sampling frequencies
    MAX_EMISSION_SAMPLES = 8192  # emission signal maximum nb of points
    MAX_RECEPTION_SAMPLES_CH = 64000000  # maximum total acquisition samples for one channel
    MAX_RECEPTION_SAMPLES_TOT = 64000000  # maximum total acquisition samples for one channel
    MIN_PRF = 0  # TODO
    MAX_PRF = 250  # TODO
    MAX_TENSION = 50
    MAX_AVERAGES = 2**16


class GeroConfig:
    """
    Class for json configuration files handling (default values, autofill, etc.)
    """

    DEFAULT_CONFIG_FILE_NAME = 'config.json'
    DEFAULT_TAB = 'pan-opt'
    DEFULT_OPTIONS_CONFIG = {
        'base_folder': '',
        'geronimo_nb_ch': [8],
        'geronimo_address': [None],
        'acq_loop': 'all',
        'acq_saving_mode': 'all',
        'user_data': None
    }
    DEFAULT_SIGNAL_CONFIG = {
        'fs': 10e6,
        'samples': 8192,
        'amplitude': 20,
        'freq': 40,
        'cycles': 5,
        'phase': 0,
        'window': 'hann',
        'alpha': 50,
        'signal_formula': '',
        'signal_folder': 'signals',
        'signal_file_name': ''
    }
    DEFAULT_TAB_CONFIG = {
        'file_name': '{y}{m}{d}_{H}{M}{S}_{acq}_{rep}',
        'file_folder': 'data',
        'disp_signals': True,
        'signal_folder': 'signals',
        'signal_file': None,
        'trigger': 'internal',
        'trigger_timestamp': None,
        'fs': 2E6,
        'samples': 2E3,
        'prf': 10,
        'averages': 1,
        'delay': 0,
        'repetitions': 1,
        'repetition_period': 0,
        'emission_ch': [],
        'reception_ch': ['OFF'] * sum(DEFULT_OPTIONS_CONFIG['geronimo_nb_ch']),
    }
    DEFAULT_ANALYSIS_CONFIG = {
        'last_file_folder': 'data',
        'auto_get_file': False,
        'auto_add_file': False,
        'bypass_windowing': False,
        'min_time': None,
        'max_time': None,
        'zero_padding': False,
        'window': 'Rectangular',
        'alpha_window': None,
        'bypass_filtering': False,
        'remove_mean': False,
        'highpass_freq': None,
        'highpass_type': 'fir',
        'highpass_param': 0.1,
        'lowpass_freq': None,
        'lowpass_type': 'fir',
        'lowpass_param': 0.1,
        'disp_signal': True,
        'disp_envelope': False,
        'log_scale_signal': False,
        'disp_fft': True,
        'log_scale_fft': False,
        'disp_spect': True,
        'spect_win_len': 0.1,
        'spect_win': 'rectangular',
        'spect_win_alpha': 50,
        'spect_win_overlap': 50,
        'log_scale_spect': False,
        'custom_signals': [],
    }
    DEFAULT_CONFIG = {
        'active_tab': DEFAULT_TAB,
        'options_config': DEFULT_OPTIONS_CONFIG,
        'signal_config': DEFAULT_SIGNAL_CONFIG,
        'tabs_configs': [DEFAULT_TAB_CONFIG],
        'analysis_config': DEFAULT_ANALYSIS_CONFIG
    }

    @staticmethod
    def check_config_format(config: dict):
        flag = False  # error flag
        # nh of geronimo
        flag |= len(config['options_config']['geronimo_nb_ch']) != len(config['options_config']['geronimo_address'])
        # nb of reception channels
        nb_ch_tot = sum(config['options_config']['geronimo_nb_ch'])
        flag |= any([len(tab_config['reception_ch']) != nb_ch_tot for tab_config in config['tabs_configs']])
        # emission channels formatting
        nb_ch_gero = config['options_config']['geronimo_nb_ch']
        nb_gero = len(nb_ch_gero)
        if nb_gero == 1:  # single geronimo mode
            for tab_config in config['tabs_configs']:
                flag |= any([not re.match(r'E\d+$', ch) for ch in tab_config['emission_ch']])
                flag |= any([int(ch[1:]) > nb_ch_gero[0] for ch in tab_config['emission_ch']])
        else:  # multiple geronimos mode
            for tab_config in config['tabs_configs']:
                flag |= any([not re.match(r'G\d+:E\d+$', ch) for ch in tab_config['emission_ch']])
                flag |= any([int(ch.split(':')[0][1:]) > nb_gero for ch in tab_config['emission_ch']])
                flag |= any([int(ch.split(':')[1][1:]) > nb_ch_gero[int(ch.split(':')[0][1:]) - 1]
                             for ch in tab_config['emission_ch']])
        if flag:
            raise RuntimeError('"config.json" file corrupted (delete or fix file)')

    @staticmethod
    def get_config_str(config: dict) -> str:
        return json.dumps(config, separators=(',', ': '), indent=2)

    @staticmethod
    def get_complete_config(config: dict, ref_config: dict = None, first_recursion: bool = True) -> dict:
        # creating config from scratch
        if ref_config is None:
            ref_config = GeroConfig.DEFAULT_CONFIG
        # filling the missing parameters
        for k in ref_config.keys():
            if k not in config.keys():
                config[k] = copy.deepcopy(ref_config[k])
            elif type(config[k]) == dict:
                config[k] = copy.deepcopy(GeroConfig.get_complete_config(config[k], ref_config[k],
                                                                         first_recursion=False))
            elif k == 'tabs_configs':
                for i in range(len(config[k])):
                    if type(config[k][i]) == dict:
                        config[k][i] = copy.deepcopy(GeroConfig.get_complete_config(config[k][i], ref_config[k][0],
                                                                                    first_recursion=False))
        # updating base folder if none
        if first_recursion and config['options_config']['base_folder'] == '':
            config['options_config']['base_folder'] = os.getcwd()
        # updating shot trigger date if none
        if first_recursion:
            for k_tab in range(len(config['tabs_configs'])):
                if config['tabs_configs'][k_tab]['trigger_timestamp'] is None:
                    config['tabs_configs'][k_tab]['trigger_timestamp'] = int(time.time()) + 1
        # config file error check
        if first_recursion:
            GeroConfig.check_config_format(config)
        return config
