import numpy as np
import re
from .back_front_com import GeronimoGeneralInterface
from .config_files import GeroParam
import uuid
import time


def acquisition(
        gero_addresses: list[str],
        gero_nb_ch: list[int],
        emission_ch: str,
        emission_sig: (np.ndarray[float], float),
        reception_ch: list[str],
        samples: int,
        fs: int,
        averages: int,
        prf: float,
        delay: int = 0,
        trigger: str = 'internal',
        trigger_timestamp: int = 0,
) -> (np.ndarray[float], float):
    """
    Performs acquisition from one or multiple synchronized geronimos, raises RuntimeError if a problem is encountered
    :param gero_addresses: list of geronimo(s) url address(es)
    :param gero_nb_ch: list of geronimo(s) number of channels
    :param emission_ch: emission channel, 'G2:E3' for channel 3 of geronimo 2 for instance, and 'E*' for no emission
    :param emission_sig: emission signal in V & sampling frequency, in a tuple containing 1D numpy array of floats
    (signal) and one float (sampling frequency)
    :param reception_ch: list of reception channels status ('OFF', '0dB', '20dB', '40dB' or '60dB'), by order of
    geronimo then channel (geronimo 1 channel 1, ..., geronimo 1 channel 8, geronimo 2 channel 1, ...)
    :param samples: number of acquired samples
    :param fs: sampling frequency, in Hz, either 2 MHz, 1 MHz or 500 kHz
    :param averages: number of shots averaged, minimum is 1
    :param prf: pulse repetition frequency, in Hz
    :param delay: delay between beginning of emission and beginning of reception recording, in samples, >= 0
    :param trigger: trigger mode ('internal', 'external', 'date')
    :param trigger_timestamp: timestamp for 'date' trigger mode
    :return: time array, 1D m numpy array with m number of samples, signal in V, 2D n*m numpy array with n number of
    active reception channels and , and timestamp of the acquisition
    """

    # input checks
    if len(gero_addresses) != len(gero_nb_ch):
        raise ValueError('length of gero_addresses and gero_nb_ch must be equal')
    if len(gero_addresses) == 1:
        if not (re.match(r'E\d+$', emission_ch) or re.match(r'G1:E\d+$', emission_ch) or emission_ch == 'E*'):
            raise ValueError('emission_ch must be in "G*:E*" format')
    elif len(gero_addresses) > 1:
        if not (re.match(r'G\d+:E\d+$', emission_ch) or emission_ch == 'E*'):
            raise ValueError('emission_ch must be in "G*:E*" format')
    if emission_ch != 'E*' and int(emission_ch.split(':')[0][1:]) > len(gero_addresses):
        raise ValueError('emission_ch geronimo index exceeds number of geronimos')
    if emission_ch != 'E*' and int(emission_ch.split(':')[1][1:]) > gero_nb_ch[int(emission_ch.split(':')[0][1:]) - 1]:
        raise ValueError('emission_ch channels index exceeds number of channels')
    if len(emission_sig[0]) > GeroParam.MAX_EMISSION_SAMPLES:
        raise ValueError(f'emission_sig exceeds maximum number of samples {GeroParam.MAX_EMISSION_SAMPLES}')
    if np.any(np.abs(emission_sig[0]) > GeroParam.MAX_TENSION):
        raise ValueError(f'emission_sig exceeds maximum voltage of +-{GeroParam.MAX_TENSION} V')
    if emission_sig[1] not in GeroParam.EMISSION_FS:
        raise ValueError(f'emission_sig sampling frequency must be in {GeroParam.EMISSION_FS}')
    if any([ch not in ['OFF', '0dB', '20dB', '40dB', '60dB'] for ch in reception_ch]):
        raise ValueError('reception_ch elements must be "OFF", "0dB", "20dB", "40dB" or "60dB"')
    if len(reception_ch) != sum(gero_nb_ch):
        raise ValueError('length of emission_ch must be equal to total number of channels')
    if (samples > GeroParam.MAX_RECEPTION_SAMPLES_CH or
            samples * sum([r != 'OFF' for r in reception_ch]) > GeroParam.MAX_RECEPTION_SAMPLES_TOT):
        raise ValueError(f'too much sample points (max per channel {GeroParam.MAX_RECEPTION_SAMPLES_CH}, '
                         f'max total {GeroParam.MAX_RECEPTION_SAMPLES_TOT}')
    if fs not in [2000000, 1000000, 500000]:
        raise ValueError(f'fs must be in [2000000, 1000000, 500000]')
    if not (GeroParam.MIN_PRF <= prf <= GeroParam.MAX_PRF):
        raise ValueError(f'PRF must be between {GeroParam.MIN_PRF} and {GeroParam.MAX_PRF} Hz')
    if not averages > 0:
        raise ValueError('averages must be > 0')
    if delay < 0:
        raise ValueError('delay must be >= 0')
    if trigger not in ['internal', 'external', 'date']:
        raise ValueError('trigger parameter must be "internal", "external" or "date"')
    if trigger == 'date' and trigger_timestamp != int(trigger_timestamp):
        raise ValueError('trigger_timestamp must be an integer')
    if trigger == 'date' and trigger_timestamp <= time.time() + 2:
        raise ValueError('trigger_timestamp must be in the future')

    # acquisition
    gero = GeronimoGeneralInterface(gero_addresses, gero_nb_ch)
    config_dict = {
            'averages': averages,
            'samples': samples,
            'prf': prf,
            'fs': fs,
            'delay': delay,
            'reception_ch': reception_ch,
            'trigger': trigger,
            'trigger_timestamp': trigger_timestamp,
        }
    shot_id = uuid.uuid4().fields[0]
    gero.set_acquisition(config_dict, emission_ch, emission_sig, shot_id)
    sig, ts = gero.get_acquisition_data(config_dict, shot_id)
    t = np.arange(delay, delay + samples) / fs

    return t, sig, ts
