import copy
import os
import h5py
import numpy as np
import io
from datetime import datetime
import traceback
import csv
import logging

SAVE_EXTENSION = '.gero'
OLD_SAVE_EXTENSION = '.geronimo'  # for backwards compatibility
DEFAULT_OVERRIDE = False

h5py.get_config().track_order = True


def check_saving_folder(folder_path: str) -> (0, str):
    """
    Checks if save file folder exists, and creates it if not
    :param folder_path: save file folder path
    :return: flag: 0 if folder exists/is created, error string otherwise
    """
    flag = 0
    try:
        folder_exists = os.path.isdir(folder_path)
    except Exception as e:
        folder_exists = True
        flag = type(e).__name__ + ': ' + str(e)
    if not folder_exists:
        try:
            os.mkdir(folder_path)
        except Exception as e:
            flag = type(e).__name__ + ': ' + str(e)
    return flag


def save_signal_to_file(file_path: str, signals: list[np.ndarray[float]], config: dict, override: bool) -> (0, str):
    """
    Saves signal to hdf5 format, with config.
    :param file_path: save file path
    :param signals: signal array, such as signal[idx_em][idx_rec][idx_time]
    :param config: acquisition config file
    :param override: automatic overriding file, bypasses the DEFAULT_OVERRIDE variable
    :return: flag: 0 if saving is successful, error string otherwise
    """
    if os.path.splitext(os.path.basename(file_path))[0] == 'no_save':
        return 0
    override = override or DEFAULT_OVERRIDE
    try:
        signals = concatenate_signals(signals)
        writing_mode = 'w' if override else 'x'
        with h5py.File(file_path, writing_mode) as file:
            dataset = file.create_dataset('signal', data=signals, compression='gzip')  # signal data
            for (k, h) in config.items():  # signal metadata
                if h is None:
                    h = ''
                dataset.attrs.create(k, h)
        flag = 0
    except Exception as e:
        logging.error(traceback.format_exc())
        flag = type(e).__name__ + ': ' + str(e)
    return flag


def read_file(filepath: str) -> (np.ndarray, np.ndarray, dict):
    """
    Reads .gero files and returns data and metadata
    :param filepath: file path
    :return: u: signal array as u[ind_emission, ind_reception, ind_time], t: time array as t[ind_time], metadata dict
    """
    with h5py.File(filepath, 'r') as file:
        u = file['signal'][()]
        metadata = dict(file['signal'].attrs)
    t = metadata['delay'] + np.arange(u.shape[-1]) / metadata['fs']
    return u, t, metadata


def convert_signal_to_csv(signals: np.ndarray[float], config: dict) -> str:
    """
    Converts signal to csv format, with config.
    :param signals: signal array, such as signal[idx_em][idx_rec][idx_time]
    :param config: acquisition config file
    :return: file_str: file string
    """
    # reception channels labels
    if len(config['geronimo_nb_ch']) == 1:
        reception_ch = config['reception_ch']
        reception_ch = [f'R{k + 1}' for k in range(len(reception_ch)) if reception_ch[k] != 'OFF']
    else:
        reception_ch = []
        for k_ch_tot in range(len(config['reception_ch'])):
            if config['reception_ch'][k_ch_tot] != 'OFF':
                k_ch, k_gero = k_ch_tot, 0
                gero_nb_ch = list(copy.deepcopy(config['geronimo_nb_ch']))
                while k_ch >= gero_nb_ch[0]:
                    k_gero += 1
                    k_ch -= gero_nb_ch.pop(0)
                reception_ch.append(f'G{k_gero + 1}:R{k_ch + 1}')
    # csv writer
    output = io.StringIO()
    writer = csv.writer(output, delimiter=',', lineterminator='\n')
    # csv string header
    writer.writerow(['sep=,'])
    writer.writerow(['Parameters'])
    for (k, h) in config.items():  # signal metadata
        if type(h) is np.ndarray or type(h) is list:
            writer.writerow([k] + list(h))
        else:
            writer.writerow([k, h])
    writer.writerow([])
    writer.writerow(['Data [V]'])
    sig_labels = []
    for k_em in range(signals.shape[0]):
        for k_rec in range(signals.shape[1]):
            sig_labels.append(config['emission_ch'][k_em] + ' ' + reception_ch[k_rec])
    writer.writerow(sig_labels)
    # csv string data
    signals = np.reshape(signals, (signals.shape[0] * signals.shape[1], signals.shape[2])).T
    writer.writerows(signals)
    return output.getvalue()


def concatenate_signals(signals: list[np.ndarray[float]]) -> np.ndarray[float]:
    """
    Concatenate signals from different emission channels into a single np array. Adds nans if time indexes do not match.
    :param signals: list of signals, such as signals[idx_em][idx_rec][idx_time]
    :return: array of signals, such as signals[idx_em][idx_rec][idx_time]
    """
    dims0, dims1 = [], []
    for signal in signals:
        dims0.append(signal.shape[0])
        dims1.append(signal.shape[1])
    for k in range(len(signals)):
        if signals[k].shape[0] != signals[0].shape[0]:
            raise RuntimeError('Saving error: inconsistent number of reception channels.')
        if signals[k].shape[1] < max(dims1):
            signals[k] = np.concatenate((signals[k], np.nan((signals[k].shape[0], max(dims1) - signals[k].shape[1]))))
    return np.array(signals)


def get_data_from_bytes(data: bytes) -> dict:
    with io.BytesIO(data) as file_bytes:
        with h5py.File(file_bytes, 'r') as file:
            data_dict = get_dict_from_hdf5(file)
    return get_new_format(data_dict)


def get_data_from_file(file_path: str) -> dict:
    with h5py.File(file_path, 'r') as file:
        data_dict = get_dict_from_hdf5(file)
    return get_new_format(data_dict)


def get_dict_from_hdf5(file) -> dict:
    d = {}
    for k in file.keys():
        if type(file[k]) == h5py.Group:
            d[k] = get_dict_from_hdf5(file[k])
            for k2 in file[k].attrs.keys():
                d[k][k2] = file[k].attrs[k2]
        elif type(file[k]) == h5py.Dataset:
            d[k] = {}  # dataset dictionary, with data as 'data' and attributes
            d[k]['data'] = copy.deepcopy(file[k][()])
            for k2 in file[k].attrs.keys():
                d[k][k2] = file[k].attrs[k2]
        else:
            raise NotImplementedError()
    return d


def get_new_format(d: dict) -> dict:
    if 'signal' in d.keys():  # new format
        return d
    # time data
    t = d['data']['time']['data']
    fs = d['data']['fs']
    delay = int(t[0] * fs)

    # signal data
    reception_ch_k = []
    samples = 0
    data = np.zeros((1, 0, samples))
    for k in d['data'].keys():
        if k.startswith('channel_'):
            reception_ch_k.append(int(k[8:]))
            ch_size = d['data'][k]['data'].size
            if ch_size > samples:
                samples = ch_size
                data = np.concatenate((data, np.zeros((data.shape[0], data.shape[1], samples - data.shape[2]))), axis=2)
                ch_data = np.array(d['data'][k]['data'], ndmin=3)
            else:
                ch_data = np.array(np.concatenate((d['data'][k]['data'], np.zeros(samples - ch_size))), ndmin=3)
            data = np.concatenate((data, ch_data), axis=1)
    reception_ch = []
    for k in range(8):
        match d['data'][f'gain_{8-k}']:
            case 0:
                gain = 'OFF'
            case 1:
                gain = '0dB'
            case 17:
                gain = '20dB'
            case 33:
                gain = '40dB'
            case 49:
                gain = '60dB'
            case _:
                gain = '?'
        reception_ch.append(gain)
    emission_ch = [f'E{9 - d["data"]["channel"]}']

    d3 = {}
    for k, el in d['data'].items():
        if 'channel_' not in k and k != 'time':
            d3[k] = el

    # new dict creation
    d2 = {
        'signal': {
            'data': data,
            'fs': fs,
            'samples': d['data']['length'],
            'prf': d['data']['prf'],
            'averages': d['data']['moyenne'],
            'delay': delay,
            'emission_ch': emission_ch,
            'reception_ch': reception_ch,
            'timestamps': [datetime.fromtimestamp(d['data']['timestamp']).strftime('%d/%m/%Y %H:%M:%S.%f')],
            'geronimo_address': [''],
            'geronimo_nb_ch': [8],
            'various': d3
        }
    }
    return d2
