Installation:
- for windows bundle (with venv already in folder):
    - double-click on launch_app.bat to start app
    - launch_app.bat may be used as default program for .gero files
for manual installation:
    - install packages from requirements.txt (pip install -r requirements.txt), preferably in virtual environment
    - run app by running src/main_app.py from base folder
    - for window users, update python path in launch_app.bat to use as app launcher
    - launch_app.bat may be used as default program for .gero files

Python package:
The source code contains a python package with an opener for .gero files and a SDK for controlling geronimo.
Install it from the base folder (pip install -e C:\...\geronimo_gui), the package is named geronimo_utilities.
Two methods are available: geronimo_utilities.read_file, and geronimo_utilities.acquisition.
Check saving_module.py and geronimo_sdk.py in geronimo_utilities folder for documentation.