from setuptools import setup

setup(
    name='geronimo_gui',
    version='1.0',
    description='Geronimo graphical user interface',
    author='Raphaël Carpine',
    author_email='raphael.carpine@cea.fr',
    packages=['geronimo_utilities'],
)
