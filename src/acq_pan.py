import os
from dash import html, dcc, Input, Output, State, callback, ALL, MATCH, ctx, Patch
import dash_bootstrap_components as dbc
import dash_daq as daq
import plotly.graph_objects as go
import numpy as np
import time
import copy
from datetime import datetime
import traceback
import re
import uuid
import logging

from geronimo_utilities.back_front_com import GeronimoGeneralInterface
from geronimo_utilities.saving_module import check_saving_folder, save_signal_to_file, SAVE_EXTENSION
from geronimo_utilities.config_files import GeroParam, GeroConfig
from sig_pan import SignalPanel


class AcquisitionPanel(html.Div):
    MAX_FIGS = 10  # maximum number figures (therefore maximum of tabs)
    MAX_ACQ_ATTEMPTS = 3  # maximum attempts at geronimo acquisition (connection logout, etc.)
    MAX_POINTS_WARNING = 10e6

    def __init__(self, config: dict):

        ######################################## display ########################################

        # whole tab
        super().__init__([
            dcc.Store(id='store-fig-plot'),  # store object for single figure update
            dcc.Store(id='store-acquisition-configs', data=[]),  # store object for acquisition configs
            dcc.Store(id='store-config-update', data=config),  # store object for panel updating from external config
            dcc.Store(id='store-displayed-points', data=0),  # store object for number of displayed points (for warning)
            dcc.Interval(id='interval-remaining-time', interval=1000, max_intervals=0),  # shot remaining time refresh
            top_div := html.Div(style={'padding': 8, 'width': '100%', 'height': 0, 'margin-bottom': -16}),  # start stop
            tabs_div := html.Div(style={'padding': 0, 'padding-top': 5, 'width': '100%'}),
        ])

        # top Div
        top_div.children = [
            dbc.Stack([
                dbc.Col(html.Div(), width='auto'),
                dbc.Col(html.Div(), className='ms-auto'),
                dbc.Col(html.Div([
                    html.Label(id='label-start', children=[], style={'margin-right': 5}),
                    html.Div([
                        daq.Indicator(id='ind-connection', value=False,
                                      style={'display': 'inline-block', 'margin': 8, 'verticalAlign': 'middle'})
                    ], style={'display': 'inline-block', 'margin': 0, 'verticalAlign': 'middle'},
                        title='Geronimo connection indicator'),
                    html.Button(id='btn-start', children='Start', style={'margin': 3, 'verticalAlign': 'middle'}),
                    html.Button(id='btn-stop', children='Stop', style={'margin': 3, 'verticalAlign': 'middle'},
                                disabled=True),
                ]), width='auto', style={'z-index': '1000 !important', 'position': 'relative'})
            ], direction='horizontal')
        ]

        # tabs
        self.tabs = dbc.Tabs(id='tabs', children=[])
        tabs_div.children = [self.tabs]
        self.hidden_graphs_container = html.Div(id='hidden-graphs', children=[], hidden=True)

        # graphs creation (fixed number of figures, hidden at the beginning)
        self.all_graphs = []  # all dcc.Graph object for acquisition plots
        available_graphs = []  # available dcc.Graph objects indexes
        tabs_graphs = []  # used dcc.Graph objects indexes, in same order as tabs
        for k in range(self.MAX_FIGS):
            fig = self.get_empty_figure()
            graph = dcc.Graph(id=f'graph-{k}', style={'width': '100%', 'height': '100%'}, figure=fig,
                              config={'editable': True})
            self.hidden_graphs_container.children.append(graph.to_plotly_json())
            self.all_graphs.append(graph)
            available_graphs.append(k)
        # Store object for accessing graph indexes (available graphs and used graphs (in tabs))
        self.graphs_ind = dcc.Store(id='store-graphs-ind',
                                    data={'available_graphs': available_graphs, 'tabs_graphs': tabs_graphs})
        self.children.append(self.graphs_ind)

        # + tab creation
        plus_tab = dbc.Tab(tab_id='plus-tab', label='+', children=[])
        self.tabs.children.append(plus_tab)

        plus_tab.children.append(self.hidden_graphs_container)

        # config initialization
        for tab_config in config['tabs_configs']:
            self.tabs.children, _, self.graphs_ind.data, self.hidden_graphs_container.children = \
                self.create_acq_tab(self.tabs.children, tab_config, config['options_config'])

        ######################################## callbacks ########################################

        # new tab creation callback
        @callback(Output('tabs', 'children', allow_duplicate=True),
                  Output('tabs', 'active_tab', allow_duplicate=True),
                  Output('store-graphs-ind', 'data', allow_duplicate=True),
                  Output('hidden-graphs', 'children', allow_duplicate=True),
                  Input('tabs', 'active_tab'),
                  State('tabs', 'children'),
                  State('store-config', 'data'),
                  State('store-graphs-ind', 'data'),
                  State('hidden-graphs', 'children'),
                  prevent_initial_call=True)
        def tab_creation_callback(act_tab, tabs_children, data, graphs_ind, hidden_graphs_container_children):
            if act_tab == 'plus-tab':  # click on the "+" tab to add new config tab
                if len(tabs_children) > 1:  # if previous config tab exists, copy its parameters
                    new_tab_config = data['tabs_configs'][-1]
                else:
                    new_tab_config = None
                tabs_children, act_tab, graphs_ind, hidden_graphs_container_children = \
                    self.create_acq_tab(tabs_children, new_tab_config, data['options_config'], act_tab, graphs_ind,
                                        hidden_graphs_container_children)
            return tabs_children, act_tab, graphs_ind, hidden_graphs_container_children

        # tab closing callback
        @callback(Output('tabs', 'children', allow_duplicate=True),
                  Output('tabs', 'active_tab', allow_duplicate=True),
                  Output('store-graphs-ind', 'data', allow_duplicate=True),
                  Output('hidden-graphs', 'children', allow_duplicate=True),
                  Input({'type': 'close-btn', 'index': ALL}, 'n_clicks'),
                  State('tabs', 'children'),
                  State('tabs', 'active_tab'),
                  State('store-graphs-ind', 'data'),
                  State('hidden-graphs', 'children'),
                  prevent_initial_call=True
                  )
        def close_current_tab(n_clicks, tabs_children, act_tab, graphs_ind, hidden_graphs_container_children):
            k_tab = None
            for k in range(len(n_clicks)):  # determining which close button is clicked
                if n_clicks[k] is not None:
                    k_tab = k
                    break
            if k_tab is not None and len(tabs_children) > 2:
                # assigning new active tab
                if k_tab < len(n_clicks) - 1:
                    new_act_tab = tabs_children[k_tab + 1]['props']['tab_id']
                else:
                    new_act_tab = tabs_children[k_tab - 1]['props']['tab_id']
                # deleting tab
                graph_id = graphs_ind['tabs_graphs'].pop(k_tab)
                graph = self.all_graphs[graph_id]
                graph.figure = self.get_empty_figure()
                hidden_graphs_container_children.append(graph.to_plotly_json())
                graphs_ind['available_graphs'].append(graph_id)
                del tabs_children[k_tab]
                # changing tabs names
                for k in range(len(tabs_children) - 1):
                    tabs_children[k]['props']['label'] = f'Acquisition {k+1}'
            else:
                new_act_tab = act_tab
            return tabs_children, new_act_tab, graphs_ind, hidden_graphs_container_children

        # tab closing button disabling
        @callback(
            Output({'type': 'close-btn', 'index': ALL}, 'disabled'),
            Input('tabs', 'children'),
            State({'type': 'close-btn', 'index': ALL}, 'disabled')
        )
        def close_tab_button_disabling(tabs_children, close_disabled):
            if len(tabs_children) <= 2:
                close_disabled = [True for _ in close_disabled]
            else:
                close_disabled = [False for _ in close_disabled]
            return close_disabled

        # delete signals display callback
        @callback(
            [Output(f'graph-{k_g}', 'figure') for k_g in range(AcquisitionPanel.MAX_FIGS)],
            Input({'type': 'check-display-sig', 'index': ALL}, 'value'),
            State('store-graphs-ind', 'data'),
            prevent_initial_call=True
        )
        def delete_fig(disp_sigs, graphs_ind):
            graphs = [Patch() for _ in range(AcquisitionPanel.MAX_FIGS)]
            for k_config in range(len(disp_sigs)):
                if not disp_sigs[k_config]:
                    k_graph = graphs_ind['tabs_graphs'][k_config]
                    graphs[k_graph]['data'] = []
                    graphs[k_graph]['layout']['title'] = ''
            return graphs

        # tab display callback (ms, emission folder files update, hidden parameters)
        @callback(
            Output({'type': 'label-samples', 'index': MATCH}, 'children'),  # samples label (ms conversion)
            Output({'type': 'drop-signal-file', 'index': MATCH}, 'options'),  # emission signal dropdown update
            Output({'type': 'drop-signal-file', 'index': MATCH}, 'value'),  # emission signal dropdown update
            Output({'type': 'div-timestamp', 'index': MATCH}, 'hidden'),  # trigger date input hidden/shown
            Output({'type': 'input-timestamp', 'index': MATCH}, 'value'),  # trigger date input
            Output({'type': 'input-timestamp', 'index': MATCH}, 'style'),  # trigger date input format in red
            Output({'type': 'div-repetition-period', 'index': MATCH}, 'hidden'),  # repetition period hidden/shown
            Output({'type': 'interval-update-date-color', 'index': MATCH}, 'interval'),
            Output({'type': 'interval-update-date-color', 'index': MATCH}, 'max_intervals'),
            Output({'type': 'interval-update-date-color', 'index': MATCH}, 'n_intervals'),
            Input({'type': 'input-signal-folder', 'index': MATCH}, 'value'),
            Input({'type': 'drop-signal-file', 'index': MATCH}, 'value'),
            State({'type': 'drop-signal-file', 'index': MATCH}, 'options'),
            Input({'type': 'div-drop-signal-file', 'index': MATCH}, 'n_clicks'),
            Input({'type': 'drop-fs', 'index': MATCH}, 'value'),
            Input({'type': 'input-samples', 'index': MATCH}, 'value'),
            Input({'type': 'drop-trig', 'index': MATCH}, 'value'),
            Input({'type': 'input-timestamp', 'index': MATCH}, 'value'),
            State({'type': 'input-timestamp', 'index': MATCH}, 'style'),
            Input({'type': 'input-repetitions', 'index': MATCH}, 'value'),
            Input({'type': 'interval-update-date-color', 'index': MATCH}, 'n_intervals'),
            State('store-config', 'data'),
        )
        def tab_display_callback(
                emission_folder_val,
                emission_file_val, emission_file_opt, _,
                fs_val,
                samples_val,
                trigger,
                trig_date, trig_date_style,
                nb_rep,
                __,
                config,
        ):
            # trigger
            trigger_id = ctx.triggered_id

            # reception signal length label
            if samples_val is None or fs_val is None:
                label_samples = 'Samples:'
            else:
                label_samples = f'Samples ({1e3 * samples_val / fs_val} ms):'

            # emission signal folder files update
            if trigger_id is None or trigger_id['type'] in ['input-signal-folder', 'div-drop-signal-file']:
                emission_file_opt = self.get_emission_folder_files(emission_folder_val,
                                                                   config['options_config']['base_folder'])
                if trigger_id is not None and trigger_id['type'] == 'input-signal-folder':  # signal folder changed
                    emission_file_val = None
                if not (emission_file_val in emission_file_opt):  # previous file not in new folder
                    emission_file_val = None

            # trigger date hidden update
            trig_date_hidden = trigger != 'date'
            if not trig_date_hidden and trigger_id is not None and trigger_id['type'] == 'drop-trig':
                trig_ts = int(datetime.strptime(trig_date, '%Y-%m-%d %H:%M:%S').timestamp())
                if trig_ts <= time.time():
                    trig_date = datetime.fromtimestamp(time.time() + 30).strftime('%Y-%m-%d %H:%M:%S')

            # trigger date style (red if date past)
            date_color_interval = 0
            data_color_max_intervals = 0
            if 'color' in trig_date_style.keys():
                trig_date_style.pop('color')
            if trig_date is None or trig_date == '':
                trig_date = datetime.fromtimestamp(time.time() + 30).strftime('%Y-%m-%d %H:%M:%S')
            try:
                trig_ts = int(datetime.strptime(trig_date, '%Y-%m-%d %H:%M:%S').timestamp())
                if trig_ts <= time.time():
                    trig_date_style['color'] = 'red'
                else:
                    date_color_interval = int(1000 * (trig_ts - time.time() + 0.5))
                    data_color_max_intervals = 1
            except ValueError:
                trig_date_style['color'] = 'red'

            # repetition period div hidden
            rep_div_hidden = nb_rep == 1

            return (label_samples, emission_file_opt, emission_file_val, trig_date_hidden, trig_date, trig_date_style,
                    rep_div_hidden, date_color_interval, data_color_max_intervals, 0)

        # tab parameters callback
        @callback(Output({'type': 'tab-param-store', 'index': MATCH}, 'data', allow_duplicate=True),  # acq tab param
                  Input({'type': 'input-file', 'index': MATCH}, 'value'),
                  Input({'type': 'input-folder', 'index': MATCH}, 'value'),
                  Input({'type': 'check-display-sig', 'index': MATCH}, 'value'),
                  Input({'type': 'input-signal-folder', 'index': MATCH}, 'value'),
                  Input({'type': 'drop-signal-file', 'index': MATCH}, 'value'),
                  Input({'type': 'drop-trig', 'index': MATCH}, 'value'),
                  Input({'type': 'input-timestamp', 'index': MATCH}, 'value'),
                  Input({'type': 'drop-fs', 'index': MATCH}, 'value'),
                  Input({'type': 'input-samples', 'index': MATCH}, 'value'),
                  Input({'type': 'input-prf', 'index': MATCH}, 'value'),
                  Input({'type': 'input-averages', 'index': MATCH}, 'value'),
                  Input({'type': 'input-delay', 'index': MATCH}, 'value'),
                  Input({'type': 'input-repetitions', 'index': MATCH}, 'value'),
                  Input({'type': 'input-repetition-period', 'index': MATCH}, 'value'),
                  Input({'type': 'gero-emission', 'index': MATCH, 'ch': ALL, 'gero': ALL}, 'value'),
                  Input({'type': 'gero-emission', 'index': MATCH, 'ch': ALL, 'gero': ALL}, 'id'),
                  Input({'type': 'gero-reception', 'index': MATCH, 'ch': ALL, 'gero': ALL}, 'value'),
                  Input({'type': 'gero-reception', 'index': MATCH, 'ch': ALL, 'gero': ALL}, 'id'),
                  State({'type': 'tab-param-store', 'index': MATCH}, 'data'),
                  State('store-config', 'data'),
                  prevent_initial_call=True
                  )
        def tab_params_callback(file_val,
                                folder_val,
                                disp_sig_val,
                                emission_folder_val,
                                emission_file_val,
                                trigger,
                                trigger_date,
                                fs_val,
                                samples_val,
                                prf_val,
                                averages_val,
                                delay_val,
                                repetitions_val,
                                repetition_period_val,
                                gero_em_vals, gero_em_ids,
                                gero_re_vals, gero_re_ids,
                                tab_config,
                                config):
            # initial data object, to have the necessary dict fields
            if tab_config is None:
                tab_config = copy.deepcopy(GeroConfig.DEFAULT_TAB_CONFIG)
            else:
                tab_config = copy.deepcopy(tab_config)

            # geronimo emission channels
            for k in range(len(gero_em_vals)):
                arg_value, arg_id = gero_em_vals[k], gero_em_ids[k]
                ch = arg_id['ch']  # geronimo channel (starts at 0)
                gero_ind = arg_id['gero']  # geronimo index (in case multiple geronimos are connected)
                if len(config['options_config']['geronimo_nb_ch']) == 1:  # single geronimo mode
                    ch_str = f'E{ch + 1}'
                else:
                    ch_str = f'G{gero_ind + 1}:E{ch + 1}'
                if arg_value == 'ON' and ch_str not in tab_config['emission_ch']:
                    tab_config['emission_ch'].append(ch_str)
                elif arg_value == 'OFF' and ch_str in tab_config['emission_ch']:
                    tab_config['emission_ch'] = \
                        [ch_strs for ch_strs in tab_config['emission_ch'] if ch_strs != ch_str]
            tab_config['emission_ch'] = sorted(tab_config['emission_ch'])

            # geronimo reception channels
            nb_ch_tot = sum(config['options_config']['geronimo_nb_ch'])
            tab_config['reception_ch'] = [None for _ in range(nb_ch_tot)]
            for k in range(len(gero_re_vals)):
                arg_value, arg_id = gero_re_vals[k], gero_re_ids[k]
                gero_ind = arg_id['gero']  # geronimo index (in case multiple geronimos are connected)
                ch_ind = arg_id['ch']  # geronimo channel (starts at 0)
                ch_ind += sum(config['options_config']['geronimo_nb_ch'][:gero_ind])
                tab_config['reception_ch'][ch_ind] = arg_value

            # trigger timestamp
            try:
                trigger_timestamp = int(datetime.strptime(trigger_date, '%Y-%m-%d %H:%M:%S').timestamp())
            except ValueError:
                trigger_timestamp = None

            # fields
            tab_config['file_name'] = file_val  # save file name
            tab_config['file_folder'] = folder_val  # save file folder
            tab_config['disp_signals'] = bool(disp_sig_val)
            tab_config['signal_folder'] = emission_folder_val  # emission signal file folder
            tab_config['signal_file'] = emission_file_val  # emission signal file name
            tab_config['trigger'] = trigger  # trigger mode
            tab_config['trigger_timestamp'] = trigger_timestamp  # trigger timestamp for date mode
            tab_config['fs'] = fs_val  # sampling frequency
            tab_config['samples'] = samples_val  # number of samples
            tab_config['prf'] = prf_val  # pulse repetition frequency
            tab_config['averages'] = averages_val  # number of averages
            tab_config['delay'] = 1e-3 * delay_val  # delay (conversion from ms to s)
            tab_config['repetitions'] = repetitions_val  # number of repetitions of same config
            tab_config['repetition_period'] = repetition_period_val  # minimum period between repetitions of same config

            return tab_config

        # all parameters callback
        @callback(Output('store-config', 'data', allow_duplicate=True),
                  Input({'type': 'tab-param-store', 'index': ALL}, 'data'),
                  Input({'type': 'tab-param-store', 'index': ALL}, 'id'),
                  State('tabs', 'children'),
                  State('store-config', 'data'),
                  prevent_initial_call=True)
        def params_callback(tab_datas, tab_datas_ids, tabs_children, data):
            # initial data object
            data['tabs_configs'] = [None] * len(tab_datas)

            # configs tab ids
            configs_ids = [tab['props']['tab_id'] for tab in tabs_children]

            # loop over tabs (configs)
            for k in range(len(tab_datas)):
                tab_data = copy.deepcopy(tab_datas[k])
                config = configs_ids.index(tab_datas_ids[k]['index'])
                data['tabs_configs'][config] = tab_data

            return data

        # max number of points limit callback
        @callback(
            Output({'type': 'input-samples', 'index': MATCH}, 'max'),
            Input({'type': 'tab-param-store', 'index': MATCH}, 'data')
        )
        def max_points_limit(tab_config: dict):
            n_ch = max(sum([e != 'OFF' for e in tab_config['reception_ch']]), 1)
            return min(GeroParam.MAX_RECEPTION_SAMPLES_CH, GeroParam.MAX_RECEPTION_SAMPLES_TOT / n_ch)

        # warning number of displayed points
        @callback(
            Output('popup-warning-nb-pts', 'children'),
            Output('popup-warning-nb-pts', 'is_open'),
            Output('store-displayed-points', 'data'),
            Input('store-config', 'data'),
            State('store-displayed-points', 'data'),
        )
        def warning_nb_points(config: dict, n_points_prev: int):
            n_points = 0
            for tab_config in config['tabs_configs']:
                n_ch_rec = sum([e != 'OFF' for e in tab_config['reception_ch']])
                n_ch_em = max(len(tab_config['emission_ch']), 1)
                n_samples = 0 if tab_config['samples'] is None else tab_config['samples']
                n_points += n_samples * n_ch_rec * n_ch_em * tab_config['disp_signals']
            if n_points > n_points_prev and n_points > AcquisitionPanel.MAX_POINTS_WARNING:
                msg = ('Warning:\nVery large number of points to be displayed, may cause graphical interface lag, '
                       'consider deactivating signals display')
                return msg, True, n_points
            else:
                return '', False, n_points

        # start callback
        @callback(
            output=[Output('tabs', 'active_tab', allow_duplicate=True),
                    Output('popup-error-shot', 'children'),
                    Output('popup-error-shot', 'is_open'),
                    Output('store-file-added', 'data')] +
                   [Output(f'graph-{k_g}', 'figure', allow_duplicate=True) for k_g in range(AcquisitionPanel.MAX_FIGS)],
            progress=[Output('tabs', 'active_tab', allow_duplicate=True),
                      Output('label-start', 'children', allow_duplicate=True),
                      Output('interval-remaining-time', 'n_intervals'),
                      Output('interval-remaining-time', 'max_intervals')] +
                     [Output(f'graph-{k_g}', 'figure', allow_duplicate=True) for k_g in range(AcquisitionPanel.MAX_FIGS)],
            inputs=(Input('btn-start', 'n_clicks'),
                    State('store-config', 'data'),
                    State('tabs', 'children'),
                    State('tabs', 'active_tab'),
                    [State(f'graph-{k_g}', 'figure') for k_g in range(AcquisitionPanel.MAX_FIGS)],
                    State('store-graphs-ind', 'data')
                    ),
            background=True,
            running=[
                (Output('btn-start', 'disabled'), True, False),
                (Output('btn-stop', 'disabled'), False, True),
                (Output('label-start', 'children', allow_duplicate=True), [], []),
                (Output('store-auto-save-config', 'data'), 0, 1)
            ],
            cancel=[Input('btn-stop', 'n_clicks')],
            prevent_initial_call=True
        )
        def start_acquisition(progress_func, _, param_data, tabs_children, original_tab, graphs, graphs_ind):
            logging.info('start of acquisition')

            # acquisitions list
            if param_data['options_config']['acq_loop'] == 'all':
                tabs_list = range(len(param_data['tabs_configs']))
            else:
                for k_tab in range(len(tabs_children) - 1):
                    if tabs_children[k_tab]['props']['tab_id'] == original_tab:
                        break
                tabs_list = [k_tab]

            # graphs reset
            # progress_func([original_tab, 'Initialization'] + graphs)  # may take time
            for k_tab in tabs_list:
                k_graph = graphs_ind['tabs_graphs'][k_tab]
                graphs[k_graph]['data'] = []
                graphs[k_graph]['layout']['title'] = ''

            # checking if inputs are ok
            progress_func([original_tab, 'Initialization', 0, 0] + graphs)  # gui update
            for k_tab in tabs_list:
                config = param_data['tabs_configs'][k_tab]
                # checking if inputs are empty
                for (param, param_val) in zip(config.keys(), config.values()):
                    if param_val is None:
                        if param == 'signal_file' and not config['emission_ch']:
                            config['signal_file'] = ''
                        else:
                            error_str = f'Missing parameter(s) (Acquisition {k_tab+1}, {param})'
                            logging.error(f'Missing parameter(s) (Acquisition {k_tab+1}, {param})')
                            return original_tab, error_str, True, [], *graphs
                # checking if save file folder exists
                file_folder = config['file_folder']
                if not os.path.isabs(file_folder):
                    file_folder = os.path.join(param_data['options_config']['base_folder'], file_folder)
                flag = check_saving_folder(file_folder)
                if flag != 0:
                    logging.error(flag)
                    return original_tab, flag, True, [], *graphs

            # emission signals loading
            progress_func([original_tab, 'Loading emission signals', 0, 0] + graphs)  # gui update
            emission_sigs = [(None, None)] * len(param_data['tabs_configs'])
            for k_tab in tabs_list:
                config = param_data['tabs_configs'][k_tab]
                save_folder = config['signal_folder']
                if not os.path.isabs(save_folder):
                    save_folder = os.path.join(param_data['options_config']['base_folder'], save_folder)
                signal_file_path = os.path.join(save_folder, config['signal_file'])
                if not os.path.isabs(signal_file_path):
                    signal_file_path = os.path.join(param_data['options_config']['base_folder'], signal_file_path)
                try:
                    emission_sigs[k_tab] = AcquisitionPanel.upload_emission_signal(signal_file_path)
                except Exception as e:
                    if config['emission_ch']:
                        error_str = f'Emission signal file not found or corrupted in Acquisition {k_tab+1}'
                        error_str += ' (' + type(e).__name__ + ': ' + str(e) + ')'
                        logging.error(traceback.format_exc())
                        return original_tab, error_str, True, [], *graphs
                    else:
                        emission_sigs[k_tab] = (np.zeros(1), GeroParam.EMISSION_FS[0])

            # geronimo connection checking
            back_front_interface = GeronimoGeneralInterface(param_data['options_config']['geronimo_address'],
                                                            param_data['options_config']['geronimo_nb_ch'])
            if not all(back_front_interface.check_connection()[0]):
                logging.error('Geronimo not connected')
                return original_tab, 'Geronimo not connected', True, [], *graphs

            # acquisitions
            files_folders = []  # for acquisition panel auto get/add
            for k_acq in tabs_list:  # config tabs loop
                tab_id = tabs_children[k_acq]['props']['tab_id']
                config = param_data['tabs_configs'][k_acq]
                if not config['emission_ch']:  # no emission channel
                    config['emission_ch'] = ['E*']
                t0_rep = None  # repetition start time
                graph_title_strs = []
                for k_rep in range(config['repetitions']):
                    # pause for minimal repetition period time
                    if t0_rep is not None and config['trigger'] == 'date':
                        config['trigger_timestamp'] += config['repetition_period']
                    elif t0_rep is not None and time.time() < t0_rep + config['repetition_period']:
                        progress_func([tab_id, f'Waiting before repetition {k_rep+1}/{config["repetitions"]}',
                                       0, int(t0_rep + config['repetition_period'] - time.time()) + 1] + graphs)
                        time.sleep(max(t0_rep + config['repetition_period'] - time.time(), 0))
                    # date trigger verification
                    if config['trigger'] == 'date' and time.time() >= config['trigger_timestamp'] - 2:
                        e_str = 'Shot trigger date has passed'
                        logging.error(e_str)
                        return original_tab, e_str, True, files_folders, *graphs
                    t0_rep = time.time()
                    # repetition signal data arrays
                    sigs, timestamps, t0 = [], [], None
                    for emission_ch in config['emission_ch']:  # emission channels loop
                        if param_data['options_config']['acq_saving_mode'] == 'each':
                            sigs, timestamps, t0 = [], [], None
                        # signal acquisition
                        for k_attemps in range(AcquisitionPanel.MAX_ACQ_ATTEMPTS):  # multiple shot attempts (logout, .)
                            # configuration string
                            config_str = f'Acquisition {k_acq + 1} ; {emission_ch}'
                            config_str += f' ; {k_rep + 1}/{config["repetitions"]}' if config['repetitions'] > 1 else ''
                            config_str_attempt = config_str + (f' (attempt #{k_attemps + 1})' if k_attemps > 0 else '')
                            # trigger exceptions
                            config_trig = copy.deepcopy(config)
                            match config_trig['trigger']:
                                case 'internal':
                                    pass
                                case 'external':
                                    if emission_ch != config['emission_ch'][0]:
                                        config_trig['trigger'] = 'internal'  # looping over emission ch after ext. trig.
                                case 'date':
                                    if len(config['emission_ch']) > 1:
                                        if len(param_data['options_config']['geronimo_address']) > 1:
                                            e_str = ('Cannot set multiple emission channels with multiple synchronized '
                                                     'geronimos on date trigger')
                                            logging.error(e_str)
                                            return original_tab, e_str, True, files_folders, *graphs
                                        elif emission_ch != config['emission_ch'][0]:
                                            config_trig['trigger'] = 'internal'  # looping over em. ch after date trig.
                            # countdown time
                            t_shot = int(config['averages'] / config['prf']) + 1
                            match config_trig['trigger']:
                                case 'internal':
                                    pass
                                case 'external':
                                    t_shot = 0
                                case 'date':
                                    t_shot += max(config['trigger_timestamp'] - time.time(), 0)
                            t_shot = t_shot if t_shot > 2 else 0  # minimal countdown time display
                            t_shot = int(t_shot)
                            progress_func([tab_id, config_str_attempt, 0, t_shot] + graphs)  # gui update
                            try:
                                shot_id = uuid.uuid4().fields[0]
                                back_front_interface.set_acquisition(config_trig, emission_ch, emission_sigs[k_acq],
                                                                     shot_id)
                                sig, timestamp = back_front_interface.get_acquisition_data(config_trig, shot_id)
                                break
                            except Exception as e:
                                logging.warning(traceback.format_exc())
                                if k_attemps == AcquisitionPanel.MAX_ACQ_ATTEMPTS - 1:
                                    logging.error(f'shot dropped after {k_attemps + 1} failed attempt(s)')
                                    return original_tab, type(e).__name__ + ': ' + str(e), True, files_folders, *graphs
                                else:
                                    time.sleep(1)
                                    if not all(back_front_interface.check_connection()[0]):
                                        logging.warning('geronimo disconnected')
                                    else:
                                        logging.warning('geronimo connected')
                        # previous repetition graph removal
                        if emission_ch == config['emission_ch'][0]:
                            graph_title_strs = []
                            k_graph = graphs_ind['tabs_graphs'][k_tab]
                            graphs[k_graph]['data'] = []
                            graphs[k_graph]['layout']['title'] = ''
                        # display variables update
                        sigs.append(sig)
                        timestamps.append(datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S.%f'))
                        graph_title_strs.append(emission_ch + datetime.fromtimestamp(timestamp).strftime(' (%H:%M:%S)'))

                        # saving
                        # file path
                        file_folder = config['file_folder']
                        if not os.path.isabs(file_folder):
                            file_folder = os.path.join(param_data['options_config']['base_folder'], file_folder)
                        t0 = datetime.fromtimestamp(timestamp) if t0 is None else t0
                        y, m, d, H, M, S = t0.year, t0.month, t0.day, t0.hour, t0.minute, t0.second
                        y, m, d, H, M, S = f'{y}', f'{m:02d}', f'{d:02d}', f'{H:02d}', f'{M:02d}', f'{S:02d}'
                        acq, rep = k_acq + 1, k_rep + 1
                        signame = os.path.splitext(str(config['signal_file']))[0]
                        file_name = config['file_name'].format(y=y, m=m, d=d, H=H, M=M, S=S, acq=acq, rep=rep,
                                                               signame=signame)
                        if param_data['options_config']['acq_saving_mode'] == 'each':
                            file_name += '_' + emission_ch.replace(':', '').replace('*', 'x')
                        file_path = os.path.join(file_folder, file_name + SAVE_EXTENSION)
                        # metadata
                        saving_config = copy.deepcopy(config)
                        saving_config['timestamps'] = timestamps
                        saving_config['geronimo_address'] = param_data['options_config']['geronimo_address']
                        saving_config['geronimo_nb_ch'] = param_data['options_config']['geronimo_nb_ch']
                        saving_config['user_data'] = param_data['options_config']['user_data']
                        if param_data['options_config']['acq_saving_mode'] == 'each':
                            saving_config['emission_ch'] = [emission_ch]
                        override = (param_data['options_config']['acq_saving_mode'] == 'all' and
                                    emission_ch != config['emission_ch'][0])
                        flag = save_signal_to_file(file_path, sigs, saving_config, override=override)
                        if flag != 0:  # saving error
                            logging.error('Saving error: ' + flag)
                            return original_tab, 'Saving error: ' + flag, True, files_folders, *graphs

                        # figure updating
                        for k_rec_sig in range(sig.shape[0]):
                            if not config['disp_signals']:
                                continue
                            k_rec = int(np.arange(len(config['reception_ch']))
                                        [[el != 'OFF' for el in config['reception_ch']]][k_rec_sig])
                            if len(param_data['options_config']['geronimo_nb_ch']) == 1:  # single geronimo mode
                                reception_ch = f'R{k_rec+1}'
                            else:  # multiple synchronized geronimos mode
                                nb_ch = copy.deepcopy(param_data['options_config']['geronimo_nb_ch'])
                                k_gero = 0
                                while k_rec >= nb_ch[0]:
                                    k_gero += 1
                                    k_rec -= nb_ch.pop(0)
                                reception_ch = f'G{k_gero + 1}:R{k_rec + 1}'
                            k_graph = graphs_ind['tabs_graphs'][k_acq]
                            graphs[k_graph]['data'].append(go.Scatter(
                                x=1e3 * np.arange(sig.shape[1]) / config['fs'] + 1e3 * config['delay'],
                                y=sig[k_rec_sig, :], mode='lines', name=f'{emission_ch} {reception_ch}'))
                            graphs[k_graph]['layout']['title'] = ' ; '.join(graph_title_strs)

                        # files folders updating, for acquisition panel auto get/add
                        if emission_ch == config['emission_ch'][-1]:
                            files_folders.append(file_folder)

                        # gui update
                        if emission_ch == config['emission_ch'][-1] and k_rep == config['repetitions'] - 1:
                            progress_func([tab_id, config_str + ' (saved)', 0, 0] + graphs)
                            time.sleep(2)  # waiting, for display before moving to next tab
                if config['emission_ch'] == ['E*']:
                    config['emission_ch'] = []
            logging.info('end of acquisition')
            return original_tab, '', False, files_folders, *graphs

        # remaining time during shot callback
        @callback(
            Output('label-start', 'children'),
            Input('interval-remaining-time', 'n_intervals'),
            State('interval-remaining-time', 'max_intervals'),
            State('label-start', 'children'),
            State('btn-start', 'disabled'),
        )
        def update_remaining_time(n_intervals: int, max_intervals: int, label: str, start_btn_disabled: bool):
            # no label
            if not label:
                return label
            # acquisition ended (in case stop button is clicked right when function is called)
            if not start_btn_disabled:
                return ''
            # removing previous remaining time in label
            label_end = label.split('(')[-1]
            if (len(label) - len(label_end)) >= 2 and re.match(r'(\d\d:\d\d|\d+\d:\d\d:\d\d)\)$', label_end):
                label = label[:-len(label_end)-2]
            # adding current remaining time
            s = max_intervals - n_intervals - 1
            if s > 0:
                m, s = s // 60, s % 60
                h, m = m // 60, m % 60
                label += ' ('
                if h > 0:
                    label += f'{h:02d}:'
                label += f'{m:02d}:{s:02d})'
            return label

    # empty figures creation
    def get_empty_figure(self):
        fig = go.Figure()
        fig.update_xaxes(title_text='Time [ms]')
        fig.update_yaxes(title_text='Amplitude [V]', range=[-5, 5])
        fig.update_layout(margin={'l': 10, 'r': 10, 't': 30, 'b': 10})
        fig.update_layout(legend={'yanchor': 'top', 'y': 0.99, 'xanchor': 'right', 'x': 0.99})
        fig.update_layout(title='')
        fig.update_layout(showlegend=True)
        return fig

    # tab creation function
    def create_acq_tab(self, tabs_children, tab_config=None, opt_config=None, active_tab=None, graphs_ind=None,
                       hidden_graph_container_children=None):
        # default arguments
        if tab_config is None:
            tab_config = copy.deepcopy(GeroConfig.DEFAULT_TAB_CONFIG)
        if opt_config is None:
            opt_config = copy.deepcopy(GeroConfig.DEFULT_OPTIONS_CONFIG)
        if graphs_ind is None:
            graphs_ind = copy.deepcopy(self.graphs_ind.data)
        if hidden_graph_container_children is None:
            hidden_graph_container_children = copy.deepcopy(self.hidden_graphs_container.children)

        # max number of active tabs
        if not graphs_ind['available_graphs']:  # empty list, no graph available
            return tabs_children, active_tab, graphs_ind, hidden_graph_container_children

        tab_id = 'tab-' + str(time.time()).replace('.', '-')

        # config parameters storing object
        store_obj = dcc.Store(id={'type': 'tab-param-store', 'index': tab_id}, data=tab_config)

        # close button
        close_btn = html.Button(  # closing button
            id={'type': 'close-btn', 'index': tab_id},
            children='Delete acquisition',
            style={'margin': 10},
            className='red-button',
            title='Delete current acquisition tab'
        )

        # parameters Div
        param_lab_width = 230  # labels width
        param_inp_width = 120  # inputs width
        param_margin = 5
        params_div = html.Div([
            dcc.Interval(id={'type': 'interval-update-date-color', 'index': tab_id}, max_intervals=0),
            html.Div([
                dcc.Checklist(id={'type': 'check-display-sig', 'index': tab_id}, options={'disp': ' Display signals'},
                              value=['disp'] * tab_config['disp_signals'],
                              style={'margin-left': param_margin, 'margin-top': 0, 'margin-bottom': 0,
                                     'padding-top': 10})
            ], title='Disable if lagging for large data', style={'text-align': 'right', 'height': 0}),
            html.H3('Parameters'),
            html.Div([
                html.Label('Save file folder:', style={'width': param_lab_width, 'display': 'inline-block'}),
                html.Br(),
                dcc.Input(id={'type': 'input-folder', 'index': tab_id}, type='text', value=tab_config['file_folder'],
                          style={'width': 350}),
            ], style={'margin': param_margin, 'margin-bottom': 0},
                title='Relative (from project base folder) and absolute paths allowed'),
            html.Div([
                html.Label('Save file name:', style={'width': param_lab_width, 'display': 'inline-block'}),
                html.Br(),
                dcc.Input(id={'type': 'input-file', 'index': tab_id}, type='text', value=tab_config['file_name'],
                          list='datalist-input-file', style={'width': 350}),
                html.Datalist(id='datalist-input-file',
                              children=[html.Option(value=GeroConfig.DEFAULT_TAB_CONFIG['file_name'])])
            ], style={'margin': param_margin, 'margin-bottom': 0},
                title='"{y}{m}{d}_{H}{M}{S}" for acquisition date & time\n"{acq}" for acquisition index\n'
                      '"{rep}" for repetition index\n"{signame}" for emission signal file name\n'
                      '"_Ek" appended if one file per emission channel\nOverwriting not allowed'),
            html.Div([
                html.Label('Emission signal folder:', style={'width': param_lab_width, 'display': 'inline-block'}),
                html.Br(),
                dcc.Input(id={'type': 'input-signal-folder', 'index': tab_id}, type='text',
                          value=tab_config['signal_folder'], style={'width': 350}),
            ], style={'margin': param_margin}, title='Relative (from project base folder) and absolute paths allowed'),
            html.Div(id={'type': 'div-drop-signal-file', 'index': tab_id}, children=[
                html.Label('Emission signal file:', style={'width': param_lab_width, 'display': 'inline-block'}),
                html.Br(),
                dcc.Dropdown(id={'type': 'drop-signal-file', 'index': tab_id}, options=[],
                             value=tab_config['signal_file'], maxHeight=400, style={'width': 350}),
            ], style={'margin': param_margin}),
            html.Div([
                html.Div([
                    html.Label('Geronimo channels:', style={'width': param_lab_width, 'display': 'inline-block'}),
                ], style={'margin': param_margin, 'margin-bottom': 0}),
                html.Div(id={'type': 'div-gero-io', 'index': tab_id}, children=[
                    GeronimosIO(id={'type': 'gero-io', 'index': tab_id}, nb_ch=opt_config['geronimo_nb_ch'],
                                emission_ch=tab_config['emission_ch'], reception_ch=tab_config['reception_ch'])
                ]),
            ], title='Emission channels triggered consecutively, not simultaneously\n'
                     'No emission channel still triggers passive measurement ("E*" emission channel)'),
            html.Div([
                html.Label('Trigger mode:', style={'width': param_lab_width, 'display': 'inline-block'}),
                dcc.Dropdown(id={'type': 'drop-trig', 'index': tab_id}, value=tab_config['trigger'],
                             options={'internal': 'Internal', 'external': 'External', 'date': 'Date'}, clearable=False,
                             style={'width': param_inp_width, 'display': 'inline-block', 'verticalAlign': 'middle'})
            ], style={'margin': param_margin},
                title='Shot initiated by trigger:\n'
                      '- Internal: shot starts immediately after "Start" button is pressed\n'
                      '- External: shot starts when external trigger wavefront is detected\n'
                      '- Date: shot starts at specified date and time\n'),
            html.Div(id={'type': 'div-timestamp', 'index': tab_id}, children=[
                html.Label('Trigger date:', style={'width': (param_lab_width + param_inp_width)/2,
                                                   'display': 'inline-block'}),
                dcc.Input(id={'type': 'input-timestamp', 'index': tab_id}, required=False,
                          value=datetime.fromtimestamp(tab_config['trigger_timestamp'])
                          .strftime('%Y-%m-%d %H:%M:%S'),
                          placeholder='yyyy-mm-dd HH:MM:SS', style={'width': (param_lab_width + param_inp_width)/2}),
            ], style={'margin': param_margin, 'width': '100%'}, hidden=tab_config['trigger'] != 'date'),
            html.Div([
                html.Label('Sampling frequency:', style={'width': param_lab_width, 'display': 'inline-block'}),
                dcc.Dropdown(id={'type': 'drop-fs', 'index': tab_id}, value=tab_config['fs'], clearable=False,
                             style={'width': param_inp_width, 'display': 'inline-block', 'verticalAlign': 'middle'},
                             options=[{'value': 2E6, 'label': '2 MHz'}, {'value': 1E6, 'label': '1 MHz'},
                                      {'value': 0.5E6, 'label': '500 kHz'}])
            ], style={'margin': param_margin}),
            html.Div([
                html.Label('Samples:', style={'width': param_lab_width, 'display': 'inline-block'},
                           id={'type': 'label-samples', 'index': tab_id}),
                dcc.Input(id={'type': 'input-samples', 'index': tab_id}, type='number', min=0, step=1,
                          max=GeroParam.MAX_RECEPTION_SAMPLES_CH, required=True, value=tab_config['samples'],
                          style={'width': param_inp_width})
            ], style={'margin': param_margin}),
            html.Div([
                html.Label('Delay [ms]:', style={'width': param_lab_width, 'display': 'inline-block'},
                           id={'type': 'label-delay', 'index': tab_id}),
                dcc.Input(id={'type': 'input-delay', 'index': tab_id}, type='number', min=0,
                          style={'width': param_inp_width}, required=True,
                          value=1e3 * GeroConfig.DEFAULT_TAB_CONFIG['delay'])
            ], style={'margin': param_margin},
                title='Delay between emission and reception\n'
                      '(no delay means reception starts at beginning of emission signal)\n'
                      'May be negative (recording data before shot)'),
            html.Div([
                html.Label('Averages:', style={'width': param_lab_width, 'display': 'inline-block'}),
                dcc.Input(id={'type': 'input-averages', 'index': tab_id}, type='number',
                          min=1, max=GeroParam.MAX_AVERAGES, step=1, required=True, value=tab_config['averages'],
                          style={'width': param_inp_width})
            ], style={'margin': param_margin}, title='Number of shots used for average'),
            html.Div([
                html.Label('PRF [Hz]:', style={'width': param_lab_width, 'display': 'inline-block'}),
                dcc.Input(id={'type': 'input-prf', 'index': tab_id}, type='number',
                          style={'width': param_inp_width}, min=GeroParam.MIN_PRF, max=GeroParam.MAX_PRF,
                          required=True, value=tab_config['prf'])
            ], style={'margin': param_margin}, title='Pulse Repetition Frequency'),
            html.Div([
                html.Label('Repetitions:', style={'width': param_lab_width, 'display': 'inline-block'},
                           id={'type': 'label-repetitions', 'index': tab_id}),
                dcc.Input(id={'type': 'input-repetitions', 'index': tab_id}, type='number', min=1, step=1,
                          style={'width': param_inp_width}, required=True, value=tab_config['repetitions'])
            ], style={'margin': param_margin}, title='Number of times acquisition is performed\n'
                                                     'Saved in separate files'),
            html.Div(id={'type': 'div-repetition-period', 'index': tab_id}, children=[
                html.Label('Repetition min. period [s]:', style={'width': param_lab_width, 'display': 'inline-block'},
                           id={'type': 'label-repetition-period', 'index': tab_id}),
                dcc.Input(id={'type': 'input-repetition-period', 'index': tab_id}, type='number',
                          style={'width': param_inp_width}, min=0, required=True, value=tab_config['repetition_period'])
            ], style={'margin': param_margin}, title='Minimum time period between repetitions\n'
                                                     'May be longer if repetitions take more time',
                     hidden=tab_config['repetitions'] == 1),
            close_btn,
        ], style={'display': 'inline-block'})

        # plot Div
        k_graph = graphs_ind['available_graphs'].pop(0)  # index of available graph
        graphs_ind['tabs_graphs'].append(k_graph)
        plot_div = html.Div(children=[
            self.all_graphs[k_graph]  # adding graph to tab
        ], style={'width': '100%', 'margin-left': 30, 'display': 'inline-block'})
        for k_g in range(len(hidden_graph_container_children)):  # removing graph from hidden div container
            if type(hidden_graph_container_children[k_g]) is dict:
                graph_id = hidden_graph_container_children[k_g]['props']['id']
            else:
                graph_id = hidden_graph_container_children[k_g].id
            if graph_id == f'graph-{k_graph}':
                hidden_graph_container_children.pop(k_g)
                break

        # tab creation
        new_tab = dbc.Tab(tab_id=tab_id, label=f'Acquisition {len(tabs_children)}', children=[html.Div([
            store_obj,
            params_div,
            plot_div
        ], style={'display': 'flex', 'height': '90vh', 'padding': 10})])

        # adding tab to the tabs
        return tabs_children[:-1] + [new_tab.to_plotly_json()] + [tabs_children[-1]], tab_id, graphs_ind, \
            hidden_graph_container_children

    @staticmethod
    def upload_emission_signal(file_path: str) -> (np.ndarray[float] | None, float | None):
        """
        Uploads emission signal data from csv file.
        :param file_path: emission signal file path
        :return: emission signal array, as emission_sig[idx_time]
        """
        with open(file_path, 'r') as file:
            data_str = file.read()
        emission_sig = SignalPanel.read_csv_str(data_str)
        return emission_sig

    @staticmethod
    def get_emission_folder_files(folder, base_folder):
        if not os.path.isabs(folder):
            folder = os.path.join(base_folder, folder)
        if os.path.isdir(folder):
            emission_file_opt = [file for file in os.listdir(folder) if file.endswith('.csv')]
            # radix sort
            emission_file_split = []
            for file in emission_file_opt:
                l_file = []
                for s in file:
                    if not l_file:
                        l_file.append(s)
                    elif l_file[-1][-1].isdigit() != s.isdigit():
                        l_file.append(s)
                    else:
                        l_file[-1] += s
                for ks in range(len(l_file)):
                    if l_file[ks].isnumeric():
                        l_file[ks] = '%#040.20f' % float(l_file[ks])
                emission_file_split.append(l_file)
            emission_file_opt = [i[0] for i in sorted(zip(emission_file_opt, emission_file_split), key=lambda x: x[1])]
            # # last file
            # files_ctime = [os.path.getctime(os.path.join(folder, file)) for file in emission_file_opt]
            # k_last = np.argmax(files_ctime)
            # if time.time() - files_ctime[k_last] <= 5 * 60:
            #     last_file = {
            #         'label': html.Span([emission_file_opt[k_last] + ' (new)'], style={'color': 'green'}),
            #         'value': emission_file_opt[k_last],
            #     }
            #     emission_file_opt = [{'label': file, 'value': file} for file in emission_file_opt]
            #     emission_file_opt = [last_file] + emission_file_opt
        else:
            emission_file_opt = [{
                'label': html.Span(['Folder does not exist'], style={'color': 'red'}),
                'value': 'Folder does not exist',
                'search': 'Folder does not exist',
                'disabled': True
            }]
        return emission_file_opt


class GeronimosIO(html.Div):
    def __init__(self, nb_ch: list[int], emission_ch: list[str] = None, reception_ch: list[str] = None, **kwargs):
        super().__init__(**kwargs)
        if len(nb_ch) == 1:  # single geronimo mode
            self.children = self._get_geroIO(nb_ch=nb_ch[0], parent_id=self.id, emission_ch=emission_ch,
                                             reception_ch=reception_ch)
        else:  # multiple synchronized geronimos mode
            self.children = dcc.Tabs(children=[])
            for gero_ind in range(len(nb_ch)):
                emission_ch_gero = [ch for ch in emission_ch if ch.startswith(f'G{gero_ind + 1}:')]
                emission_ch_gero = [ch.split(':')[-1] for ch in emission_ch_gero]
                reception_ch_gero = reception_ch[sum(nb_ch[:gero_ind]):sum(nb_ch[:gero_ind+1])]
                self.children.children.append(
                    dcc.Tab(label=f'G{gero_ind+1}', children=
                            self._get_geroIO(nb_ch=nb_ch[gero_ind], gero_ind=gero_ind, parent_id=self.id,
                                             emission_ch=emission_ch_gero,
                                             reception_ch=reception_ch_gero),
                            style={'padding': 0, 'line-height': 30, 'line-width': 30},
                            selected_style={'padding': 0, 'line-height': 30, 'line-width': 40,
                                            'backgroundColor': 'darkgrey', 'border-left': 'darkgrey',
                                            'border-right': 'darkgrey', 'border-top': 'darkgrey'})
                )

    @staticmethod
    def _get_geroIO(nb_ch: int, **kwargs):
        match nb_ch:
            case 8:
                return Geronimo8chIO(**kwargs)
            case _:
                raise NotImplementedError('only 8 channels geronimo implemented')


class Geronimo8chIO(html.Div):
    btn_margin = - 9
    RECEPT_DICT = [
        {'value': 'OFF', 'label': html.Span('OFF', style={'margin': btn_margin})},
        {'value': '0dB', 'label': html.B('0dB', style={'margin': btn_margin})},
        {'value': '20dB', 'label': html.B('20dB', style={'margin': btn_margin})},
        {'value': '40dB', 'label': html.B('40dB', style={'margin': btn_margin})},
        {'value': '60dB', 'label': html.B('60dB', style={'margin': btn_margin})},
    ]
    EMIT_DICT = [
        {'value': 'OFF', 'label': html.Span('OFF', style={'margin': btn_margin})},
        {'value': 'ON', 'label': html.B('ON', style={'margin': btn_margin})},
    ]

    # on/off dropdown toggle behavior
    @staticmethod
    @callback(
        Output({'type': 'gero-emission', 'index': MATCH, 'ch': MATCH, 'gero': MATCH}, 'value', allow_duplicate=True),
        Input({'type': 'div-gero-emission', 'index': MATCH, 'ch': MATCH, 'gero': MATCH}, 'n_clicks'),
        State({'type': 'gero-emission', 'index': MATCH, 'ch': MATCH, 'gero': MATCH}, 'value'),
        prevent_initial_call=True
    )
    def toggle_emission_buttons(_, val: str):
        return ['ON', 'OFF'][val == 'ON']

    # on/off all channels toggle
    @staticmethod
    @callback(
        Output({'type': 'gero-emission', 'index': MATCH, 'ch': ALL, 'gero': MATCH}, 'value', allow_duplicate=True),
        Input({'type': 'div-gero-emission-all', 'index': MATCH, 'gero': MATCH}, 'n_clicks'),
        State({'type': 'gero-emission', 'index': MATCH, 'ch': ALL, 'gero': MATCH}, 'value'),
        prevent_initial_call=True
    )
    def toggle_all_emission_buttons(_, val: list[str]):
        on_new = sum([v == 'ON' for v in val]) <= len(val) / 2
        new_val = [['OFF', 'ON'][on_new] for _ in range(len(val))]
        return new_val

    # gain all channels toggle
    @staticmethod
    @callback(
        Output({'type': 'gero-reception', 'index': MATCH, 'ch': ALL, 'gero': MATCH}, 'value', allow_duplicate=True),
        Input({'type': 'div-gero-reception-all', 'index': MATCH, 'gero': MATCH}, 'n_clicks'),
        State({'type': 'gero-reception', 'index': MATCH, 'ch': ALL, 'gero': MATCH}, 'value'),
        prevent_initial_call=True
    )
    def toggle_all_reception_buttons(_, val: list[str]):
        gain_val = [g['value'] for g in Geronimo8chIO.RECEPT_DICT]
        gain_occ = [val.count(g) for g in gain_val]
        new_gain = (np.argmax(gain_occ) + 1) % len(gain_val)
        new_val = [gain_val[new_gain] for _ in range(len(val))]
        return new_val

    def __init__(self, parent_id: dict, gero_ind: int = 0, emission_ch: list[str] = None,
                 reception_ch: list[str] = None):
        """
        Graphical interface for emission/reception channels selection.
        :param gero_ind: geronimo index (0 if only geronimo connected)
        :param emission_ch: list of the active emission channels ('E1', 'E2', etc.)
        :param reception_ch: list of the reception channels states ('off', '0dB', '20dB', '40dB', '60dB')
        """
        btn_width = 40
        btn_all_width = 25

        # superclass init
        super().__init__()  # style={'backgroundColor': 'darkgrey'},

        # inputs
        if emission_ch is None:
            emission_ch = []
        if reception_ch is None:
            reception_ch = ['OFF'] * 8

        # display
        io_panel = html.Div(style={'padding': 3, 'padding-top': 2, 'padding-bottom': 5,
                                   'width': 8 * (btn_width + 1) + btn_all_width + 3 + 2 * 3, 'display': 'flex',
                                   'backgroundColor': 'darkgrey'}, children=[])  # inside panel
        self.children = io_panel
        drop_style = {'width': btn_width, 'padding': -0*7, 'display': 'inline-block', 'verticalAlign': 'middle'}
        for k in range(8):
            emission_val = ['OFF', 'ON'][f'E{k+1}' in emission_ch]
            emission_ch_drop = html.Div(dcc.Dropdown(
                id={'type': 'gero-emission', 'index': parent_id['index'], 'ch': k, 'gero': gero_ind},
                options=self.EMIT_DICT, value=emission_val, clearable=False, style=drop_style, disabled=True
            ), id={'type': 'div-gero-emission', 'index': parent_id['index'], 'ch': k, 'gero': gero_ind})
            reception_ch_drop = dcc.Dropdown(
                id={'type': 'gero-reception', 'index': parent_id['index'], 'ch': k, 'gero': gero_ind},
                options=self.RECEPT_DICT, value=reception_ch[k], clearable=False, style=drop_style
            )
            ch_div = html.Div([
                html.B(f'E{k+1}'),
                emission_ch_drop,
                html.Div(style={'height': '.5em'}),
                html.B(f'R{k+1}'),
                reception_ch_drop,
            ], style={'margin': 0.5, 'textAlign': 'center'})
            io_panel.children.append(ch_div)
        toggle_btns = []
        for mode in ['emission', 'reception']:
            toggle_btns.append(html.Div(dcc.Dropdown(
                options=[{'value': 0, 'label': html.B('<', style={'margin': -20})}], value=0, disabled=True,
                style={'width': btn_all_width, 'padding': 0, 'display': 'inline-block', 'verticalAlign': 'middle',
                       'backgroundColor': 'darkgrey'}),
                id={'type': f'div-gero-{mode}-all', 'index': parent_id['index'], 'gero': gero_ind}
            ))
        all_div = html.Div([
            html.Br(),
            toggle_btns[0],
            html.Div(style={'height': '.5em'}),
            html.Br(),
            toggle_btns[1],
        ], style={'margin': 1, 'margin-left': 2, 'textAlign': 'center'})
        io_panel.children.append(all_div)
