import copy
import os
import traceback
from dash import html, dcc, Input, Output, State, callback, ALL, MATCH, ctx, Patch, no_update
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import numpy as np
import scipy.signal as sig
import time
import base64
import logging

from geronimo_utilities.saving_module import (get_data_from_bytes, get_data_from_file, convert_signal_to_csv,
                                              SAVE_EXTENSION, OLD_SAVE_EXTENSION)
from custom_signals_tab import CustomSignalsTab
from sig_pan import SignalPanel


class AnalysisPanel(html.Div):
    DEFAULT_REDUC_FACTOR = 100  # default reduction factor for emission signal files

    def __init__(self, config: dict, files: list[str] = None):
        if files is None:
            files = []

        ######################################## display ########################################

        # whole tab
        super().__init__([
            # parameters div
            param_div := html.Div(style={'padding': 0, 'width': 350, 'min-width': 350, 'display': 'inline-block'}),
            # vertical separation
            html.Div(style={'display': 'inline-block', 'height': '96vh', 'width': 0,
                            'border-right': 'solid lightgrey 2px'}),
            # display div
            disp_div := html.Div(style={'padding': 0, 'width': '100%', 'display': 'inline-block'}),
        ], style={'display': 'flex', 'height': '100%'})

        self.files_data: list[dict] = []  # file information and data storage array
        self.signals_data: list[dict] = []  # processed signals storage array

        # parameters div
        param_tabs_style = {'padding': 10}  # , 'width': 350}
        param_margin = 5
        param_div.children = [dbc.Tabs(children=[
            file_tab := dbc.Tab(id='tab-files-analysis', label='Files', style=param_tabs_style),
            win_tab := dbc.Tab(id='tab-win-analysis', label='Windowing', style=param_tabs_style),
            filt_tab := dbc.Tab(id='tab-filt-analysis', label='Filtering', style=param_tabs_style),
            CustomSignalsTab(label='More', config=config, analysis_pan=self, style=param_tabs_style)
        ])]
        # file tab
        config = config['analysis_config']
        file_tab.children = [
            dcc.Store(id='store-file-added', data=[]),  # store for added file flag (triggered by acquisition panel)
            dcc.Store(id='store-analysis-files', data=0),  # store for files data flag
            dcc.Store(id='store-analysis-signals', data=0),  # store for signals data flag
            dcc.Store(id='store-data-folders-debounce', data=None),  # store for data folders inputs debounce behavior
            html.H4('Uploading'),
            html.Label('Direct upload:', style={'margin': param_margin}),
            # html.Br(),
            dcc.Upload(
                id='upload-data-file', children=html.Button('Drop or select file(s)', style={'margin': param_margin}),
                multiple=True, accept=[SAVE_EXTENSION, OLD_SAVE_EXTENSION, '.csv']
            ),
            # html.Br(),
            html.Label('Upload last file from folder:', style={'margin': param_margin}),
            html.Br(),
            html.Div([
                dcc.Input(id='input-last-file-folder', value=config['last_file_folder'],
                          style={'margin': param_margin, 'width': 300})
            ], title='Relative (from project base folder) and absolute paths allowed'),
            html.Div([
                html.Button(id='btn-get-last-file', children='Get', style={'margin': param_margin},
                            title='Get folder\'s last created file & close other opened files'),
                html.Button(id='btn-add-last-file', children='Add', style={'margin': param_margin, 'margin-left': 10},
                            title='Add folder\'s last created file to file list'),
                html.Div([
                    dcc.Checklist(id='check-auto-get-file', options=[' Auto get'],
                                  value=[' Auto get'] * config['auto_get_file'],
                                  labelStyle={'margin-left': 5, 'margin-right': 10},
                                  style={'display': 'inline-block', 'margin-left': 10})
                ], style={'display': 'inline-block'}, title='Automatic "Get" after file creation from Acquisition tab\n'
                                                            'Works only for files saved in this folder'),
                html.Div([
                dcc.Checklist(id='check-auto-add-file', options=[' Auto add'],
                              value=[' Auto add'] * config['auto_add_file'],
                              labelStyle={'margin-left': 5, 'margin-right': 10},
                              style={'display': 'inline-block'})
                ], style={'display': 'inline-block'}, title='Automatic "Add" after file creation from Acquisition tab\n'
                                                            'Works only for files saved in this folder')
            ]),
            html.Br(),
            html.H4('Selected file(s)'),
            dbc.Accordion(id='list-files', children=[])
        ]
        # windowing tab
        win_tab.children = [
            dcc.Checklist(id='check-bypass-windowing', options=[' Bypass windowing tab'],
                          value=[' Bypass windowing tab'] * config['bypass_windowing'],
                          labelStyle={'margin-left': 5, 'margin-top': 10, 'margin-bottom': 10}),
            html.H4('Time bounds'),
            html.Label('Min. time [ms]:', style={'margin': param_margin, 'width': 120}),
            dcc.Input(id='input-min-time', type='number', value=config['min_time'], debounce=True,
                      style={'margin': param_margin, 'width': 100}),
            html.Br(),
            html.Label('Max. time [ms]:', style={'margin': param_margin, 'width': 120}),
            dcc.Input(id='input-max-time', type='number', value=config['max_time'], debounce=True,
                      style={'margin': param_margin, 'width': 100}),
            html.Div([
                dcc.Checklist(id='check-zero-padding', options=[' Zero-padding'],
                              value=[' Zero-padding'] * config['zero_padding'],
                              labelStyle={'margin-left': 5, 'margin-right': 10})
            ], title='Add zeros before and/or signal is time bounds exceed signal bounds'),
            html.Br(),
            html.H4('Window'),
            dcc.Dropdown(id='drop-window', options=['Rectangular', 'Hann', 'Tukey'], value=config['window'],
                         clearable=False),
            html.Div(id='div-analysis-window-alpha', children=[
                html.Label('Alpha [%]:', style={'margin': param_margin, 'width': 120}),
                dcc.Input(id='input-analysis-alpha', type='number', min=0, max=100, value=config['alpha_window'],
                          debounce=True, style={'margin': param_margin, 'width': 100}),
            ])
        ]
        # filtering tab
        filt_tab.children = [
            dcc.Checklist(id='check-bypass-filtering', options=[' Bypass filtering tab'],
                          value=[' Bypass filtering tab'] * config['bypass_filtering'],
                          labelStyle={'margin-left': 5, 'margin-top': 10, 'margin-bottom': 10}),
            html.H4('DC component'),
            dcc.Checklist(id='check-remove-mean', options=[' Remove mean'],
                          value=[' Remove mean'] * config['remove_mean'],
                          labelStyle={'margin-left': 5, 'margin-top': 10, 'margin-bottom': 10}),
            html.Br(),
            html.H4('Highpass filter'),
            html.Label('Cutoff freq. [kHz]:', style={'margin': param_margin, 'width': 150}),
            dcc.Input(id='input-freq-high', type='number', value=config['highpass_freq'], min=0, debounce=True,
                      style={'margin': param_margin, 'width': 80}),
            html.Br(),
            html.Label('Type:', style={'margin': param_margin, 'margin-right': 25}),
            dcc.Dropdown(id='drop-high-filt', options={'butterworth': 'Butterworth',
                                                       'fir': 'FIR linear phase (time centered)'},
                         value=config['highpass_type'], clearable=False, optionHeight=50,
                         style={'display': 'inline-block', 'verticalAlign': 'middle', 'width': 180}),
            html.Br(),
            html.Label(id='label-n-high', children='Order:', style={'margin': param_margin, 'width': 150}),
            dcc.Input(id='input-n-high', type='number', value=config['highpass_param'], min=0, debounce=True,
                      style={'margin': param_margin, 'width': 80}),
            html.Br(),
            html.Br(),
            html.H4('Lowpass filter'),
            html.Label('Cutoff freq. [kHz]:', style={'margin': param_margin, 'width': 150}),
            dcc.Input(id='input-freq-low', type='number', value=config['lowpass_freq'], min=0, debounce=True,
                      style={'margin': param_margin, 'width': 80}),
            html.Br(),
            html.Label('Type:', style={'margin': param_margin, 'margin-right': 25}),
            dcc.Dropdown(id='drop-low-filt', options={'butterworth': 'Butterworth',
                                                      'fir': 'FIR linear phase (time centered)'},
                         value=config['lowpass_type'], clearable=False, optionHeight=50,
                         style={'display': 'inline-block', 'verticalAlign': 'middle', 'width': 180}),
            html.Br(),
            html.Label(id='label-n-low', children='Order:', style={'margin': param_margin, 'width': 150}),
            dcc.Input(id='input-n-low', type='number', value=config['lowpass_param'], min=0, debounce=True,
                      style={'margin': param_margin, 'width': 80}),
        ]

        # display div
        disp_div.children = [dbc.Tabs(children=[
            info_tab := dbc.Tab(label='Files info & conversion'),
            time_tab := dbc.Tab(label='Time domain', tab_id='tab-time'),
            freq_tab := dbc.Tab(label='Frequency domain'),
            timefreq_tab := dbc.Tab(label='Time-frequency domain'),
        ], active_tab='tab-time')]
        # files info tab
        info_tab.children = [
            html.Div([
                dbc.Accordion(id='accordion-files-infos')
            ], style={'padding': 10})
        ]
        # time domain tab
        fig = go.Figure()
        fig.update_xaxes(title_text='Time [ms]')
        fig.update_layout(margin={'l': 10, 'r': 10, 't': 30, 'b': 10})
        fig.update_layout(legend={'yanchor': 'top', 'y': 0.99, 'xanchor': 'right', 'x': 0.99})
        fig.update_layout(title='')
        fig.update_layout(showlegend=True)
        time_opt_div = html.Div([
            dcc.Checklist({
                'signal': ' Display signal',
                'enveloppe': ' Display envelope'
            }, value=['signal'] * config['disp_signal'] + ['enveloppe'] * config['disp_envelope'],
                id='check-analysis-time-opt', inline=True,
                labelStyle={'margin-left': 5, 'margin-right': 10}, style={'display': 'inline-block'}),
            dcc.Checklist({
                'log-scale': ' Log scale'
            }, value=['log-scale'] * config['log_scale_signal'], id='check-analysis-time-log', inline=True,
                labelStyle={'margin-left': 5, 'margin-right': 10}, style={'display': 'inline-block'})
        ], style={'margin-bottom': 0})
        time_tab.children = [
            html.Div(className='loaderWrapper', children=[
                dcc.Loading(type='circle', children=[
                    html.Div([
                        time_opt_div,
                        dcc.Graph(id='graph-analysis-time', style={'width': '100%', 'height': '100%'}, figure=fig,
                                  config={'editable': True})
                    ], style={'padding': 10, 'height': '89vh'})
                ])
            ])
        ]
        # frequency domain tab
        fig = go.Figure()
        fig.update_xaxes(title_text='Frequency [kHz]')
        fig.update_layout(margin={'l': 10, 'r': 10, 't': 30, 'b': 10})
        fig.update_layout(legend={'yanchor': 'top', 'y': 0.99, 'xanchor': 'right', 'x': 0.99})
        fig.update_layout(title='')
        fig.update_layout(showlegend=True)
        freq_opt_div = html.Div([
            dcc.Checklist({
                'fft': ' Display FFT'
            }, value=['fft'] * config['disp_fft'], id='check-analysis-disp-freq', inline=True,
                labelStyle={'margin-left': 5, 'margin-right': 10}, style={'display': 'inline-block'}),
            dcc.Checklist({
                'log-scale': ' Log scale'
            }, value=['log-scale'] * config['log_scale_fft'], id='check-analysis-freq-log', inline=True,
                labelStyle={'margin-left': 5, 'margin-right': 10}, style={'display': 'inline-block'})
        ], style={'margin-bottom': 0})
        freq_tab.children = [
            html.Div(className='loaderWrapper', children=[
                dcc.Loading(type='circle', children=[
                    html.Div([
                        freq_opt_div,
                        dcc.Graph(id='graph-analysis-freq', style={'width': '100%', 'height': '100%'}, figure=fig,
                                  config={'editable': True})
                    ], style={'padding': 10, 'height': '89vh'})
                ])
            ])
        ]
        # time-frequency domain tab
        fig = go.Figure()
        fig.update_xaxes(title_text='Time [ms]')
        fig.update_yaxes(title_text='Frequency [kHz]')
        fig.update_layout(margin={'l': 10, 'r': 10, 't': 30, 'b': 10})
        fig.update_layout(legend={'yanchor': 'top', 'y': 0.99, 'xanchor': 'right', 'x': 0.99})
        fig.update_layout(title='')
        fig.update_layout(showlegend=True)
        timefreq_opt_div = html.Div([
            dcc.Checklist({
                'fft': ' Display spectrogram'
            }, value=['fft'] * config['disp_spect'], id='check-analysis-disp-spect', inline=True,
                labelStyle={'margin-left': 5, 'margin-right': 10},
                style={'display': 'inline-block', 'verticalAlign': 'middle'}),
            dcc.Dropdown(id='drop-spect-ch', options={}, placeholder='Signal channel', maxHeight=600,
                         style={'width': 155, 'height': '1.8em', 'display': 'inline-block', 'verticalAlign': 'middle'}),
            html.Label('Window length [ms]:', style={'margin-left': 15, 'margin-right': 5, 'verticalAlign': 'middle'}),
            dcc.Input(id='input-spect-win-length', type='number', value=config['spect_win_len'], min=0, debounce=True,
                      style={'width': 50, 'height': '1.5em', 'verticalAlign': 'middle'}),
            html.Label('Window :', style={'margin-left': 15, 'margin-right': 5, 'verticalAlign': 'middle'}),
            dcc.Dropdown(id='drop-spect-win', options={'rectangular': 'Rectangular', 'hann': 'Hann', 'tukey': 'Tukey'},
                         value=config['spect_win'], clearable=False,
                         style={'width': 120, 'height': '1.8em', 'display': 'inline-block', 'verticalAlign': 'middle'}),
            html.Div([
                html.Label('Alpha [%]:', style={'margin-left': 15, 'margin-right': 5, 'verticalAlign': 'middle'}),
                dcc.Input(id='input-spect-win-alpha', type='number', value=config['spect_win_alpha'], min=0, max=100,
                          debounce=True, style={'width': 50, 'height': '1.5em', 'verticalAlign': 'middle'}),
            ], id='div-spect-win-alpha', style={'display': 'inline-block', 'verticalAlign': 'middle'}, hidden=True),
            html.Label('Window overlapping [%]:',
                       style={'margin-left': 15, 'margin-right': 5, 'verticalAlign': 'middle'}),
            dcc.Input(id='input-spect-win-overlap', type='number', value=config['spect_win_overlap'], min=0, max=199,
                      debounce=True, style={'width': 50, 'height': '1.5em', 'verticalAlign': 'middle'}),
            dcc.Checklist({'log-scale': ' Log scale'}, value=['log-scale'] * config['log_scale_spect'],
                          id='check-analysis-spect-log', inline=True, labelStyle={'margin-left': 5, 'margin-right': 10},
                          style={'display': 'inline-block', 'margin-left': 15, 'verticalAlign': 'middle'}),
        ], style={'margin-bottom': 0})
        timefreq_tab.children = [
            html.Div(className='loaderWrapper', children=[
                dcc.Loading(type='circle', children=[
                    html.Div([
                        timefreq_opt_div,
                        html.Div([
                            dcc.Graph(id='graph-analysis-spect', responsive=True, figure=fig,
                                      style={'width': '100%', 'height': '100%'},
                                      config={'editable': True}),
                            html.Div(id='div-spect-log-scale', children=[
                                dcc.RangeSlider(id='slider-spect-log-scale', min=0, max=1, value=[0, 1], marks=None,
                                                allowCross=False, pushable=1e-6, vertical=True)  # , verticalHeight=600)
                            ], style={'display': 'inline-block', 'margin-top': '8ch'}, hidden=True)
                        ], style={'display': 'flex', 'height': '100%'})
                    ], style={'padding': 10, 'height': '89vh'})
                ])
            ])
        ]

        ######################################## callbacks ########################################

        # files_data initialization
        for file_path in files:
            file = os.path.basename(file_path)
            if file.endswith(SAVE_EXTENSION) or file.endswith(OLD_SAVE_EXTENSION):
                data_dict = get_data_from_file(file_path)
                data_dict['displayed-ch'] = [[i, j] for i in range(data_dict['signal']['data'].shape[0])
                                             for j in range(data_dict['signal']['data'].shape[1])]
            elif file.endswith('.csv'):  # emission signal
                with open(file_path, 'r') as f:
                    csv_str = f.read()
                u, fs = SignalPanel.read_csv_str(csv_str)
                data_dict = format_emission_sig(u, fs)
                data_dict['reduction-factor'] = self.DEFAULT_REDUC_FACTOR
            else:
                raise ValueError
            data_dict['file-name'] = file
            data_dict['file-id'] = file + ' ' + str(len(self.files_data)) + ' ' + str(time.time())
            # updating store data
            self.files_data.append(data_dict)

        # active tabs underline
        @callback(
            Output('tab-files-analysis', 'tab_style'),
            Output('tab-win-analysis', 'tab_style'),
            Output('tab-filt-analysis', 'tab_style'),
            Input('check-auto-get-file', 'value'),
            Input('check-auto-add-file', 'value'),
            Input('check-bypass-windowing', 'value'),
            Input('input-min-time', 'value'),
            Input('input-max-time', 'value'),
            Input('drop-window', 'value'),
            Input('check-bypass-filtering', 'value'),
            Input('input-freq-high', 'value'),
            Input('input-freq-low', 'value'),
        )
        def tab_underline(
                get_file, add_file,
                bypass_win, min_time, max_time, window,
                bypass_filt, min_freq, max_freq,
        ):
            file_underline = bool(get_file) or bool(add_file)
            win_underline = ((not bool(bypass_win)) and
                             ((min_time is not None) or (max_time is not None) or (window != 'Rectangular')))
            filt_underline = (not bool(bypass_filt)) and ((min_freq is not None) or (max_freq is not None))
            file_style = {'text-decoration': 'underline'} if file_underline else {}
            win_style = {'text-decoration': 'underline'} if win_underline else {}
            filt_style = {'text-decoration': 'underline'} if filt_underline else {}
            return file_style, win_style, filt_style

        # auto get/add radiobutton behavior callback
        @callback(
            Output('check-auto-get-file', 'value'),
            Output('check-auto-add-file', 'value'),
            Input('check-auto-get-file', 'value'),
            Input('check-auto-add-file', 'value'),
        )
        def get_add_buttons(get_val: list[str], add_val: list[str]):
            if ctx.triggered_id == 'check-auto-get-file' and get_val:
                add_val = []
            if ctx.triggered_id == 'check-auto-add-file' and add_val:
                get_val = []
            return get_val, add_val

        # auto get/add different folders warning
        @callback(
            Output('popup-warning-data-folders', 'children'),
            Output('popup-warning-data-folders', 'is_open'),
            Output('store-data-folders-debounce', 'data'),
            Input({'type': 'input-folder', 'index': ALL}, 'n_blur'),
            Input('input-last-file-folder', 'n_blur'),
            Input('check-auto-get-file', 'value'),
            Input('check-auto-add-file', 'value'),
            State({'type': 'input-folder', 'index': ALL}, 'value'),
            State('input-last-file-folder', 'value'),
            State('store-data-folders-debounce', 'data'),
        )
        def warning_folders(_, __, check_get: list[str], check_add: list[str], acq_folders: list[str],
                            analysis_folder: str, store_data: list | None):
            # debounce behavior
            if (ctx.triggered_id not in ['check-auto-get-file', 'check-auto-add-file']
                    and store_data == [acq_folders, analysis_folder]):
                return '', False, store_data
            store_data = copy.deepcopy([acq_folders, analysis_folder])
            # no automatic get/add behavior
            if not check_get and not check_add:
                return '', False, store_data
            # same folders check
            if not all([f == analysis_folder for f in acq_folders]):
                warning_msg = ('Warning:\nSave file folder(s) from acquisition tab different from last file folder from'
                               ' analysis tab')
                return warning_msg, True, store_data
            else:
                return '', False, store_data

        # highpass filter param update
        @callback(
            Output('label-n-high', 'children'),
            Output('input-n-high', 'value'),
            Input('drop-high-filt', 'value'),
            State('input-n-high', 'value'),
        )
        def highpass_param_update(filt_method: str, param: float):
            # filter parameter label
            match filt_method.lower():
                case 'butterworth':
                    label = 'Order:'
                case 'fir':
                    label = 'Length [ms]:'
                case _:
                    raise ValueError()
            # filter parameter update
            if ctx.triggered_id is not None:  # not initial call
                param = None
            return label, param

        # lowpass filter param update
        @callback(
            Output('label-n-low', 'children'),
            Output('input-n-low', 'value'),
            Input('drop-low-filt', 'value'),
            State('input-n-low', 'value'),
        )
        def lowpass_param_update(filt_method: str, param: float):
            # filter parameter label
            match filt_method.lower():
                case 'butterworth':
                    label = 'Order:'
                case 'fir':
                    label = 'Length [ms]:'
                case _:
                    raise ValueError()
            # filter parameter update
            if ctx.triggered_id is not None:  # not initial call
                param = None
            return label, param

        # spectrogram tukey window alpha input
        @callback(
            Output('div-spect-win-alpha', 'hidden'),
            Input('drop-spect-win', 'value'),
        )
        def spect_tukey_alpha_input(win: str):
            if win.lower() == 'tukey':
                return False
            else:
                return True

        # file upload callback
        @callback(
            Output('store-analysis-files', 'data', allow_duplicate=True),
            Output('upload-data-file', 'contents'),
            Output('popup-warning-csv-format', 'children'),
            Output('popup-warning-csv-format', 'is_open'),
            Input('upload-data-file', 'contents'),
            State('upload-data-file', 'filename'),
            State('store-analysis-files', 'data'),
            prevent_initial_call=True
        )
        def upload_data_file(contents, file_names, store_flag):
            logging.debug('file upload ' + str(ctx.triggered_id) + ' ' + str(store_flag))
            if contents is None:
                return no_update, contents, '', False
            I = np.argsort(file_names)
            contents, file_names = [contents[i] for i in I], [file_names[i] for i in I]
            for content, file_name in zip(contents, file_names):
                if file_name.endswith(SAVE_EXTENSION) or file_name.endswith(OLD_SAVE_EXTENSION):
                    data_type, contents_string = content.split(',')
                    decoded = base64.b64decode(contents_string)
                    data_dict = get_data_from_bytes(decoded)
                    data_dict['displayed-ch'] = [[i, j] for i in range(data_dict['signal']['data'].shape[0])
                                                 for j in range(data_dict['signal']['data'].shape[1])]
                elif file_name.endswith('.csv'):  # emission signal
                    try:
                        u, fs = SignalPanel.read_csv_file(content)
                        data_dict = format_emission_sig(u, fs)
                        data_dict['reduction-factor'] = self.DEFAULT_REDUC_FACTOR
                    except Exception as e:
                        logging.warning(traceback.format_exc())
                        error_msg = 'File formatting error (' + type(e).__name__ + ': ' + str(e) + ')'
                        return no_update, contents, error_msg, True
                else:
                    error_msg = ('File format not supported (' + ', '.join([SAVE_EXTENSION, OLD_SAVE_EXTENSION, '.csv'])
                                 + ')')
                    logging.warning(error_msg)
                    return no_update, contents, error_msg, True
                data_dict['file-name'] = file_name
                data_dict['file-id'] = file_name + ' ' + str(len(self.files_data)) + ' ' + str(time.time())
                self.files_data.append(data_dict)
            return store_flag + 1, None, '', False

        # last file upload callback
        @callback(
            Output('store-analysis-files', 'data', allow_duplicate=True),
            Input('btn-get-last-file', 'n_clicks'),
            Input('btn-add-last-file', 'n_clicks'),
            Input('store-file-added', 'data'),
            State('store-analysis-files', 'data'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def upload_data_last_file(_, __, auto_file_flag, store_flag, config):
            logging.debug('last file upload ' + str(ctx.triggered_id) + ' ' + str(store_flag))
            folder = config['analysis_config']['last_file_folder']
            auto_get_file = config['analysis_config']['auto_get_file']
            auto_add_file = config['analysis_config']['auto_add_file']
            # base folder correction
            if not os.path.isabs(folder):
                folder = os.path.join(config['options_config']['base_folder'], folder)
            # auto flag but no file added
            if ctx.triggered_id == 'store-file-added' and (  # auto add file store flag
                    not auto_file_flag  # no file added
                    or folder not in auto_file_flag  # all files added in different folder (folders stocked in flag)
                    or not auto_get_file and not auto_add_file  # no automatic get/add
            ):
                return no_update
            # number of files to get
            if ctx.triggered_id == 'store-file-added':
                n_files = sum([f == folder for f in auto_file_flag])
            else:
                n_files = 1
            # finding last geronimo files in folder
            if not os.path.isdir(folder):  # folder doesn't exist
                return no_update
            files = os.listdir(folder)
            files = [file for file in files if (file.endswith(SAVE_EXTENSION) or file.endswith(OLD_SAVE_EXTENSION) or
                                                file.endswith('.csv'))]
            if ctx.triggered_id == 'btn-add-last-file':  # looping over already opened files
                opened_files = [fd['file-name'] for fd in self.files_data]
                files = [f for f in files if f not in opened_files]
            if not files:  # no geronimo file in folder
                return no_update
            files_ctime = [os.stat(os.path.join(folder, file)).st_ctime for file in files]
            files_ctime, files = zip(*sorted(zip(files_ctime, files)))
            files = files[-n_files:]
            # flushing files list for "get" option
            if (ctx.triggered_id == 'btn-get-last-file' or
                    ctx.triggered_id == 'store-file-added' and auto_get_file):
                self.files_data = []
            # converting files
            for file in files:
                if file.endswith(SAVE_EXTENSION) or file.endswith(OLD_SAVE_EXTENSION):
                    data_dict = get_data_from_file(os.path.join(folder, file))
                    data_dict['displayed-ch'] = [[i, j] for i in range(data_dict['signal']['data'].shape[0])
                                                 for j in range(data_dict['signal']['data'].shape[1])]
                elif file.endswith('.csv'):  # emission signal
                    with open(os.path.join(folder, file), 'r') as f:
                        csv_str = f.read()
                    u, fs = SignalPanel.read_csv_str(csv_str)
                    data_dict = format_emission_sig(u, fs)
                    data_dict['reduction-factor'] = self.DEFAULT_REDUC_FACTOR
                else:
                    raise ValueError
                data_dict['file-name'] = file
                data_dict['file-id'] = file + ' ' + str(len(self.files_data)) + ' ' + str(time.time())
                # updating store data
                self.files_data.append(data_dict)
            return store_flag + 1

        # files store changed callback
        @callback(
            Output('list-files', 'children'),
            Output('list-files', 'active_item'),
            Input('store-analysis-files', 'data'),
            State('list-files', 'children'),
            State('list-files', 'active_item'),
        )
        def update_data_file(_, list_children, active_item):
            # deleting deleted files
            k = 0
            while k < len(list_children):
                if list_children[k]['props']['id']['index'] not in [el['file-id'] for el in self.files_data]:
                    del list_children[k]
                else:
                    k += 1
            # adding new files
            for data in self.files_data:
                # file already displayed
                if data['file-id'] in [child['props']['id']['index'] for child in list_children]:
                    continue
                # emission/reception channels
                signal = data['signal']
                if 'displayed-ch' in list(data.keys()):  # normal .gero or .geronimo file
                    emission_ch = signal['emission_ch']
                    if len(signal['geronimo_nb_ch']) == 1:
                        reception_ch = signal['reception_ch']
                        reception_ch = [f'R{k + 1}' for k in range(len(reception_ch)) if reception_ch[k] != 'OFF']
                    else:
                        reception_ch = []
                        for k_ch_tot in range(len(signal['reception_ch'])):
                            if signal['reception_ch'][k_ch_tot] != 'OFF':
                                k_ch, k_gero = k_ch_tot, 0
                                gero_nb_ch = list(copy.deepcopy(signal['geronimo_nb_ch']))
                                while k_ch >= gero_nb_ch[0]:
                                    k_gero += 1
                                    k_ch -= gero_nb_ch.pop(0)
                                reception_ch.append(f'G{k_gero + 1}:R{k_ch + 1}')
                    pairs_ind = [[i, j] for i in range(len(emission_ch)) for j in range(len(reception_ch))]
                    pairs_ch = [{'value': k, 'label': ' ' + emission_ch[k[0]] + ' ' + reception_ch[k[1]]} for k in
                                pairs_ind]
                    emission_ch = [{'value': k, 'label': ' ' + emission_ch[k]} for k in range(len(emission_ch))]
                    reception_ch = [{'value': k, 'label': ' ' + reception_ch[k]} for k in range(len(reception_ch))]
                # item creation
                active_item = str(data['file-id'])
                item = dbc.AccordionItem(id={'type': 'file-item', 'index': data['file-id']}, title=data['file-name'],
                                         item_id=active_item, style={'width': '100%'})
                check_style = {'margin-left': 5, 'margin-right': 10}
                if 'displayed-ch' in list(data.keys()):  # normal .gero or .geronimo file
                    item.children = [
                        html.Button(id={'type': 'button-select', 'index': data['file-id']}, children='Select all',
                                    style={'margin-left': 5}),
                        html.Button(id={'type': 'button-clear', 'index': data['file-id']}, children='Clear all',
                                    style={'margin-left': 10}),
                        html.Br(),
                        html.Label('Select by channels:', style={'margin-top': 5}),
                        dcc.Checklist(emission_ch, [k for k in range(len(emission_ch))], inline=True,
                                      id={'type': 'check-emission', 'index': data['file-id']}, labelStyle=check_style),
                        dcc.Checklist(reception_ch, [k for k in range(len(reception_ch))], inline=True,
                                      id={'type': 'check-reception', 'index': data['file-id']}, labelStyle=check_style),
                        html.Label('Displayed pairs:'),
                        dcc.Checklist(pairs_ch, pairs_ind, inline=True,
                                      id={'type': 'check-pairs', 'index': data['file-id']}, labelStyle=check_style),
                    ]
                else:  # .csv emission signal file
                    item.children = [
                        html.B('Emission signal'),
                        dcc.Checklist(id={'type': 'check-em-sig-win-filt', 'index': data['file-id']},
                                      options={'win': ' Not affected by Windowing tab',
                                               'filt': ' Not affected by Filtering tab'},
                                      value=['filt'], labelStyle=check_style),
                        html.Label('Reduction factor: ', style={'display': 'inline-block', 'margin-right': '1.5ch'}),
                        dcc.Input(id={'type': 'input-reduction', 'index': data['file-id']},
                                  value=self.DEFAULT_REDUC_FACTOR, style={'width': '5ch'}),
                        html.Br()
                    ]
                item.children += [
                    html.Button(id={'type': 'delete-file', 'index': data['file-id']}, children='Remove file',
                                style={'margin-left': 5, 'margin-top': 5}, className='red-button'),
                    html.Button(id={'type': 'delete-all-files', 'index': data['file-id']}, children='Remove all files',
                                style={'margin-left': 10, 'margin-top': 5}, className='red-button')
                ]
                list_children.append(item.to_plotly_json())
            return list_children, active_item

        # delete file callback
        @callback(
            Output('store-analysis-files', 'data', allow_duplicate=True),
            Input({'type': 'delete-file', 'index': ALL}, 'n_clicks'),
            State({'type': 'delete-file', 'index': ALL}, 'id'),
            State('store-analysis-files', 'data'),
            prevent_initial_call=True
        )
        def delete_file_callback(n_clicks, btn_ids, store_flag):
            logging.debug('delete file ' + str(ctx.triggered_id) + ' ' + str(store_flag))
            # finding click id
            id_file = None
            for n_click, btn_id in zip(n_clicks, btn_ids):
                if n_click is not None:
                    id_file = btn_id['index']
                    break
            if id_file is None:  # no button clicked
                return no_update
            # deleting file
            for k in range(len(self.files_data)):
                if self.files_data[k]['file-id'] == id_file:
                    del self.files_data[k]
                    break
            return store_flag + 1

        # delete all files callback
        @callback(
            Output('store-analysis-files', 'data', allow_duplicate=True),
            Input({'type': 'delete-all-files', 'index': ALL}, 'n_clicks'),
            State('store-analysis-files', 'data'),
            prevent_initial_call=True
        )
        def delete_all_files_callback(n_clicks, store_flag):
            logging.debug('delete all files ' + str(ctx.triggered_id) + ' ' + str(store_flag))
            # finding click id
            flag = False  # one button clicked flag
            for n_click in n_clicks:
                if n_click is not None:
                    flag = True
                    break
            if not flag:  # no button clicked
                return no_update
            # deleting files
            self.files_data = []
            return store_flag + 1

        # emission/reception selection callback
        @callback(
            Output({'type': 'check-pairs', 'index': MATCH}, 'value'),
            Input({'type': 'check-emission', 'index': MATCH}, 'value'),
            Input({'type': 'check-reception', 'index': MATCH}, 'value'),
            prevent_initial_call=True
        )
        def em_rec_selection_callback(em_val, rec_val):
            return [[i, j] for i in em_val for j in rec_val]

        # select all channels callback
        @callback(
            Output({'type': 'check-emission', 'index': MATCH}, 'value', allow_duplicate=True),
            Output({'type': 'check-reception', 'index': MATCH}, 'value', allow_duplicate=True),
            Input({'type': 'button-select', 'index': MATCH}, 'n_clicks'),
            State({'type': 'check-emission', 'index': MATCH}, 'options'),
            State({'type': 'check-reception', 'index': MATCH}, 'options'),
            prevent_initial_call=True
        )
        def select_all_callback(_, em_opt, rec_opt):
            return [el['value'] for el in em_opt], [el['value'] for el in rec_opt]

        # clear all channels callback
        @callback(
            Output({'type': 'check-emission', 'index': MATCH}, 'value', allow_duplicate=True),
            Output({'type': 'check-reception', 'index': MATCH}, 'value', allow_duplicate=True),
            Input({'type': 'button-clear', 'index': MATCH}, 'n_clicks'),
            prevent_initial_call=True
        )
        def clear_all_callback(_):
            return [], []

        # update displayed channels callback
        @callback(
            Output('store-analysis-files', 'data', allow_duplicate=True),
            Input({'type': 'check-pairs', 'index': ALL}, 'value'),
            State({'type': 'check-pairs', 'index': ALL}, 'id'),
            State('store-analysis-files', 'data'),
            prevent_initial_call=True
        )
        def update_ch_callback(files_pairs, files_ids, store_flag):
            logging.debug('update displayed ch ' + str(ctx.triggered_id) + ' ' + str(store_flag))
            flag_pairs_changed = False
            for pairs, ind in zip(files_pairs, files_ids):
                pairs.sort()
                for k in range(len(self.files_data)):
                    if self.files_data[k]['file-id'] == ind['index'] and self.files_data[k]['displayed-ch'] != pairs:
                        self.files_data[k]['displayed-ch'] = pairs
                        flag_pairs_changed = True
            if flag_pairs_changed:
                return store_flag + 1
            else:
                return no_update

        # update reduction factors
        @callback(
            Output('store-analysis-files', 'data', allow_duplicate=True),
            Input({'type': 'input-reduction', 'index': ALL}, 'value'),
            State({'type': 'input-reduction', 'index': ALL}, 'id'),
            State('store-analysis-files', 'data'),
            prevent_initial_call=True
        )
        def update_reduc(reduc_vals, reduc_ids, store_flag):
            reduc_changed = False
            for reduc, ind in zip(reduc_vals, reduc_ids):
                reduc = float(reduc)
                for k in range(len(self.files_data)):
                    if (self.files_data[k]['file-id'] == ind['index']
                            and self.files_data[k]['reduction-factor'] != reduc):
                        self.files_data[k]['reduction-factor'] = reduc
                        reduc_changed = True
            if reduc_changed:
                return store_flag + 1
            else:
                return no_update

        # files info callback
        @callback(
            Output('accordion-files-infos', 'children'),
            Output('accordion-files-infos', 'active_item'),
            Input('store-analysis-files', 'data')
        )
        def update_signals_callback(_):
            acc_children = []
            active_item = ''
            for k in range(len(self.files_data)):
                data = self.files_data[k]
                item_id = f'file-info-{k}'
                acc = dbc.AccordionItem(title=f'File {k+1}', children=[], item_id=item_id)
                s = 'file_name: ' + data['file-name'] + '\n'
                for ke in data['signal'].keys():
                    if ke == 'data':
                        continue
                    s += ke + ': ' + str(data['signal'][ke]) + '\n'
                acc.children = [
                    html.Div(s, style={'whiteSpace': 'pre-wrap'}),
                    html.Br(),
                    html.Button(id={'type': 'button-convert-csv', 'file-id': data['file-id']},
                                children='Convert to csv'),
                    dcc.Download(id={'type': 'download-signal-csv', 'file-id': data['file-id']}),
                ]

                acc_children.append(acc)
                active_item = item_id
            return acc_children, active_item

        # file saving as csv callback
        @callback(
            Output({'type': 'download-signal-csv', 'file-id': MATCH}, 'data'),
            Input({'type': 'button-convert-csv', 'file-id': MATCH}, 'n_clicks'),
            State({'type': 'button-convert-csv', 'file-id': MATCH}, 'id'),
            prevent_initial_call=True
        )
        def download_csv(_, file_id):
            # file index
            k_file = None
            for k in range(len(self.files_data)):
                if self.files_data[k]['file-id'] == file_id['file-id']:
                    k_file = k
            if k_file is None:
                raise RuntimeError('File not found')
            # file name
            filename = self.files_data[k_file]['file-name']
            if filename.endswith('.gero'):
                filename = filename[:-5]
            filename += '.csv'
            # file content
            config = copy.deepcopy(self.files_data[k_file]['signal'])
            signals = config.pop('data')
            content = convert_signal_to_csv(signals, config)
            file_dict = {
                'content': content,
                'filename': filename
            }
            return file_dict

        # signal processing callback
        @callback(
            Output('store-analysis-signals', 'data'),
            Output('div-analysis-window-alpha', 'hidden'),
            Input('store-analysis-files', 'data'),
            Input('check-bypass-windowing', 'value'),
            Input('input-min-time', 'value'),
            Input('input-max-time', 'value'),
            Input('check-zero-padding', 'value'),
            Input('drop-window', 'value'),
            Input('input-analysis-alpha', 'value'),
            Input('check-bypass-filtering', 'value'),
            Input('check-remove-mean', 'value'),
            Input('input-freq-high', 'value'),
            Input('drop-high-filt', 'value'),
            Input('input-n-high', 'value'),
            Input('input-freq-low', 'value'),
            Input('drop-low-filt', 'value'),
            Input('input-n-low', 'value'),
            Input({'type': 'check-em-sig-win-filt', 'index': ALL}, 'value'),
            Input({'type': 'check-em-sig-win-filt', 'index': ALL}, 'id'),
            State('store-analysis-signals', 'data'),
        )
        def sig_proc_callback(_, bypass_win: list[str], t_min: float | None, t_max: float | None,
                              zero_padding: list[str], win: str, alpha: float | None,
                              bypass_filt: list[str], remove_mean: list[str],
                              freq_high: float, type_high: str, param_high: float,
                              freq_low: float, type_low: str, param_low: float,
                              check_em_sigs: list[list[str]], check_em_sig_ids: list[dict],
                              signals_flag: int) -> (int, bool):
            logging.debug('>>>> signal processing ' + str(ctx.triggered_id) + ' ' + str(signals_flag + 1))
            # time conversion
            t_min = None if t_min is None else 1e-3 * t_min
            t_max = None if t_max is None else 1e-3 * t_max
            # csv files ids
            check_em_sig_ids = [i['index'] for i in check_em_sig_ids]
            # lines & signal proc
            lines = []
            for k in range(len(self.files_data)):
                data = self.files_data[k]
                signal = data['signal']
                if 'displayed-ch' in list(data.keys()):  # normal .gero or .geronimo file
                    if len(signal['geronimo_nb_ch']) == 1:
                        reception_ch = signal['reception_ch']
                        reception_ch = [f'R{k + 1}' for k in range(len(reception_ch)) if reception_ch[k] != 'OFF']
                    else:
                        reception_ch = []
                        for k_ch_tot in range(len(signal['reception_ch'])):
                            if signal['reception_ch'][k_ch_tot] != 'OFF':
                                k_ch, k_gero = k_ch_tot, 0
                                gero_nb_ch = list(copy.deepcopy(signal['geronimo_nb_ch']))
                                while k_ch >= gero_nb_ch[0]:
                                    k_gero += 1
                                    k_ch -= gero_nb_ch.pop(0)
                                reception_ch.append(f'G{k_gero + 1}:R{k_ch + 1}')
                    for chs in data['displayed-ch']:
                        if chs[0] >= len(signal['data']):  # data not recorded during acquisition (bug)
                            continue
                        u = np.array(signal['data'][chs[0]][chs[1]][:])
                        fs = signal['fs']
                        t0 = signal['delay']
                        lines.append({
                            'u': u,
                            't0': t0,
                            'fs': fs,
                            'window': True,
                            'filter': True,
                            'label': data['signal']['emission_ch'][chs[0]] + ' ' + reception_ch[chs[1]] +
                                     f' (file {k+1})'
                        })
                else:  # .csv emission signal file
                    try:
                        k_csv = check_em_sig_ids.index(data['file-id'])
                        check_em_sig = check_em_sigs[k_csv]
                        win_bool, filt_bool = 'win' not in check_em_sig, 'filt' not in check_em_sig
                    except ValueError:
                        win_bool, filt_bool = True, False
                    lines.append({
                        'u': signal['data'] / data['reduction-factor'],
                        't0': 0.,
                        'fs': signal['fs'],
                        'window': win_bool,
                        'filter': filt_bool,
                        'label': f'Emission signal (file {k+1})'
                    })
            # windowing and filtering
            for line in lines:
                u, t0, fs, win_bool, filt_bool = line['u'], line['t0'], line['fs'], line['window'], line['filter']
                # dc component
                if not bypass_filt and filt_bool and remove_mean:
                    u -= np.mean(u, axis=0)
                # windowing
                if not bypass_win and win_bool:
                    # time bounds
                    n_min, n_max = 0, u.size - 1
                    if t_min is not None:
                        n_min = int(np.ceil((t_min - t0) * fs))
                    if t_max is not None:
                        n_max = int(np.floor((t_max - t0) * fs))
                    # upper bound clipping
                    if n_max < 0:
                        u = np.array([], ndmin=1)
                    elif n_max < u.size:
                        u = u[:n_max + 1]
                    elif zero_padding:
                        u = np.concatenate((u, np.zeros(n_max + 1 - u.size)))
                    # lower bound clipping
                    if n_min >= u.size:
                        u = np.array([], ndmin=1)
                    elif n_min >= 0:
                        u = u[n_min:]
                        t0 += n_min / fs
                    elif zero_padding:
                        u = np.concatenate((np.zeros(-n_min), u))
                        t0 += n_min / fs
                    # windowing
                    alpha1 = None if alpha is None else alpha / 100
                    w = get_window(win.lower(), n_max - n_min + 1, alpha1)
                    if not zero_padding:
                        w = w[max(-n_min, 0):]
                        w = w[:min(len(u), len(w))]
                    u = u * w
                # filtering
                if not bypass_filt and filt_bool:
                    # highpass
                    if freq_high is not None and param_high is not None:
                        u = get_filtered(u, fs, freq_high, 'highpass', type_high, param_high)
                    # lowpass
                    if freq_low is not None and param_low is not None:
                        u = get_filtered(u, fs, freq_low, 'lowpass', type_low, param_low)
                line['u'], line['t0'], line['fs'] = u, t0, fs

            self.signals_data = lines

            # alpha parameter hide
            alpha_hidden = win.lower() in ['rectangular', 'hann']

            return signals_flag + 1, alpha_hidden

        # config update callback
        @callback(
            Output('store-config', 'data', allow_duplicate=True),
            Input('input-last-file-folder', 'value'),
            Input('check-auto-get-file', 'value'),
            Input('check-auto-add-file', 'value'),
            Input('check-bypass-windowing', 'value'),
            Input('input-min-time', 'value'),
            Input('input-max-time', 'value'),
            Input('check-zero-padding', 'value'),
            Input('drop-window', 'value'),
            Input('input-analysis-alpha', 'value'),
            Input('check-bypass-filtering', 'value'),
            Input('check-remove-mean', 'value'),
            Input('input-freq-high', 'value'),
            Input('drop-high-filt', 'value'),
            Input('input-n-high', 'value'),
            Input('input-freq-low', 'value'),
            Input('drop-low-filt', 'value'),
            Input('input-n-low', 'value'),
            Input('check-analysis-time-opt', 'value'),
            Input('check-analysis-time-log', 'value'),
            Input('check-analysis-disp-freq', 'value'),
            Input('check-analysis-freq-log', 'value'),
            Input('check-analysis-disp-spect', 'value'),
            Input('input-spect-win-length', 'value'),
            Input('drop-spect-win', 'value'),
            Input('input-spect-win-alpha', 'value'),
            Input('input-spect-win-overlap', 'value'),
            Input('check-analysis-spect-log', 'value'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def config_update_call(last_file_folder: str, auto_get_file: list[str], auto_add_file: list[str],
                               bypass_win: list[str], min_time: float | None, max_time: float | None,
                               zero_padding: list[str], window: str, alpha: float | None,
                               bypass_filt: list[str], remove_mean: list[str],
                               freq_high: float, type_high: str, param_high: float,
                               freq_low: float, type_low: str, param_low: float,
                               time_opt: list[str], time_log: list[str], freq_opt: list[str], freq_log: list[str],
                               spect_opt: list[str], spect_win_len: float, spect_win: str, spect_win_alpha: float,
                               spect_win_overlap: float, spect_log: list[str],
                               data: dict):
            data['analysis_config']['last_file_folder'] = last_file_folder
            data['analysis_config']['auto_get_file'] = bool(auto_get_file)
            data['analysis_config']['auto_add_file'] = bool(auto_add_file)
            data['analysis_config']['bypass_windowing'] = bool(bypass_win)
            data['analysis_config']['min_time'] = min_time
            data['analysis_config']['max_time'] = max_time
            data['analysis_config']['zero_padding'] = bool(zero_padding)
            data['analysis_config']['window'] = window
            data['analysis_config']['alpha_window'] = alpha
            data['analysis_config']['bypass_filtering'] = bool(bypass_filt)
            data['analysis_config']['remove_mean'] = bool(remove_mean)
            data['analysis_config']['highpass_freq'] = freq_high
            data['analysis_config']['highpass_type'] = type_high
            data['analysis_config']['highpass_param'] = param_high
            data['analysis_config']['lowpass_freq'] = freq_low
            data['analysis_config']['lowpass_type'] = type_low
            data['analysis_config']['lowpass_param'] = param_low
            data['analysis_config']['disp_signal'] = 'signal' in time_opt
            data['analysis_config']['disp_envelope'] = 'enveloppe' in time_opt
            data['analysis_config']['log_scale_signal'] = bool(time_log)
            data['analysis_config']['disp_fft'] = bool(freq_opt)
            data['analysis_config']['log_scale_fft'] = bool(freq_log)
            data['analysis_config']['disp_spect'] = bool(spect_opt)
            data['analysis_config']['spect_win_len'] = spect_win_len
            data['analysis_config']['spect_win'] = spect_win
            data['analysis_config']['spect_win_alpha'] = spect_win_alpha
            data['analysis_config']['spect_win_overlap'] = spect_win_overlap
            data['analysis_config']['log_scale_spect'] = bool(spect_log)

            return data

        # time domain display callback
        @callback(
            Output('graph-analysis-time', 'figure'),
            Input('store-analysis-signals', 'data'),
            Input('check-analysis-time-opt', 'value'),
            Input('check-analysis-time-log', 'value'),
        )
        def time_display(flag, opt: list[str], log_opt: list[str]):
            logging.debug('>>>>>>>> time plot ' + str(ctx.triggered_id) + ' ' + str(flag))
            fig = Patch()
            # lines
            fig['data'] = []
            for line in self.signals_data:
                if not ('signal' in opt or 'enveloppe' in opt):  # no line
                    continue
                # signal
                t = line['t0'] + np.arange(len(line['u'])) / line['fs']  # time array
                u, u_env = line['u'], 1
                if 'enveloppe' in opt:
                    u_env = np.abs(sig.hilbert(u))
                if log_opt:
                    u, u_env = 20 * np.log10(np.abs(u)), 20 * np.log10(u_env)
                # plotting
                x, y = np.zeros(0), np.zeros(0)
                if 'signal' in opt:
                    x = np.concatenate((x, t))
                    y = np.concatenate((y, u))
                    if 'enveloppe' in opt:
                        x = np.concatenate((x, np.nan * np.zeros(1)))
                        y = np.concatenate((y, np.nan * np.zeros(1)))
                if 'enveloppe' in opt:
                    x = np.concatenate((x, t))
                    y = np.concatenate((y, u_env))
                fig['data'].append(go.Scatter(x=1e3*x, y=y, mode='lines', name=line['label']))
            # yaxis label
            if log_opt:
                fig['layout']['yaxis']['title']['text'] = 'Amplitude [dB]'
            else:
                fig['layout']['yaxis']['title']['text'] = 'Amplitude [V]'
            # yaxis autorange
            if ctx.triggered_id == 'check-analysis-time-log':
                fig['layout']['yaxis']['autorange'] = True
                fig['layout']['yaxis']['range'] = None
            return fig

        # frequency domain display callback
        @callback(
            Output('graph-analysis-freq', 'figure'),
            Input('store-analysis-signals', 'data'),
            Input('check-analysis-disp-freq', 'value'),
            Input('check-analysis-freq-log', 'value'),
        )
        def freq_display(_, disp_opt: list[str], log_opt: list[str]):
            fig = Patch()
            # lines
            fig['data'] = []
            if disp_opt:
                for line in self.signals_data:
                    # signal
                    f = np.arange(len(line['u']), dtype=float) * line['fs'] / len(line['u'])  # freq array
                    u = line['u']
                    if u.size > 0:
                        u = 2 * np.abs(np.fft.fft(line['u'])) / len(line['u'])
                        u[0] /= 2
                        f, u = f[:len(f) // 2 + 1], u[:len(f) // 2 + 1]  # + 1 for display to 1 MHz
                        if log_opt:
                            u = 20 * np.log10(u)
                    # plotting
                    fig['data'].append(go.Scatter(x=1e-3*f, y=u, mode='lines', name=line['label']))
            # yaxis label
            if log_opt:
                fig['layout']['yaxis']['title']['text'] = 'Amplitude [dB]'
                fig['layout']['yaxis']['rangemode'] = 'normal'
            else:
                fig['layout']['yaxis']['title']['text'] = 'Amplitude [V]'
                fig['layout']['yaxis']['rangemode'] = 'tozero'
            # yaxis autorange
            if ctx.triggered_id == 'check-analysis-freq-log':
                fig['layout']['yaxis']['autorange'] = True
                fig['layout']['yaxis']['range'] = None
            return fig

        # spectrogram channels update
        @callback(
            Output('drop-spect-ch', 'options'),
            Output('drop-spect-ch', 'value'),
            Input('store-analysis-signals', 'data'),
            State('drop-spect-ch', 'value'),
            prevent_initial_call=True
        )
        def update_spect_channels(_, prev_ch: str):
            ch_opt = [line['label'] for line in self.signals_data]
            new_ch = prev_ch
            if new_ch not in ch_opt:
                new_ch = None
            if new_ch is None and ch_opt:
                new_ch = ch_opt[0]
            return ch_opt, new_ch

        # spectrogram log scale colorbar slider hidden
        @callback(
            Output('div-spect-log-scale', 'hidden'),
            Input('check-analysis-spect-log', 'value')
        )
        def hide_slider(spect_log: dict[str]):
            return not bool(spect_log)

        # spectrogram display callback
        @callback(
            Output('graph-analysis-spect', 'figure'),
            Input('store-analysis-signals', 'data'),
            Input('check-analysis-disp-spect', 'value'),
            Input('drop-spect-ch', 'value'),
            Input('input-spect-win-length', 'value'),
            Input('drop-spect-win', 'value'),
            Input('input-spect-win-alpha', 'value'),
            Input('input-spect-win-overlap', 'value'),
            Input('check-analysis-spect-log', 'value'),
            Input('slider-spect-log-scale', 'value')
        )
        def spect_display(_, spect_disp: list[str], spect_ch: str, spect_win_len: float, spect_win: str,
                          spect_win_alpha: float, spect_win_overlap: float, spect_log: list[str],
                          spect_range: list[float]):
            # figure modifications object
            fig = Patch()
            fig['data'] = []

            # input check
            if spect_ch is None or spect_win_len is None or spect_win is None or spect_win_overlap is None:
                return fig
            if spect_win.lower() == 'tukey' and spect_win_alpha is None:
                return fig
            if not spect_disp:
                return fig

            # finding selected line
            line = [l for l in self.signals_data if l['label'] == spect_ch]
            if len(line) != 1:
                raise RuntimeError('Unexpected multiple lines with same label')
            line = line[0]

            # computing spectrogram
            spect, freqs, win_t = get_spectrogram(line['u'], line['fs'], spect_win_len, spect_win, spect_win_alpha,
                                                  spect_win_overlap, bool(spect_log))
            win_t += line['t0']

            # displaying spectrogram
            if spect_log:
                min_range, max_range = np.nanmin(spect[spect > -np.inf]), np.nanmax(spect)
                min_range, max_range = min_range + spect_range[0] * (max_range - min_range), \
                    min_range + spect_range[1] * (max_range - min_range)
                map_range = {'zmin': min_range, 'zmax': max_range}
                fig['data'].append(
                    go.Heatmap(x=1e3*win_t, y=1e-3*freqs, z=spect, colorbar={'title': 'Ampl.<br>[dB]'}, **map_range)
                )
            else:
                map_range = {'zmin': 0}
                fig['data'].append(
                    go.Heatmap(x=1e3*win_t, y=1e-3*freqs, z=spect, colorbar={'title': 'Ampl.<br>[V]'}, **map_range)
                )

            return fig


# emission signal formatting
def format_emission_sig(u: np.ndarray[float], fs: float) -> dict:
    sig_dict = {
        'data': u,
        'fs': fs,
        'samples': u.size,
    }
    data_dict = {'signal': sig_dict}
    return data_dict


# windowing function
def get_window(win: str, n: int, alpha: float = None) -> np.ndarray[float]:
    if n <= 0:
        return np.array([], ndmin=1)
    if alpha is None:
        alpha = 0
    match win:
        case 'rectangular':
            w = np.ones(n)
        case 'hann':
            w = np.square(np.sin(np.pi / n * np.arange(n)))
        case 'tukey':
            w = np.ones(n)
            a = int(n * alpha)
            if a > 0:
                w[:a // 2] = np.square(np.sin(np.pi / a * np.arange(a // 2)))
                w[-(a // 2):] = np.flip(np.square(np.sin(np.pi / a * np.arange(a // 2))))
        case _:
            raise ValueError(f'"{win}" window not implemented')
    return w


# filtering function
def get_filtered(u: np.ndarray, fs: float, cutoff: float, filt_type: str, filt_method: str, filt_param: float) -> \
        np.ndarray:
    cutoff *= 1e3  # kHz to Hz
    match filt_method.lower():
        case 'butterworth':
            sos = sig.butter(int(filt_param), [cutoff], filt_type, fs=fs, output='sos')
            v = sig.sosfilt(sos, u)
        case 'fir':
            timetaps = 1e-3 * filt_param  # ms to s
            numtaps = (int(timetaps * fs) + 1) | 1
            h = sig.firwin(numtaps, cutoff, pass_zero=filt_type, fs=fs)
            v = sig.convolve(u, h, mode='same')
        case _:
            raise RuntimeError('Not implemented')
    return v


# spectrogram function
def get_spectrogram(u: np.ndarray, fs: float, win_len: float, win: str, win_alpha: float, win_overlap: float,
                    log_scale: bool) -> (np.ndarray, np.ndarray, np.ndarray):
    win_len *= 1e-3  # ms to s
    win_alpha /= 100  # % to
    win_overlap /= 100  # % to
    u = np.array(u)

    # computing window parameters
    n = max(int(np.round(win_len * fs)), 1)  # windows length
    n_overlap = int(np.round(win_len * fs * win_overlap))  # windows overlap length
    n_overlap = min(n_overlap, n - 1)  # to avoid infinite loop over time increment
    w = get_window(win, n, win_alpha)  # window weights
    w /= np.mean(w)  # normalization for amplitude

    # windows indexes
    win_k = []
    k = 0
    while k + n <= u.size:
        win_k.append(k)
        k += n - n_overlap
    # tot_size = win_k[-1] + n if win_k else 0
    # u = np.concatenate((u, np.zeros(tot_size - u.size)))  # zero-padding for last window

    # computing spectrogram
    spect = np.zeros((n // 2, len(win_k)), dtype=float)
    for k_ind in range(len(win_k)):
        k = win_k[k_ind]
        spect[:, k_ind] = np.abs(np.fft.fft(u[k:k + n] * w))[:n // 2] * (2 / n)
        spect[:, k_ind][0] /= 2

    # log scale
    if log_scale:
        spect = 20 * np.log10(spect)

    # windows times
    win_t = (np.arange(u.size)[win_k] + n / 2) / fs

    # frequencies array
    freqs = np.arange(n // 2) * (fs / n)

    return spect, freqs, win_t
