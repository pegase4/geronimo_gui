from dash import html, dcc, Input, Output, State, callback, ALL, MATCH, ctx, Patch
import dash_bootstrap_components as dbc
import numpy as np
import scipy.signal as sig
import traceback


class CustomSignalsTab(dbc.Tab):
    """
    Tab for custom signals creation, from operations on recorded signals. Goes in the parameters Tabs of Analysis
    """

    def __init__(self, config: dict, analysis_pan, **kwargs):
        # attributes
        self.last_item_id = 0
        self.analysis_pan = analysis_pan

        # superclass init
        super().__init__(**kwargs)

        ######################################## display ########################################

        self.children = [
            dcc.Store(id='store-custom-sigs', data=[]),  # store for additional signals creation
            html.H4('Base signals'),
            html.Div(id='div-raw-sigs-labels', children=[]),
            html.Br(),
            html.H4('Custom signals'),
            html.Button(id='button-create-custom-sig', children='New custom signal'),
            html.Br(),
            html.Br(),
            list_custom_sigs := dbc.Accordion(id='list-custom-sigs', children=[])
        ]

        # custom signal creation function
        def custom_sig_creation(_, custom_sigs: list, item_title: str = None, item_expr: str = None) -> (list, str):
            self.last_item_id += 1
            item_id = str(self.last_item_id)
            if item_title is None:
                item_title = f'Custom #{len(custom_sigs)+1}'
            if item_expr is None:
                item_expr = ''
            item = dbc.AccordionItem(id={'type': 'acc-custom-sig', 'index': item_id}, item_id=item_id, title=item_title,
                                     children=[
                html.Label('Custom signal label:'),
                html.Br(),
                dcc.Input(id={'type': 'input-custom-sig-label', 'index': item_id}, value=item_title,
                          style={'width': '100%'}),
                html.Label('Custom signal expression:'),
                html.Br(),
                dcc.Textarea(id={'type': 'input-custom-sig-expr', 'index': item_id}, value=item_expr,
                             placeholder='Enter custom signal expression (np for numpy, sig for scipy.signal)...',
                             style={'width': '100%', 'height': '4em'}),
                html.Br(),
                html.Button(id={'type': 'button-delete-custom-sig', 'index': item_id}, children='Delete signal',
                            style={'background-color': '#FF7A7A', 'margin-top': 5})
            ])
            custom_sigs.append(item)
            return custom_sigs, item_id

        # creating custom signals from config file
        for custom_sig in config['analysis_config']['custom_signals']:
            list_custom_sigs.children, list_custom_sigs.active_item = \
                custom_sig_creation(None, list_custom_sigs.children, custom_sig['label'], custom_sig['expression'])

        ######################################## callbacks ########################################

        # raw signals labels display
        @callback(
            Output('div-raw-sigs-labels', 'children'),
            Input('store-analysis-signals', 'data')
        )
        def raw_sigs_disp(_) -> list:
            children = []
            for k in range(len(self.analysis_pan.signals_data)):
                line = self.analysis_pan.signals_data[k]
                if 'custom' in line.keys():  # custom signals
                    break
                children.append(html.B('u' + str(k+1)))
                children.append(html.Label(': "' + line['label'] + '"'))
                children.append(html.Br())
            return children

        # new custom signal creation
        @callback(
            Output('list-custom-sigs', 'children', allow_duplicate=True),
            Output('list-custom-sigs', 'active_item', allow_duplicate=True),
            Input('button-create-custom-sig', 'n_clicks'),
            State('list-custom-sigs', 'children'),
            prevent_initial_call=True
        )
        def custom_sig_creation_callback(*args):
            return custom_sig_creation(*args)

        # update custom sig title callback
        @callback(
            Output({'type': 'acc-custom-sig', 'index': MATCH}, 'title'),
            Input({'type': 'input-custom-sig-label', 'index': MATCH}, 'value'),
            prevent_initial_call=True
        )
        def update_custom_sig_title(title: str) -> str:
            return title

        # delete custom signal callback:
        @callback(
            Output('list-custom-sigs', 'children', allow_duplicate=True),
            Input({'type': 'button-delete-custom-sig', 'index': ALL}, 'n_clicks'),
            State('list-custom-sigs', 'children'),
            prevent_initial_call=True
        )
        def delete_custom_sig_callback(n_clicks: list, custom_sigs: list) -> list:
            k_sig = -1
            for k in range(len(n_clicks)):
                if n_clicks[k] is not None:
                    k_sig = k
                    break
            if k_sig >= 0:
                del custom_sigs[k_sig]
            return custom_sigs

        # update custom signals info list
        @callback(
            Output('store-custom-sigs', 'data'),
            Input({'type': 'input-custom-sig-label', 'index': ALL}, 'n_blur'),  # to avoid recomputing while typing
            Input({'type': 'input-custom-sig-expr', 'index': ALL}, 'n_blur'),
            State({'type': 'input-custom-sig-label', 'index': ALL}, 'value'),
            State({'type': 'input-custom-sig-expr', 'index': ALL}, 'value'),
        )
        def custom_sigs_info_update(_, __, custom_sigs_labels: list[str], custom_sigs_expr: list[str]) -> list[dict]:
            custom_sigs = []
            for label, expression in zip(custom_sigs_labels, custom_sigs_expr):
                if expression is None:
                    expression = ''
                custom_sigs.append({
                    'label': label,
                    'expression': expression
                })
            return custom_sigs

        # custom signals creation callback
        @callback(
            Output('store-analysis-signals', 'data', allow_duplicate=True),
            Input('store-custom-sigs', 'data'),
            Input('store-analysis-signals', 'data'),
            prevent_initial_call=True
        )
        def custom_sigs_creation_callback(custom_sigs: list[dict], signals_flag) -> list[dict]:
            # removing old custom signals
            for k in range(len(self.analysis_pan.signals_data)):
                if 'custom' in self.analysis_pan.signals_data[k].keys():
                    if ctx.triggered_id == 'store-custom-sigs':  # custom signals changed
                        self.analysis_pan.signals_data = self.analysis_pan.signals_data[:k]
                        break
                    else:  # nothing changed
                        return signals_flag

            # no custom signals
            if not custom_sigs:
                return signals_flag

            # assigning raw signals to local variables
            for k in range(len(self.analysis_pan.signals_data)):
                locals()['u' + str(k+1)] = np.array(self.analysis_pan.signals_data[k]['u'])

            # adding custom signals
            for custom_sig in custom_sigs:
                if not self.analysis_pan.signals_data:
                    continue
                # raw signals detection
                expr = custom_sig['expression']
                expr_u = expr
                for k in range(len(self.analysis_pan.signals_data), 0, -1):
                    expr_u = expr_u.replace(f'u{k}', f'u[{k}]')
                sigs_k = []
                for k in range(len(self.analysis_pan.signals_data)):
                    if f'u[{k+1}]' in expr_u:
                        sigs_k.append(k)
                if not sigs_k:  # no signal in expression
                    sigs_k = [0]

                # sampling freq and init time check
                t0 = self.analysis_pan.signals_data[sigs_k[0]]['t0']
                fs = self.analysis_pan.signals_data[sigs_k[0]]['fs']
                samples = len(self.analysis_pan.signals_data[sigs_k[0]]['u'])
                t = t0 + np.arange(samples) / fs

                # signal computation
                try:
                    u = eval(expr)
                except Exception:
                    traceback.print_exc()
                    continue
                if not (type(u) == np.ndarray and u.dtype in [float, int]):  # problem in array shape or type
                    if type(u) == np.ndarray:
                        print(f'Custom signal type error ({type(u)}, {u.dtype})')
                    else:
                        print(f'Custom signal type error ({type(u)})')
                    continue

                # signal creation
                self.analysis_pan.signals_data.append({
                    'u': u,
                    't0': t0,
                    'fs': fs,
                    'label': custom_sig['label'],
                    'custom': True
                })

            return signals_flag + 1

        # config file update callback
        @callback(
            Output('store-config', 'data', allow_duplicate=True),
            Input('store-custom-sigs', 'data'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def config_custom_sigs_update(custom_sigs: list[dict], data: dict) -> dict:
            data['analysis_config']['custom_signals'] = custom_sigs
            return data
