import copy
import os
import json
import sys
import traceback
import socket
import logging
import time

from dash import Dash, html, dcc, DiskcacheManager, callback, Input, Output, State, ALL, ctx
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
import diskcache
import webbrowser
# from celery import Celery
# from dash.long_callback import CeleryManager

from options_pan import OptionsPanel
from sig_pan import SignalPanel
from acq_pan import AcquisitionPanel
from analysis_pan import AnalysisPanel
from geronimo_utilities.back_front_com import GeronimoGeneralInterface
from geronimo_utilities.config_files import GeroConfig

# info & error logger
logging.basicConfig(handlers=[logging.StreamHandler()], encoding='utf-8', level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
# prevent most dash logs
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


class MainApp(Dash):
    EXIT_ON_TAB_CLOSE = False  # exit whole python script when geronimo tab is closed in browser

    def __init__(self, config: dict = None, files: list[str] = None):
        """
        Main app object

        :param config: app config dictionnary, if None loads config.json at base folder, if doesn't exist loads default
        :param files: list of .gero files paths, to open in Analysis tab
        """

        if files is None:
            files = []

        # cache for long callbacks (multithreading)
        # cache = diskcache.Cache(r'.\cache')
        cache = diskcache.Cache()
        background_callback_manager = DiskcacheManager(cache)

        # init
        super().__init__(__name__, title='Geronimo', update_title=None,  # , external_stylesheets=[dbc.themes.LUMEN]
                         background_callback_manager=background_callback_manager)

        # config
        if config is None:
            file_name = 'config.json'
            if os.path.isfile(file_name):
                with open(file_name, 'r') as file:
                    config = json.load(file)
            else:
                config = copy.deepcopy(GeroConfig.DEFAULT_CONFIG)

        # layout
        def get_layout_children(config: dict, init_tab: str = None, files: list[str] = None):
            # arguments
            if files is None:
                files = []
            config = GeroConfig.get_complete_config(config)
            if init_tab is None:
                init_tab = 'pan-ana' if files else config['active_tab']

            # general application layout
            tab_height = '4vh'
            tabs_options = {
                'style': {'padding': 0, 'line-height': tab_height},
                'selected_style': {'padding': '0', 'line-height': tab_height}
            }
            layout_children = [
                dcc.Tabs(id='tabs-app', children=[
                    # options
                    dcc.Tab(value='pan-opt', label='Options', **tabs_options, children=[
                        OptionsPanel(config=config)
                    ]),
                    # emission signal
                    dcc.Tab(value='pan-sig', label='Emission signal generation', **tabs_options, children=[
                        SignalPanel(config=config)
                    ]),
                    # acquisition
                    dcc.Tab(value='pan-acq', label='Acquisition', **tabs_options, children=[
                        AcquisitionPanel(config=config)
                    ]),
                    # visualisation
                    dcc.Tab(value='pan-ana', label='Analysis', **tabs_options, children=[
                        AnalysisPanel(config=config, files=files)
                    ]),
                ],
                    value=init_tab,
                    style={'font-size': '110%', 'height': tab_height}
                ),
                dcc.Interval(id='interval-saving', interval=3000, n_intervals=0),  # interval for automatic saving (3s)
                dcc.Interval(id='interval-gero-connection', interval=5000, n_intervals=0),  # checking gero connection
                dcc.Location(id='link-reload', refresh=True),  # element for page reload
                # popups
                dbc.Alert(id='popup-warning-gero-not-found', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-warning-gero-address', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-info-sig', is_open=False, dismissable=True, color='success', duration=3000,
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-warning-sig', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-error-sig', is_open=False, dismissable=True, color='danger',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-warning-sig-folders', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-error-shot', is_open=False, dismissable=True, color='danger',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-warning-nb-pts', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-warning-data-folders', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                dbc.Alert(id='popup-warning-csv-format', is_open=False, dismissable=True, color='warning',
                          class_name='alert-fixed'),
                html.Button(id='btn-close-console', hidden=True)
            ]

            return layout_children

        self.layout = html.Div(id='div-main-app', children=get_layout_children(config=config, files=files),
                               style={'height': '100vh'})

        # close console when browser closed
        @callback(
            Output('btn-close-console', 'n_clicks'),
            Input('btn-close-console', 'n_clicks'),
            prevent_initial_call=True,
        )
        def close_console(_):
            if self.EXIT_ON_TAB_CLOSE:
                os._exit(0)
            return 1

        # new config loaded callback
        @callback(
            Output('div-main-app', 'children', allow_duplicate=True),
            # Output('link-reload', 'href'),
            Input('store-config-update', 'data'),
            prevent_initial_call=True
        )
        def update_layout(config: dict):
            return get_layout_children(config, init_tab='pan-opt')  # , '/'

        # geronimo connection indicator
        @callback(
            Output('ind-connection', 'value'),
            Output({'type': 'ind-connection-options', 'gero': ALL}, 'value'),
            Output({'type': 'label-info', 'gero': ALL}, 'children'),
            Output('label-gero-connection', 'children'),
            Input({'type': 'input-gero-address', 'gero': ALL}, 'value'),
            Input('interval-gero-connection', 'n_intervals'),
            State('btn-start', 'disabled'),
            State({'type': 'label-info', 'gero': ALL}, 'children'),
        )
        def connection_indicator_callback(addresses, _, btn_disabled, info_str):
            # no info persistence for new address
            if ctx.triggered_id != 'interval-gero-connection':
                info_str = [''] * len(info_str)
            # no update during shot
            if btn_disabled:
                raise PreventUpdate
            # connection & geronimos info update
            back_front_interface = GeronimoGeneralInterface(addresses, [8] * len(addresses))
            connected, info = back_front_interface.check_connection()
            info_str = ['(' + ', '.join([f'{k}: {e}' for k, e in i.items()]) + ')' if i else i_str
                        for i, i_str in zip(info, info_str)]
            msg = 'Geronimo connected' if all(connected) else 'Geronimo not connected'
            return all(connected), connected, info_str, msg

        # active panel
        @callback(
            Output('store-config', 'data', allow_duplicate=True),
            Input('tabs-app', 'value'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def active_pan_callback(act_pan: str, config: dict):
            config['active_tab'] = act_pan
            return config


if __name__ == '__main__':
    debug = False
    # check if app already running
    if not debug:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('127.0.0.1', 8050))
        sock.close()
        if result == 0:
            input('\nApp already running (port 8050 in host 127.0.0.1 open).'
                  '\nUse running app or close it to launch new app.')
    # launch app
    try:
        app = MainApp(config=None, files=sys.argv[1:])
        webbrowser.open('http://127.0.0.1:8050/', new=1, autoraise=True)
        app.run(host='127.0.0.1', port=8050, debug=debug)
    except:
        logging.critical(traceback.format_exc())
        input()
