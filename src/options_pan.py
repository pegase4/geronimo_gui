from dash import html, dcc, Input, Output, State, callback, ALL, ctx
import dash_daq as daq
from dash.exceptions import PreventUpdate
import copy
import json
import base64
import traceback
import numpy as np
import logging

from geronimo_utilities.config_files import GeroConfig
from geronimo_utilities.back_front_com import GERO_URL_ADDRESSES, SingleGeronimo
from acq_pan import GeronimosIO


class OptionsPanel(html.Div):
    MAX_SYNC_GERO = 100
    DEFAULT_NB_CH = 8
    GERO_INDICATORS = [
        daq.Indicator(id={'type': 'ind-connection-options', 'gero': k_gero}, value=False,
                      style={'display': 'inline-block', 'margin': 8, 'verticalAlign': 'middle'}).to_plotly_json()
        for k_gero in range(MAX_SYNC_GERO)
    ]

    def __init__(self, config: dict):

        ######################################## display ########################################

        super().__init__([
            html.Div(style={'width': '100%', 'padding': 30}, children=[
                dcc.Store(id='store-config', data=config),  # store object for config
                dcc.Store(id='store-config-saved-flag', data=False),  # flag/not saved for saved config
                dcc.Store(id='store-callback-dump'),  # store object for dump output
                dcc.Store(id='store-prevent-multiple', data=False),  # prevent multiple geronimo check trigger
                html.Div([
                    html.H3('Configuration'),
                    html.Div(style={'margin-left': 5}, children=[
                        html.Label(id='label-config-saved', children=' ',
                                   style={'margin-bottom': '0.5ch'}),
                        html.Br(),
                        html.Button(id='btn-save-config', children='Save config as',
                                    style={'display': 'inline-block'}),
                        dcc.Download(id='download-save-config'),
                        dcc.Store(id='store-auto-save-config', data=-1),  # store object for auto saving from outside
                        html.Div([
                            dcc.Upload(
                                id='upload-config', children=html.Button('Load config'),
                                multiple=False, accept='.json'
                            ),
                        ], style={'margin-left': 20, 'display': 'inline-block'},
                            title='Load new config from config file\nOverwrites config.json at root folder'),
                        html.Button(id='btn-load-default-config', children='Load default config',
                                    style={'margin-left': 20, 'display': 'inline-block'},
                                    title='Overwrites config.json at root folder'),
                    ])
                ], title='Current window parameters'),
                html.Br(),
                html.Div([
                    html.H3('Base folder'),
                    html.Div(style={'margin-left': 5}, children=[
                        dcc.Input(id='input-base-folder', value=config['options_config']['base_folder'],
                                  autoComplete='off', style={'width': 600}),
                    ])
                ], title='Project base folder (for data and signals subfolders)'),
                html.Br(),
                html.Div([
                    html.H3('Geronimo'),
                    html.Div(style={'margin-left': 5}, children=[
                        html.Div([
                            dcc.Checklist(id='check-multiple-geros',
                                          options={'mult_gero': ' Use multiple synchronized Geronimos'},
                                          value=['mult_gero'] * (len(config['options_config']['geronimo_nb_ch']) > 1),
                                          style={'margin-bottom': 10}),
                            dcc.Store(id='gero-new-order', data=[])
                        ]),
                        html.Datalist(id='datalist-gero-adress',
                                      children=[html.Option(value=el) for el in GERO_URL_ADDRESSES]),
                        gero_div := html.Div(id='div-gero-address'),
                        html.Button(id='find-gero-address-btn', children='Find Geronimo address(es)',
                                    style={'margin-top': 10}),
                        html.Progress(id='ip-search-progress', value='0', hidden=True,
                                      style={'display': 'inline-block', 'margin-left': 20, 'verticalAlign': 'middle'})
                    ])
                ]),
                html.Br(),
                html.Div([
                    html.H3('Acquisitions'),
                    html.Div(style={'margin-left': 5}, children=[
                        html.H5('Loop'),
                        html.Div(style={'margin-left': 5}, children=[
                            dcc.RadioItems(id='radio-acq-loop', value=config['options_config']['acq_loop'],
                                           options={'all': ' Loop over all acquisitions',
                                                    'current': ' Only selected acquisition'})
                        ]),
                        html.H5('Saving mode'),
                        html.Div(style={'margin-left': 5}, children=[
                            dcc.RadioItems(id='radio-acq-saving-mode',
                                           value=config['options_config']['acq_saving_mode'],
                                           options={'all': ' Save signals from all emission channels in one file',
                                                    'each': ' Save signals from each emission channel in different files'})
                        ])
                    ])
                ], title='See Acquisition tab'),
                html.Br(),
                html.Div([
                    html.H3('File saving metadata'),
                    html.Div(style={'margin-left': 5}, children=[
                        dcc.Textarea(id='text-user-data', value=config['options_config']['user_data'],
                                     style={'width': 600, 'height': 150})
                    ])
                ], title='Saved in all .gero files as "user_data"')
            ])
        ])

        def _get_single_gero_address_layout(address: str | None):
            l = [
                html.Label('Geronimo address:'),
                html.Br(),
                dcc.Input(id={'type': 'input-gero-address', 'gero': 0}, value=address,  # type='url',
                          list='datalist-gero-adress', autoComplete='off', debounce=False, style={'width': 500}),
                html.Br(),
                html.Div([
                    self.GERO_INDICATORS[0],
                    html.Label(id='label-gero-connection', children='', style={'verticalAlign': 'middle'}),
                    html.Label(id={'type': 'label-info', 'gero': 0},
                               style={'display': 'inline-block', 'margin-left': 15, 'verticalAlign': 'middle'})
                ]),
            ]
            return l

        def _get_multi_gero_addresses_layout(addresses: list[str | None]):
            l = [
                html.Label('Number of geronimos:', style={'margin-right': 25, 'margin-bottom': 12}),
                dcc.Input(id='input-nb-gero', value=len(addresses), type='number', min=2, max=self.MAX_SYNC_GERO,
                          step=1, style={'width': 50}),
                html.Br(),
                html.Label('Geronimos addresses:'),
                html.Label(id='label-gero-connection', children='', hidden=True),  # for compatibility with multi gero
                html.Br()
            ]
            for k_gero in range(len(addresses)):
                address = addresses[k_gero]
                l += [
                    html.Label(children=f'Geronimo {k_gero + 1}',
                               style={'margin-left': 10, 'margin-right': 10, 'margin-top': 8}),
                    dcc.Input(id={'type': 'input-gero-address', 'gero': k_gero}, value=address,  # type='url',
                              list='datalist-gero-adress', autoComplete='off', debounce=False, style={'width': 500}),
                    self.GERO_INDICATORS[k_gero]
                ]
                if k_gero == 0:
                    l.append(html.Label('Master Geronimo', style={'display': 'inline-block', 'margin-left': 15}))
                else:
                    l.append(html.Button(id={'type': 'button-set-gero-master', 'gero': k_gero},
                                         children='Set as master',
                                         style={'display': 'inline-block', 'margin-left': 15}))
                l.append(html.Label(id={'type': 'label-info', 'gero': k_gero},
                                    style={'display': 'inline-block', 'margin-left': 15}))
                l.append(html.Br())
            return l

        if len(config['options_config']['geronimo_address']) == 1:  # single geronimo mode
            gero_addr = config['options_config']['geronimo_address'][0]
            gero_addr = '' if gero_addr is None else gero_addr  # for dcc.Input update
            gero_div.children = _get_single_gero_address_layout(gero_addr)
        else:  # multiple synchronized geronimos
            gero_div.children = _get_multi_gero_addresses_layout(config['options_config']['geronimo_address'])

        ######################################## callbacks ########################################

        # options config update
        @callback(
            Output('store-config', 'data', allow_duplicate=True),
            Output({'type': 'tab-param-store', 'index': ALL}, 'data', allow_duplicate=True),
            Output({'type': 'div-gero-io', 'index': ALL}, 'children'),
            Output('gero-new-order', 'data', allow_duplicate=True),
            Input('radio-acq-loop', 'value'),
            Input('radio-acq-saving-mode', 'value'),
            Input('input-base-folder', 'value'),
            Input({'type': 'input-gero-address', 'gero': ALL}, 'value'),
            Input('text-user-data', 'value'),
            State('gero-new-order', 'data'),
            State({'type': 'tab-param-store', 'index': ALL}, 'id'),
            State({'type': 'div-gero-io', 'index': ALL}, 'id'),
            State('tabs', 'children'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def options_config_update(acq_loop: str, acq_saving_mode: str, base_folder: str | None,
                                  gero_address: list[str | None], user_data: str | None, gero_new_order: list[int],
                                  tab_param_ids: list[dict], gero_io_ids: list[dict], tabs_children: list[dict],
                                  config: dict):
            gero_address_old = copy.deepcopy(config['options_config']['geronimo_address'])
            gero_nb_ch_old = copy.deepcopy(config['options_config']['geronimo_nb_ch'])
            config['options_config'] = {
                'base_folder': base_folder,
                'geronimo_address': gero_address,
                'acq_loop': acq_loop,
                'acq_saving_mode': acq_saving_mode,
                'user_data': user_data
            }

            # geronimos new order
            if gero_new_order:  # gero_new_order not empty, i.e. number of geronimos changed or master geronimo changed
                new_order = gero_new_order
                if len(new_order) != len(gero_address):
                    err_msg = (f'Inconsistent new geronimo order {new_order}, {len(gero_address)}'
                               f', {len(gero_address_old)}')
                    logging.error(err_msg)
                    raise RuntimeError(err_msg)
            else:  # gero_new_order empty, i.e. order of geronimos was not changed (only address inputs, or other input)
                new_order = [k_gero for k_gero in range(len(gero_address))]

            # number of channels
            if len(gero_address) > len(gero_address_old):  # adding default number of channels
                gero_nb_ch_old += [self.DEFAULT_NB_CH for _ in range(len(gero_address) - len(gero_address_old))]
            config['options_config']['geronimo_nb_ch'] = [gero_nb_ch_old[k_ch] for k_ch in new_order]

            # emission/reception channels update
            for k_acq in range(len(config['tabs_configs'])):
                if len(gero_address) > len(gero_address_old):
                    config['tabs_configs'][k_acq]['reception_ch'] += \
                        ['OFF' for _ in range(self.DEFAULT_NB_CH * (len(gero_address) - len(gero_address_old)))]
                # correcting reception channels
                rec_ch_old = copy.deepcopy(config['tabs_configs'][k_acq]['reception_ch'])
                config['tabs_configs'][k_acq]['reception_ch'] = []
                for k_gero_old in new_order:
                    config['tabs_configs'][k_acq]['reception_ch'] += \
                        rec_ch_old[sum(gero_nb_ch_old[:k_gero_old]):sum(gero_nb_ch_old[:k_gero_old+1])]
                # correcting emission channels
                em_ch_old = copy.deepcopy(config['tabs_configs'][k_acq]['emission_ch'])
                # adding 'G1:' for single geronimo
                if all([e.startswith('G') for e in em_ch_old]):
                    pass
                elif all([not e.startswith('G') for e in em_ch_old]):
                    em_ch_old = ['G1:' + e for e in em_ch_old]
                else:
                    raise RuntimeError  # emission channels must be all 'E2', 'E5', etc. or all 'G1:E3', 'G2:E1', etc.
                # switching geronimos
                config['tabs_configs'][k_acq]['emission_ch'] = []
                for k_gero in range(len(new_order)):
                    config['tabs_configs'][k_acq]['emission_ch'] += \
                        [f'G{k_gero+1}:' + e.split(':')[-1]
                         for e in em_ch_old if e.startswith(f'G{new_order[k_gero]+1}:')]
                # removing 'G1:' for single geronimo
                if len(new_order) == 1:
                    config['tabs_configs'][k_acq]['emission_ch'] = \
                        [e.split(':')[-1] for e in config['tabs_configs'][k_acq]['emission_ch']]
                # sorting
                config['tabs_configs'][k_acq]['emission_ch'] = sorted(config['tabs_configs'][k_acq]['emission_ch'])

            # individual tabs param stores update
            tabs_ids = [tab['props']['tab_id'] for tab in tabs_children]  # acquisition tabs ids
            tabs_configs = []
            for tab_param_id in tab_param_ids:
                tab_ind = tabs_ids.index(tab_param_id['index'])
                tabs_configs.append(config['tabs_configs'][tab_ind])
            # geronimo IO panels update
            gero_ios = []
            for gero_io_id in gero_io_ids:
                tab_ind = tabs_ids.index(gero_io_id['index'])
                tab_config = config['tabs_configs'][tab_ind]
                gero_ios.append(
                    GeronimosIO(id=gero_io_id, nb_ch=config['options_config']['geronimo_nb_ch'],
                                emission_ch=tab_config['emission_ch'], reception_ch=tab_config['reception_ch'])
                )

            return config, tabs_configs, gero_ios, []

        # config upload callback
        @callback(Output('store-config-update', 'data', allow_duplicate=True),
                  Output('upload-config', 'contents'),
                  Input('upload-config', 'contents'),
                  State('store-config-update', 'data'),
                  prevent_initial_call=True)
        def new_config_callback(data, previous_config):
            data_type, data_string = data.split(',')
            decoded = base64.b64decode(data_string)
            new_config = json.loads(decoded)
            if new_config is None:
                new_config = previous_config
            return new_config, None

        # default config upload callback
        @callback(Output('store-config-update', 'data', allow_duplicate=True),
                  Input('btn-load-default-config', 'n_clicks'),
                  prevent_initial_call=True)
        def default_config_callback(_):
            return {}

        # config saving callback
        @callback(Output('download-save-config', 'data'),
                  Input('btn-save-config', 'n_clicks'),
                  State('store-config', 'data'),
                  prevent_initial_call=True)
        def save_config(_, config):
            return {'content': GeroConfig.get_config_str(config),
                    'filename': GeroConfig.DEFAULT_CONFIG_FILE_NAME}

        # config automatic saving callback
        @callback(Output('store-callback-dump', 'data', allow_duplicate=True),
                  Output('store-config-saved-flag', 'data', allow_duplicate=True),
                  Input('store-auto-save-config', 'data'),
                  Input('interval-saving', 'n_intervals'),
                  State('store-config', 'data'),
                  State('store-config-saved-flag', 'data'),
                  prevent_initial_call=True)
        def auto_save_config(_, __, config, config_saved):
            if ctx.triggered_id == 'interval-saving' and config_saved:  # automatic save but config saved
                return None, True
            with open(GeroConfig.DEFAULT_CONFIG_FILE_NAME, 'w') as file:
                file.write(GeroConfig.get_config_str(config))
            return None, True

        # config changed flag update
        @callback(
            Output('store-config-saved-flag', 'data'),
            Input('store-config', 'data'),
            prevent_initial_call=True
        )
        def update_saved_flag(config):
            GeroConfig.check_config_format(config)
            # print('param update')
            # print(json.dumps(config, separators=(',', ': '), indent=2))
            return False

        # save flag changed update
        @callback(
            Output('label-config-saved', 'children'),
            Input('store-config-saved-flag', 'data')
        )
        def update_saved_label(flag):
            if flag:
                label = 'Current configuration saved'
            else:
                label = 'Current configuration unsaved'
            return label

        # find geronimo address callback
        @callback(
            output=[Output('div-gero-address', 'children', allow_duplicate=True),
                    Output('check-multiple-geros', 'value'),
                    Output('store-prevent-multiple', 'data', allow_duplicate=True),
                    Output('popup-warning-gero-not-found', 'children'),
                    Output('popup-warning-gero-not-found', 'is_open')],
            progress=[Output('ip-search-progress', 'value', allow_duplicate=True)],
            inputs=[Input('find-gero-address-btn', 'n_clicks'),
                    State('div-gero-address', 'children'),
                    State('check-multiple-geros', 'value'),
                    State('store-config', 'data')],
            background=True,
            running=[
                (Output('find-gero-address-btn', 'disabled'), True, False),
                (Output('ip-search-progress', 'hidden'), False, True),
                (Output('ip-search-progress', 'value', allow_duplicate=True), 0., 1.),
            ],
            prevent_initial_call=True
        )
        def update_gero_address(progress_func, n_clicks, addr_div, check_mult: list[str], config: dict):
            if n_clicks is None:
                return addr_div, check_mult, True, '', False
            try:
                addresses1 = copy.deepcopy(config['options_config']['geronimo_address'])
                addresses2 = SingleGeronimo.get_gero_address(progress_func, addresses1, self.MAX_SYNC_GERO)
                addresses2 = [addr for addr in addresses2 if addr not in addresses1]
                if len(addresses2) == 0:
                    return addr_div, check_mult, True, 'No new Geronimo address(es) found', True
                # filling the blank addresses
                for k1, addr1 in enumerate(addresses1):
                    if addr1 is None or addr1 == '':
                        addresses1[k1] = addresses2[0]
                        del addresses2[0]
                    if not addresses2:
                        break
                addresses1 += addresses2
                if len(addresses1) > self.MAX_SYNC_GERO:
                    addresses1 = addresses1[:self.MAX_SYNC_GERO]
                # updating display
                if len(addresses1) == 1:
                    return _get_single_gero_address_layout(addresses1[0]), [], True, '', False
                elif len(addresses1) > 1:
                    return _get_multi_gero_addresses_layout(addresses1), ['mult_gero'], True, '', False
                else:
                    raise RuntimeError
            except Exception as e:
                logging.error(traceback.format_exc())
                return addr_div, check_mult, True, type(e).__name__ + ': ' + str(e), True

        # multiple geronimos option callback
        @callback(
            Output('div-gero-address', 'children', allow_duplicate=True),
            Output('store-prevent-multiple', 'data', allow_duplicate=True),
            Output('gero-new-order', 'data', allow_duplicate=True),
            Input('check-multiple-geros', 'value'),
            State('div-gero-address', 'children'),
            State('store-prevent-multiple', 'data'),
            State({'type': 'input-gero-address', 'gero': ALL}, 'value'),
            prevent_initial_call=True
        )
        def multi_gero_mode(check_mult: list[str], div_address, prevent_mult: bool, gero_address: list[str | None]):
            if prevent_mult:
                return div_address, False, []
            if check_mult:
                gero_address.append('')
                return _get_multi_gero_addresses_layout(gero_address), False, [0, 1]
            else:
                kg = [kg for kg in range(len(gero_address)) if gero_address[kg] is not None and gero_address[kg] != '']
                kg = kg[0] if kg else 0
                addr = gero_address[kg] if gero_address[kg] is not None else ''
                return _get_single_gero_address_layout(addr), False, [kg]

        # number of geronimos input callback
        @callback(
            Output('div-gero-address', 'children', allow_duplicate=True),
            Output('gero-new-order', 'data', allow_duplicate=True),
            Input('input-nb-gero', 'value'),
            State({'type': 'input-gero-address', 'gero': ALL}, 'value'),
            prevent_initial_call=True
        )
        def update_nb_gero_addresses(nb_gero: int, gero_address: list[str | None]):
            if nb_gero > len(gero_address):
                # adding empty addresses
                gero_address += [''] * (nb_gero - len(gero_address))
                new_order = [k_gero for k_gero in range(nb_gero)]
            elif nb_gero < len(gero_address):
                # removing and moving empty addresses
                new_order = [k_gero for k_gero in range(len(gero_address))]
                k_addr = -1
                while len(gero_address) > nb_gero:
                    if k_addr < - len(gero_address):
                        del gero_address[-1]
                        del new_order[-1]
                    elif gero_address[k_addr] is None or gero_address[k_addr] == '':
                        del gero_address[k_addr]
                        del new_order[k_addr]
                    else:
                        k_addr -= 1
            else:
                new_order = [k_gero for k_gero in range(nb_gero)]
            return _get_multi_gero_addresses_layout(gero_address), new_order

        # set geronimo as master callback
        @callback(
            Output({'type': 'input-gero-address', 'gero': ALL}, 'value', allow_duplicate=True),
            Output('gero-new-order', 'data', allow_duplicate=True),
            Input({'type': 'button-set-gero-master', 'gero': ALL}, 'n_clicks_timestamp'),
            State({'type': 'button-set-gero-master', 'gero': ALL}, 'id'),
            State({'type': 'input-gero-address', 'gero': ALL}, 'value'),
            State({'type': 'input-gero-address', 'gero': ALL}, 'id'),
            prevent_initial_call=True
        )
        def set_master(btn_clicks_ts: list[int], btn_ids: list[dict], addresses: list[str], add_ids: list[dict]):
            btn_clicks_ts = [-1 if e is None else e for e in btn_clicks_ts]
            if all([t == -1 for t in btn_clicks_ts]):
                raise PreventUpdate
            k_click = int(np.argmax(btn_clicks_ts))
            id_click = btn_ids[k_click]['gero']
            k_address = None
            for k_addr in range(len(add_ids)):
                if add_ids[k_addr]['gero'] == id_click:
                    k_address = k_addr
                    break
            if k_address is None:
                raise ValueError
            master_addr = addresses.pop(k_address)
            addresses = [master_addr] + addresses
            addresses = ['' if addr is None else addr for addr in addresses]
            # addresses new order
            new_order = [k_gero for k_gero in range(len(addresses))]
            new_order.pop(k_address)
            new_order = [k_address] + new_order
            return addresses, new_order

        # identical geronimo ip addresses warning
        @callback(
            Output('popup-warning-gero-address', 'children'),
            Output('popup-warning-gero-address', 'is_open'),
            Input({'type': 'input-gero-address', 'gero': ALL}, 'value'),
        )
        def warning_identical_ip_addr(addresses: list[str | None]):
            addresses = [addr for addr in addresses if addr is not None and addr not in ['', 'fake']]
            addresses.sort()
            for k in range(len(addresses) - 1):
                if addresses[k] == addresses[k + 1]:
                    return 'Multiple identical Geronimo IP addresses', True
            return '', False
