from dash import Dash, html, dcc, Input, Output, State, callback, ctx, ALL
import plotly.graph_objects as go
import numpy as np
import traceback
import scipy.signal as sig
import base64
import io
import os
import copy
import re
import logging

from geronimo_utilities.config_files import GeroParam


class SignalPanel(html.Div):
    FFT_SAMPLES = int(1E5)  # number of samples for fft computation (zero-padding)
    MAX_FREQ = 500e3  # max frequency displayed

    def __init__(self, config: dict):
        """
        Emission signal creation tab.
        """
        ######################################## display ########################################

        config = config['signal_config']

        # controls panel
        tab_sig_pan_set = html.Div([
            dcc.Store(id='store-signal-folders-debounce', data=None),  # store for sig. folders inputs debounce behavior
            html.Div([
                html.Div([  # parameters div
                    html.H3('Parameters'),
                    html.Div([
                        html.Label('Sampling frequency:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Dropdown(
                            id='drop-fs', value=config['fs'], clearable=False, maxHeight=300, disabled=True,
                            options=[{'value': fs, 'label': f'{1e-6 * fs} MHz'} for fs in GeroParam.EMISSION_FS],
                            style={'width': 185, 'display': 'inline-block', 'verticalAlign': 'middle'}
                        )
                    ], style={'margin': 5}),
                    html.Div([
                        html.Label('Max. samples:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Input(id='input-samples', type='number', step=1, value=config['samples'],
                                  max=GeroParam.MAX_EMISSION_SAMPLES, disabled=True)
                    ], style={'margin': 5}),
                    html.H3('Standard signal'),
                    html.Div(id='div-input-freq', children=[
                        html.Label('Frequency [kHz]:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Input(id='input-freq', type='number', value=config['freq'], min=0, required=True)
                    ], style={'margin': 5}),
                    html.Div(id='div-input-cycles', children=[
                        html.Label('Cycles:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Input(id='input-cycles', type='number', value=config['cycles'], min=0, required=True)
                    ], style={'margin': 5}),
                    html.Div(id='div-input-phase', children=[
                        html.Label('Phase [°]:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Input(id='input-phase', type='number', value=config['phase'], required=True)
                    ], style={'margin': 5}),
                    html.Div(id='div-drop-win', children=[
                        html.Label('Window:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Dropdown(id='drop-win', options={'rectangular': 'Rectangular', 'hann': 'Hann',
                                                             'tukey': 'Tukey'}, value=config['window'],
                                     clearable=False,
                                     style={'display': 'inline-block', 'width': 185, 'verticalAlign': 'middle'})
                    ], style={'margin': 5}),
                    html.Div(id='div-sig-input-alpha', children=[
                        html.Label('Alpha [%]:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Input(id='input-alpha', type='number', value=config['alpha'], min=0, max=100, required=True)
                    ], style={'margin': 5}, hidden=True),
                    html.Div(id='div-input-ampl', children=[
                        html.Label('Amplitude [V]:', style={'width': 200, 'display': 'inline-block'}),
                        dcc.Input(id='input-ampl', type='number', value=config['amplitude'], min=0,
                                  max=GeroParam.MAX_TENSION, required=True)
                    ], style={'margin': 5}),
                    html.H3('Custom signal'),
                    html.Div(id='div-sig-custom', children=[
                        html.Label('Enter custom signal python formula (t as time vector):'),
                        dcc.Textarea(id='input-custom-sig', style={'width': 400}, value=config['signal_formula'])
                    ], style={'margin': 5}),
                ]),
                html.Div([  # signal saving div
                    html.H3('Saving'),
                    html.Div([
                        html.Label('Signal files folder:'),
                        html.Br(),
                        dcc.Input(id='input-save-signal-folder', value=config['signal_folder'],
                                  style={'width': 400}),
                    ], style={'margin': 5}, title='Relative (from project base folder) and absolute paths allowed'),
                    html.Div([
                        html.Label('File name:', style={'width': 100}),
                        dcc.Input(id='input-save-signal-name', value=config['signal_file_name'],
                                  style={'margin-left': 5, 'width': '100%'}),
                    ], style={'display': 'flex', 'margin': 5, 'width': 400}),
                    html.Div([
                        html.Button(id='btn-sig-download', children='Save signal'),
                        html.Button(id='btn-sig-download-as', children='Save signal as', style={'margin-left': 10}),
                        dcc.Download(id='download-signal'),
                    ], style={'margin-left': 5, 'margin-top': 10})
                ])
            ]),
            html.Hr(),
            html.Div([
                html.H3('File visualization'),
                dcc.Upload(
                    id='upload-data', children=html.Div(['Drag and Drop or ', html.A('Select File')]),
                    style={
                        'width': 400,
                        'height': '40px',
                        'lineHeight': '40px',
                        'borderWidth': '1px',
                        'borderStyle': 'dashed',
                        'borderRadius': '5px',
                        'textAlign': 'center',
                        'margin': 5
                    },
                    multiple=False,  # allow multiple files to be uploaded
                    accept='.csv'
                ),
            ], title='Plot signal file')
        ])

        # visualisation panel
        self.sig_data = np.zeros(config['samples'])
        self.sig_time = np.zeros(config['samples'])
        self.sig_fs = config['fs']
        self.sig_tau = config['samples'] / self.sig_fs  # signal maximum non-zero time
        tab_sig_pan_vis = html.Div([
            dcc.Store(id='flag-figupdate'),  # invisible store object used as flag for figure update
            html.H4(id='sig-title'),
            html.Hr(),
            html.Div([
                html.Div([
                    dcc.Checklist(id='check-wholesig', options=[' Show whole signal'], value=[' Show whole signal']),
                ], style={'height': '41vh'},
                    title='Show whole signal / only pulse duration'),
                html.Div([
                    html.Label('Scale:'),
                    dcc.RadioItems(id='radio-fftscale', options={'linear': ' linear', 'log': ' log'},
                                   value='linear')
                ], style={'height': '41vh'}),
            ], style={'width': 100, 'margin-right': -100, 'height': 100, 'margin-bottom': -100}),
            html.Div([
                dcc.Graph(id='graph-sig', style={'width': '100%', 'height': '41vh'}),
                dcc.Graph(id='graph-fft', style={'width': '100%', 'height': '41vh'}),
            ], style={'padding-left': 100, 'width': '100%'}),
        ], style={'width': '100%', 'display': 'inline-block'})

        # whole tab
        super().__init__([
            html.Div(tab_sig_pan_set, style={'width': 500, 'padding': 30, 'display': 'inline-block'}),
            html.Div(tab_sig_pan_vis, style={'width': '100%', 'padding': 30, 'display': 'inline-block'}),
        ], style={'display': 'flex'})

        ######################################## callbacks ########################################

        # file upload callback
        @callback(Output('flag-figupdate', 'data', allow_duplicate=True),
                  Output('input-save-signal-name', 'value', allow_duplicate=True),
                  Output('upload-data', 'contents'),
                  Output('popup-error-sig', 'children', allow_duplicate=True),
                  Output('popup-error-sig', 'is_open', allow_duplicate=True),
                  Input('upload-data', 'contents'),
                  State('upload-data', 'filename'),
                  State('drop-fs', 'value'),
                  prevent_initial_call=True)
        def upload_signal(data: str, file_name: str, fs: float) -> (str, str, None):
            """
            Signal uploading callback.
            :param data: csv file data
            :param file_name: csv file name
            :return: 'flag-figupdate' data (for figure updating and name), 'upload-data' content (set back to None for
            updating even with same file)
            """
            if data is None:
                return 'File: ' + file_name, file_name, None
            try:
                self.sig_data, self.sig_fs = self.read_csv_file(data)
                self.sig_time = np.arange(self.sig_data.size) / self.sig_fs
                self.sig_tau = self.sig_data.size / self.sig_fs
                return 'File: ' + file_name + f' (fs = {1e-6 * self.sig_fs} MHz)', file_name, None, '', False
            except Exception as e:
                logging.error(traceback.format_exc())
                self.sig_data = np.nan * np.zeros(1)
                self.sig_time = np.zeros(1)
                self.sig_fs = fs
                self.sig_tau = 1 / self.sig_fs
                return 'No signal', file_name, None, type(e).__name__ + ': ' + str(e), True

        # alpha hidden callback
        @callback(
            Output('div-sig-input-alpha', 'hidden'),
            Input('drop-win', 'value')
        )
        def update_alpha_hidden(win):
            if win == 'tukey':
                return False
            else:
                return True

        # input creation callback
        @callback(
            Output('flag-figupdate', 'data'),
            Output('input-save-signal-name', 'value'),
            Input('drop-fs', 'value'),
            Input('input-samples', 'value'),
            Input('input-ampl', 'value'),
            Input('input-freq', 'value'),
            Input('input-cycles', 'value'),
            Input('input-phase', 'value'),
            Input('drop-win', 'value'),
            Input('input-alpha', 'value'),
            Input('div-input-ampl', 'n_clicks'),
            Input('div-input-freq', 'n_clicks'),
            Input('div-input-cycles', 'n_clicks'),
            Input('div-input-phase', 'n_clicks'),
            Input('div-drop-win', 'n_clicks'),
            Input('div-sig-input-alpha', 'n_clicks')
        )
        def create_signal(fs: float, samples: int, ampl: float, freq: float, cycles: float, phase: float, window: str,
                          alpha: float, *_):
            # signal creation
            self.sig_time = np.arange(samples) / fs
            self.sig_fs = fs
            if ampl is None or freq is None or cycles is None or phase is None or freq == 0:
                self.sig_data = np.nan * np.ones(samples)
                self.sig_tau = samples / fs
            else:
                self.sig_data = ampl * np.sin(2e3 * np.pi * freq * self.sig_time + phase * np.pi / 180)
                self.sig_tau = cycles / (1e3 * freq)
                match window:
                    case 'rectangular':
                        win = self.sig_time < self.sig_tau
                    case 'hann':
                        N = self.sig_tau * fs
                        win0 = np.square(np.sin(np.pi * np.arange(int(N)) / N))
                        if N <= self.sig_time.size:
                            win = np.zeros(self.sig_time.size)
                            win[:int(N)] = win0
                        else:
                            win = win0[:self.sig_time.size]
                    case 'tukey':
                        N = self.sig_tau * fs
                        Na = self.sig_tau * alpha / 100 * fs
                        t0 = np.arange(int(N) + 1)
                        if Na > 0:
                            win0 = np.square(np.sin(np.pi * t0 / Na)) * (t0 < Na/2) \
                                + 1 * ((Na/2 <= t0) & (t0 < N - Na/2)) \
                                + np.square(np.sin(np.pi * (t0 - N + 2*Na) / Na)) * (N - Na/2 <= t0)
                        else:
                            win0 = np.ones(t0.shape)
                        if win0.size <= self.sig_time.size:
                            win = np.zeros(self.sig_time.size)
                            win[:win0.size] = win0
                        else:
                            win = win0[:self.sig_time.size]
                self.sig_data = self.sig_data * win
            # signal tag string creation
            window_str = window.lower()
            if window == 'tukey':
                window_str += str(alpha)
            signal_name = f'sin_{freq}kHz_{cycles}c_{window_str}_{ampl}V'
            if phase != 0:
                signal_name += f'_{phase}deg'
            if self.sig_fs != GeroParam.EMISSION_FS[0]:
                signal_name += f'_{1e-6 * self.sig_fs:g}MHz'
            signal_name += '.csv'
            return 'Signal creation (standard)', signal_name

        # custom signal creation callback
        @callback(
            Output('flag-figupdate', 'data', allow_duplicate=True),
            Output('input-save-signal-name', 'value', allow_duplicate=True),
            Output('popup-warning-sig', 'children', allow_duplicate=True),
            Output('popup-warning-sig', 'is_open', allow_duplicate=True),
            Input('input-custom-sig', 'value'),
            Input('input-custom-sig', 'n_clicks'),
            State('drop-fs', 'value'),
            State('input-samples', 'value'),
            State('input-save-signal-name', 'value'),
            prevent_initial_call=True
            )
        def create_custom_signal(formula: str, _, fs: float, samples: float, signal_name: str):
            self.sig_time = np.arange(samples) / fs
            self.sig_fs = fs
            t = copy.deepcopy(self.sig_time)
            warning_str = ''
            try:
                self.sig_data = eval(formula)
                if type(self.sig_data) != np.ndarray:
                    raise RuntimeError('')
                if self.sig_data.shape != t.shape:
                    if self.sig_data.ndim != t.ndim or self.sig_data.size > t.size:
                        raise RuntimeError('')
                    self.sig_data = np.pad(self.sig_data, (0, t.size - self.sig_data.size))
                if np.max(np.abs(self.sig_data)) > GeroParam.MAX_TENSION:
                    self.sig_data[self.sig_data > GeroParam.MAX_TENSION] = GeroParam.MAX_TENSION
                    self.sig_data[self.sig_data < - GeroParam.MAX_TENSION] = - GeroParam.MAX_TENSION
                    warning_str = f'Signal clipped (voltage must not exceed +-{GeroParam.MAX_TENSION} V)'
                samples_non0 = samples
                while samples_non0 > 0:
                    if self.sig_data[samples_non0 - 1] == 0:
                        samples_non0 -= 1
                    else:
                        break
                self.sig_tau = samples_non0 / fs
            except:
                self.sig_data = np.nan * np.zeros(samples)
                self.sig_tau = samples / fs
            if signal_name.startswith('sin'):
                signal_name = 'custom_signal.csv'
            return 'Signal creation (custom)', signal_name, warning_str, warning_str != ''

        # figures creation callback
        @callback(Output('graph-sig', 'figure'),
                  Output('graph-fft', 'figure'),
                  Output('sig-title', 'children'),
                  Input('flag-figupdate', 'data'),
                  Input('check-wholesig', 'value'),
                  Input('radio-fftscale', 'value'),
                  State('graph-sig', 'figure'),
                  State('graph-fft', 'figure'))
        def update_fig(title: str, plot_whole_sig: bool, fft_scale: str, fig_temp, fig_fft):
            # trigger input
            trigger_id = ctx.triggered_id

            # signal sampled at 10 MHz
            fig_sig_fs = GeroParam.EMISSION_FS[0]
            fs_ratio = int(np.round(fig_sig_fs / self.sig_fs))
            fig_sig_time = self.sig_time[0] + np.arange(self.sig_time.size * fs_ratio) / fig_sig_fs
            fig_sig_data = (np.ones((fs_ratio, 1)) @ np.array(self.sig_data, ndmin=2)).T.flatten()

            # signal figure
            new_fig_temp = not bool(fig_temp)
            if new_fig_temp:  # None or empty
                fig_temp = go.Figure()
                fig_temp.update_xaxes(title_text='Time [ms]')
                fig_temp.update_yaxes(title_text='Amplitude [V]')
                fig_temp.update_layout(margin={'l': 20, 'r': 10, 't': 10, 'b': 10})
            else:
                fig_temp['data'] = []
                fig_temp = go.Figure(**fig_temp)
            fig_temp.add_trace(go.Scatter(x=1e3 * fig_sig_time, y=fig_sig_data, mode='lines'))
            # fig_temp.update_layout(layout_autorange_after=True)
            if trigger_id == 'check-wholesig' or new_fig_temp:
                if plot_whole_sig:
                    fig_temp.update_xaxes(autorange=True, range=None)
                else:
                    fig_temp.update_xaxes(autorange=False, range=[0, 1e3 * self.sig_tau])

            # fft figure
            N_sig = len(fig_sig_time)
            N_tot = self.FFT_SAMPLES
            sig_fft = 2 * np.abs(np.fft.fft(fig_sig_data, n=N_tot)) / N_sig
            sig_fft[0] /= 2
            freqs = np.arange(sig_fft.size) / N_tot * fig_sig_fs
            sig_fft = sig_fft[freqs <= self.MAX_FREQ]
            new_fig_fft = not bool(fig_fft)
            if new_fig_fft:
                fig_fft = go.Figure()
                fig_fft.update_xaxes(title_text='Frequency [kHz]')
                fig_fft.update_xaxes(range=[0, 1e-3 * self.MAX_FREQ])
                fig_fft.update_layout(margin={'l': 20, 'r': 10, 't': 10, 'b': 10})
                # fig_fft.update_layout(xaxis={'showspikes': True}, hovermode='x unified')
            else:
                fig_fft['data'] = []
                fig_fft['layout']['shapes'] = []
                fig_fft['layout']['annotations'] = []
                fig_fft = go.Figure(fig_fft)
            if fft_scale.lower() == 'linear':
                fig_fft.update_yaxes(title_text='Amplitude [V]', rangemode='tozero')
            else:
                fig_fft.update_yaxes(title_text='Amplitude [dB]', rangemode='normal')
                sig_fft = 20 * np.log10(sig_fft) - 20 * np.log10(np.max(sig_fft))
                fig_fft.add_hline(y=np.max(sig_fft) - 3, line_dash='solid', line_color='red', line_width=0.5,
                                  annotation_text='-3 dB', annotation_font_color='red',
                                  annotation_position='bottom right')
            fig_fft.add_trace(go.Scatter(x=1e-3 * freqs, y=sig_fft, mode='lines'))
            if not new_fig_fft and trigger_id == 'radio-fftscale':
                fig_fft.update_yaxes(autorange=True)
            return fig_temp, fig_fft, title

        # signal saving callback
        @callback(
            Output('popup-info-sig', 'children'),
            Output('popup-info-sig', 'is_open'),
            Output('popup-error-sig', 'children', allow_duplicate=True),
            Output('popup-error-sig', 'is_open', allow_duplicate=True),
            Input('btn-sig-download', 'n_clicks'),
            State('input-save-signal-name', 'value'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def download_signal(_, signal_name: str, config: dict):
            # signal extension
            if not os.path.splitext(signal_name)[1]:
                signal_name += '.csv'
            # writing signal
            sig_csv = self.get_csv_str()
            # saving folder
            save_folder = config['signal_config']['signal_folder']
            if not os.path.isabs(save_folder):
                save_folder = os.path.join(config['options_config']['base_folder'], save_folder)
            # saving signal
            try:
                if not os.path.isdir(save_folder):
                    raise RuntimeError(f'Folder "{save_folder}" doesn\'t exist')
                file_path = os.path.join(save_folder, signal_name)
                if os.path.exists(file_path):
                    raise RuntimeError(f'Error\nFile "{signal_name}" already exists in current folder')
                with open(file_path, 'w') as f:
                    f.write(sig_csv)
                msg = 'Signal saved as ' + signal_name
                return msg, True, '', False
            except Exception as e:
                logging.error(traceback.format_exc())
                return '', False, type(e).__name__ + ': ' + str(e), True

        # signal "saving as" callback
        @callback(Output('download-signal', 'data', allow_duplicate=True),
                  Input('btn-sig-download-as', 'n_clicks'),
                  State('input-save-signal-name', 'value'),
                  prevent_initial_call=True)
        def download_signal_as(_, signal_name: str):
            sig_csv = self.get_csv_str()
            return {'content': sig_csv, 'filename': signal_name}

        # config update callback
        @callback(
            Output('store-config', 'data', allow_duplicate=True),
            Input('drop-fs', 'value'),
            Input('input-samples', 'value'),
            Input('input-ampl', 'value'),
            Input('input-freq', 'value'),
            Input('input-cycles', 'value'),
            Input('input-phase', 'value'),
            Input('drop-win', 'value'),
            Input('input-alpha', 'value'),
            Input('input-custom-sig', 'value'),
            Input('input-save-signal-folder', 'value'),
            Input('input-save-signal-name', 'value'),
            State('store-config', 'data'),
            prevent_initial_call=True
        )
        def config_update(fs: float, samples: int, ampl: float, freq: float, cycles: float, phase: float, win: str,
                          alpha: float, signal_formula: str, signal_folder: str, signal_name: str, data: dict):
            data['signal_config']['fs'] = fs
            data['signal_config']['samples'] = samples
            data['signal_config']['amplitude'] = ampl
            data['signal_config']['freq'] = freq
            data['signal_config']['cycles'] = cycles
            data['signal_config']['phase'] = phase
            data['signal_config']['window'] = win
            data['signal_config']['alpha'] = alpha
            data['signal_config']['signal_formula'] = signal_formula
            data['signal_config']['signal_folder'] = signal_folder
            data['signal_config']['signal_file_name'] = signal_name
            return data

        # different folders warning
        @callback(
            Output('popup-warning-sig-folders', 'children'),
            Output('popup-warning-sig-folders', 'is_open'),
            Output('store-signal-folders-debounce', 'data'),
            Input({'type': 'input-signal-folder', 'index': ALL}, 'n_blur'),
            Input('input-save-signal-folder', 'n_blur'),
            State({'type': 'input-signal-folder', 'index': ALL}, 'value'),
            State('input-save-signal-folder', 'value'),
            State('store-signal-folders-debounce', 'data'),
        )
        def warning_signal_folders(_, __, acq_folders: list[str], local_folder: str, store_data: None | list):
            # debounce behavior
            if store_data == [acq_folders, local_folder]:
                return '', False, store_data
            store_data = copy.deepcopy([acq_folders, local_folder])
            # same folders check
            if not all([f == local_folder for f in acq_folders]):
                warning_msg = ('Warning:\nSignal files folder from emission signal tab different from emission signal '
                               'folder from acquisitions tab')
                return warning_msg, True, store_data
            else:
                return '', False, store_data

    def get_csv_str(self):
        sig_data = self.sig_data[self.sig_time < self.sig_tau]
        if sig_data.size >= GeroParam.MAX_EMISSION_SAMPLES:
            sig_data = sig_data[:GeroParam.MAX_EMISSION_SAMPLES-1]
        sig_data = np.concatenate((sig_data, np.zeros(1)))
        title = f'signal [V] (fs={self.sig_fs})'
        sig_csv = '\n'.join([title] + [str(e) for e in sig_data])
        return sig_csv

    @staticmethod
    def read_csv_file(data: str) -> (np.ndarray[float], float):
        data_type, data_string = data.split(',')
        decoded = base64.b64decode(data_string)
        data_str = io.StringIO(decoded.decode('utf-8')).read()
        sig_data, fs = SignalPanel.read_csv_str(data_str)
        return sig_data, fs

    @staticmethod
    def read_csv_str(data_str: str) -> (np.ndarray[float], float):
        data_strs = data_str.split('\n')
        float_str = r'(?:(?:\d+\.*\d*)|(?:\d*\.*\d+))(?:[Ee][+-]?\d+)?'
        fs = re.findall(r'fs *= *' + float_str, data_strs[0])[0]
        fs = re.findall(float_str, fs)[0]
        fs = float(fs)
        data_arr = np.array([float(s) for s in data_strs[1:]], ndmin=1)
        if data_arr.size > GeroParam.MAX_EMISSION_SAMPLES:
            raise RuntimeError(f'Emission signal length must not exceed {GeroParam.MAX_EMISSION_SAMPLES} points')
        if np.max(np.abs(data_arr)) > GeroParam.MAX_TENSION:
            raise RuntimeError(f'Emission signal voltage must not exceed +-{GeroParam.MAX_TENSION} V')
        return data_arr, fs
